// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017
#include "primitive2d.h"



Primitive2d::Primitive2d(VectorPrimitive pType, ofVec3f positionBases, float pHeight, float pWidth, ofColor pColor,
	bool pFilled, float pStrokeWidth, ofColor pStrokeColor): type(pType), positionBase(positionBases), height(pHeight),
	width(pWidth), color(pColor), filled(pFilled), strokeWidth(pStrokeWidth), strokeColor(pStrokeColor)						
{
	
}

void Primitive2d::setup() {
	
}

void Primitive2d::draw(ofShader * shader) {
	ofPushStyle();

	ofPushMatrix();
	ofMultMatrix(localMatrixTranslation);

	ofPushMatrix();
	ofMultMatrix(localMatrixRotation);

	ofPushMatrix();
	ofMultMatrix(localMatrixScale);
	ofDisableLighting();
	switch (type)
	{
	case VectorPrimitive::ELLIPSE:
		ofSetCircleResolution(48);
		if (filled) {
			ofSetLineWidth(0);
			ofFill();
			ofSetColor(color);
			ofDrawEllipse(0, 0, width, height);
		}
		else {
			ofNoFill();
		}
		ofNoFill();
		if (strokeWidth != 0.0f) {
			ofSetLineWidth(strokeWidth);
			ofSetColor(strokeColor);
			ofDrawEllipse(0, 0, width, height);
		}
		break;

	case VectorPrimitive::RECTANGLE:
		ofEnableLighting();
		ofSetRectMode(OF_RECTMODE_CENTER);
		if (filled) {
			ofSetLineWidth(0);
			ofFill();
			ofSetColor(color);					
						
			shader->begin();
			shader->setUniform3f("materialColorAmbient", ofVec3f(color.get().r / 255.0, color.get().g / 255.0, color.get().b / 255.0));
			shader->setUniform3f("materialColorDiffuse", ofVec3f(color.get().r / 255.0, color.get().g / 255.0, color.get().b / 255.0));
			shader->setUniform3f("materialColorSpecular", ofVec3f(0.5f, 0.5f, 0.5f));
			shader->setUniform1f("materialBrightness", 32.0);
			shader->setUniform1f("alpha", color.get().a / 255.0);			
								
			ofDrawBox(0, 0, 0, width, height, 1.0);
			shader->end();
			ofDisableLighting();
			ofSetColor(255, 0, 0);

		}
		else {
			ofNoFill();
		}
		ofNoFill();
		if (strokeWidth != 0.0f) {
			ofSetLineWidth(strokeWidth);
			ofSetColor(strokeColor);

			ofDrawRectangle(0, 0, width, height);
		}
		break;

	case VectorPrimitive::LINE:
		ofNoFill();
		ofEnableSmoothing();
		if (strokeWidth != 0.0f) {
			ofSetLineWidth(strokeWidth);
			ofSetColor(strokeColor);
			ofDrawLine(0 + width / 2, 0 + height / 2, 0 - width / 2, 0 - height / 2);
		}
		break;

	default:
		break;
	}

	if (isSelected) {
		ofNoFill();
		ofSetLineWidth(1.0);
		ofSetColor(255, 255, 255);
		drawZone();
	}
	ofPopMatrix();
	ofPopMatrix();
	ofPopMatrix();
	ofPopStyle();
	isRotate = false;


}

void Primitive2d::drawZone() {
	ofVec2f myCenter = positionBase;
	int offset = 10.0;
	ofRectangle myRectangle(0, 0, width + offset * 2, height + offset * 2);
	ofRectangle myRectangle2(0 - width / 2 - offset, 0 - height / 2 - offset, width + offset * 2, height + offset * 2);

	bool isInside = false;
	switch (type)
	{
	case VectorPrimitive::ELLIPSE:
		ofDrawRectangle(myRectangle2);
		break;

	case VectorPrimitive::RECTANGLE:
		ofDrawRectangle(myRectangle);
		break;

	case VectorPrimitive::LINE:
		ofDrawRectangle(myRectangle2);
		break;

	default:
		break;


	}
}

//-------------------------------------------------------------------------------------------------
//Style settings
void Primitive2d::setFilled(bool isFilled) {
	filled = isFilled;	
}

void Primitive2d::setColor(ofColor pColor) {
	color = pColor;
}
void Primitive2d::setStrokeWidth(float pStrokeWidth) {
	strokeWidth = pStrokeWidth;
}

void Primitive2d::setHeight(float pHeight) {
	height = pHeight;
}
void Primitive2d::setWidth(float pWidth) {
	width = pWidth;
}
void Primitive2d::setStrokeColor(ofColor pColor) {
	strokeColor = pColor;
}


bool Primitive2d::inside(int px, int py, int pz, ofBoxPrimitive myBox, ofMatrix4x4 modelMatrix, ofCamera * cam) {

	ofVec3f xSmallDistance(0, 0, 0);
	ofVec3f xBigDistance(0, 0, 0);
	ofVec3f ySmallDistance(0, 0, 0);
	ofVec3f yBigDistance(0, 0, 0);

	ofMesh mesh = myBox.getMesh();
	int n = mesh.getNumVertices();
	ofVec3f mouse(px, py);

	for (int i = 0; i < n; i++) {
		ofVec3f cur = cam->worldToScreen(mesh.getVertex(i) * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix);

		if (i == 0 || cur.x <  xSmallDistance.x) {
			xSmallDistance = cur;
		}
		if (i == 0 || cur.x >  xBigDistance.x) {
			xBigDistance = cur;
		}
		if (i == 0 || cur.y <  ySmallDistance.y) {
			ySmallDistance = cur;
		}
		if (i == 0 || cur.y >  yBigDistance.y) {
			yBigDistance = cur;
		}
	}


	if (px >= xSmallDistance.x && py >= ySmallDistance.y  && px <= xBigDistance.x  && py <= yBigDistance.y) {
		return true;
	}

	return false;
}

//-------------------------------------------------------------------------------------------------
//Selection
bool Primitive2d::isInsideShape(int xPressed, int yPressed, int z, ofMatrix4x4 modelMatrix, ofCamera * cam, bool changedSelection) {
	
	ofBoxPrimitive myBox(width + 10, height+10, 0);

	bool isInside = false;
	switch (type)
	{
	case VectorPrimitive::ELLIPSE:
		isInside = inside(xPressed, yPressed, z, myBox, modelMatrix, cam);
		//isInside = myRectangleEllipse.inside(xPressed, yPressed);

		break;

	case VectorPrimitive::RECTANGLE:
		//	isInside = myRectangle.inside(xPressed, yPressed);	
		isInside = inside(xPressed, yPressed, z, myBox, modelMatrix, cam);
		break;

	case VectorPrimitive::LINE:
		isInside = inside(xPressed, yPressed, z, myBox, modelMatrix, cam);
		//isInside = myRectangle.inside(xPressed, yPressed);
		break;

	default:
		break;


	}
	if (isInside && changedSelection)
		isSelected = !isSelected;
	return isInside;
}

//-------------------------------------------------------------------------------------------------
//Transformation

void Primitive2d::setOffsetTranslate(ofVec3f offset) {
	xOffsetTranslate += offset.x;
	yOffsetTranslate += offset.y;
	zOffsetTranslate += offset.z;

	xOffsetTranslateHistory += offset.x;
	yOffsetTranslateHistory += offset.y;
	zOffsetTranslateHistory += offset.z;

	localMatrixTranslation.setTranslation(xOffsetTranslate, yOffsetTranslate, zOffsetTranslate);
}

void Primitive2d::setOffsetScale(ofVec3f offset) {
	if (offset.x == offset.y && offset.y == offset.z) { //this means we are doing an uniform scale	
		if (((xOffsetScale + offset.x / 20.0) > 0) && ((yOffsetScale + offset.y / 20.0) > 0)) { //no z scale for 2d
			xOffsetScale += offset.x / 20.0;
			yOffsetScale += offset.y / 20.0;			
		}
	}
	else { //for non uniform scale transformation
		if (xOffsetScale + offset.x / 20.0 > 0)
			xOffsetScale += offset.x / 20.0;
		if (yOffsetScale + offset.y / 20.0 > 0)
			yOffsetScale += offset.y / 20.0;
		if (zOffsetScale + offset.z / 20.0 > 0)
			zOffsetScale += offset.z / 20.0;
	}
	localMatrixScale.makeScaleMatrix(xOffsetScale, yOffsetScale, zOffsetScale);
	
}
void Primitive2d::setOffsetRotateXY(ofVec3f offset, float delay) {		
	ofQuaternion yRot((offset.x)*delay, ofVec3f(0, 1, 0));
	ofQuaternion xRot((offset.y)*delay, ofVec3f(-1, 0, 0));
	curRot *= yRot*xRot;
	curRotHistory *= yRot*xRot;	
	localMatrixRotation.setRotate(curRot);
}

void Primitive2d::setOffsetRotateZ(ofVec3f offset, float delay) {	
	ofQuaternion zRot((offset.z)*delay, ofVec3f(0, 0, 1));
	curRot *= zRot;
	curRotHistory *= zRot;	
	localMatrixRotation.setRotate(curRot);
}

void Primitive2d::setOffsetRotateY(ofVec3f offset, float delay) {
	ofQuaternion yRot((offset.x)*delay, ofVec3f(0, 1, 0));
	curRot *= yRot;
	curRotHistory *= yRot;	
	localMatrixRotation.setRotate(curRot);
}
void Primitive2d::setOffsetRotateX(ofVec3f offset, float delay) {
	ofQuaternion xRot((offset.y)*delay, ofVec3f(-1, 0, 0));
	curRot *= xRot;
	curRotHistory *= xRot;	
	localMatrixRotation.setRotate(curRot);
}

//-------------------------------------------------------------------------------------------------
//Undo redo

void Primitive2d::keepTransformHistory() {

	//translation
	ofMatrix4x4 modelMatrixTranslationHistory;
	modelMatrixTranslationHistory.setTranslation(xOffsetTranslateHistory, yOffsetTranslateHistory, zOffsetTranslateHistory);
	xOffsetTranslateHistory = 0;
	yOffsetTranslateHistory = 0;
	zOffsetTranslateHistory = 0;

	//rotation
	ofMatrix4x4 modelMatrixRotationHistory;	
	modelMatrixRotationHistory.setRotate(curRotHistory);
	ofQuaternion start;
	curRotHistory = start;

	//scale
	ofMatrix4x4 modelMatrixScaleHistory;
	modelMatrixScaleHistory.makeScaleMatrix(xOffsetScaleHistory, yOffsetScaleHistory, zOffsetScaleHistory);
	ofVec3f newOffset = localMatrixScale.getScale();
	xOffsetScaleHistory = newOffset.x;
	yOffsetScaleHistory = newOffset.y;
	zOffsetScaleHistory = newOffset.z;

	//update the stack history	
	stackHistory.push_front(modelMatrixScaleHistory);
	stackHistory.push_front(modelMatrixRotationHistory);
	stackHistory.push_front(modelMatrixTranslationHistory);


	stackRedo.clear();
	if (stackHistory.size() > 15) { //5 transformations maximum
		stackHistory.pop_back();
		stackHistory.pop_back();
		stackHistory.pop_back();
	}
}

void Primitive2d::undoTransform() {
	//prends l'inverse des derni�res transformation pour faire le undo (sauf pour le scale)

	ofVec3f newOffset;
	if (!stackHistory.empty()) {

		//translation		
		localMatrixTranslation = localMatrixTranslation * stackHistory.front().getInverse();
		newOffset = localMatrixTranslation.getTranslation();
		xOffsetTranslate = newOffset.x;
		yOffsetTranslate = newOffset.y;
		zOffsetTranslate = newOffset.z;
		stackRedo.push_front(stackHistory.front());
		stackHistory.pop_front();

		//rotation		
		localMatrixRotation = localMatrixRotation * stackHistory.front().getInverse();
		curRot = localMatrixRotation.getRotate();
		stackRedo.push_front(stackHistory.front());
		stackHistory.pop_front();

		//scale			
		stackRedo.push_front(localMatrixScale);
		localMatrixScale = stackHistory.front();
		newOffset = localMatrixScale.getScale();
		xOffsetScale = newOffset.x;
		yOffsetScale = newOffset.y;
		zOffsetScale = newOffset.z;
		stackHistory.pop_front();


		if (stackRedo.size() > 15) { //5 transformations maximum
			stackRedo.pop_back();
			stackRedo.pop_back();
			stackRedo.pop_back();
		}
	}
}

void Primitive2d::redoTransform() {

	ofVec3f newOffset;
	if (!stackRedo.empty()) {
		//scale		
		stackHistory.push_front(localMatrixScale);
		localMatrixScale = stackRedo.front();//modelMatrixScale * stackRedo.front();
		newOffset = localMatrixScale.getScale();
		xOffsetScale = newOffset.x;
		yOffsetScale = newOffset.y;
		zOffsetScale = newOffset.z;
		stackRedo.pop_front();

		//rotation		
		localMatrixRotation = localMatrixRotation * stackRedo.front();
		curRot = localMatrixRotation.getRotate();
		stackHistory.push_front(stackRedo.front());
		stackRedo.pop_front();

		//translation
		localMatrixTranslation = localMatrixTranslation * stackRedo.front();
		newOffset = localMatrixTranslation.getTranslation();
		xOffsetTranslate = newOffset.x;
		yOffsetTranslate = newOffset.y;
		zOffsetTranslate = newOffset.z;
		stackHistory.push_front(stackRedo.front());
		stackRedo.pop_front();

		if (stackHistory.size() > 15) { //5 transformations maximum
			stackHistory.pop_back();
			stackHistory.pop_back();
			stackHistory.pop_back();
		}

	}
}
void Primitive2d::resetTransform() {
	//reset toutes les rotation et scale. Ne reset pas les translation pour �viter que tous les objets se retrouve aglutiner au centre d'un coup

	stackHistory.clear();
	stackRedo.clear();
	ofQuaternion start;
	curRot = start;
	
	xOffsetScale = 1;
	yOffsetScale = 1;
	zOffsetScale = 1;
	

	xOffsetScaleHistory = 1;
	yOffsetScaleHistory = 1;
	zOffsetScaleHistory = 1;
	curRotHistory = start;

	zOffsetTranslate = 1;
	
	localMatrixTranslation.setTranslation(xOffsetTranslate, yOffsetTranslate, zOffsetTranslate);
	localMatrixRotation.setRotate(curRot);
	localMatrixScale.makeScaleMatrix(xOffsetScale, yOffsetScale, zOffsetScale);
}

ofVec3f Primitive2d::getPosition(ofMatrix4x4 modelMatrix) {
	
	ofVec3f position;	
	position = ofVec3f(0, 0, 0) * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix;		
	return position;
}
Primitive2d::~Primitive2d() {
}