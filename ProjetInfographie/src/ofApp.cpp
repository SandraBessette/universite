// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "ofApp.h"


ofApp::ofApp(int glVerMajor, int glVerMinor) {
	glVersionMajor = glVerMajor;
	glVersionMinor = glVerMinor;
	
}
//--------------------------------------------------------------
void ofApp::setup(){
	ofSetLogLevel(OF_LOG_NOTICE); // pour voir un log sur la compilation des shaders
	myScene.setGlVersion(glVersionMajor, glVersionMinor);
	myScene.setup();
	ofSetFrameRate(60);
	//Create objet primitive
	ellipse.addListener(this, &ofApp::ellipseChanged);
	rectangle.addListener(this, &ofApp::rectangleChanged);
	line.addListener(this, &ofApp::lineChanged);
	cube.addListener(this, &ofApp::cubeChanged);
	sphere.addListener(this, &ofApp::sphereChanged);
	hilbertCurve.addListener(this, &ofApp::hilbertCurveChanged);
	octaedre.addListener(this, &ofApp::octaedreChanged);
	torus.addListener(this, &ofApp::torusChanged);

	//Material
	materialNone.addListener(this, &ofApp::materialNoneChanged);
	materialBrass.addListener(this, &ofApp::materialBrassChanged);
	materialCupper.addListener(this, &ofApp::materialCupperChanged);
	materialRuby.addListener(this, &ofApp::materialRubyChanged);


	//Style setting
	filled.addListener(this, &ofApp::filledChanged);
	color.addListener(this, &ofApp::colorChanged);
	strokeWidth.addListener(this, &ofApp::strokeWidthChanged);
	strokeColor.addListener(this, &ofApp::strokeColorChanged);
	height.addListener(this, &ofApp::heightChanged);
	width.addListener(this, &ofApp::widthChanged);
	depth.addListener(this, &ofApp::depthChanged);

	//transformation
	translateXY.addListener(this, &ofApp::translateXYChanged);
	translateZ.addListener(this, &ofApp::translateZChanged);
	rotateXY.addListener(this, &ofApp::rotateXYChanged);
	rotateZ.addListener(this, &ofApp::rotateZChanged);
	rotateY.addListener(this, &ofApp::rotateYChanged);
	rotateX.addListener(this, &ofApp::rotateXChanged);
	scaleXY.addListener(this, &ofApp::scaleXYChanged);
	scaleZ.addListener(this, &ofApp::scaleZChanged);
	scaleXYZ.addListener(this, &ofApp::scaleXYZChanged);
	undo.addListener(this, &ofApp::undoChanged);
	redo.addListener(this, &ofApp::redoChanged);
	resetTransform.addListener(this, &ofApp::resetTransformChanged);

	//Camera	
	fovV.addListener(this, &ofApp::fovVChanged); 
//	fovH.addListener(this, &ofApp::fovHChanged);
	nearPlane.addListener(this, &ofApp::nearPlaneChanged);
	farPlane.addListener(this, &ofApp::farPlaneChanged);
	tilt.addListener(this, &ofApp::tiltChanged);
	pan.addListener(this, &ofApp::panChanged);
	roll.addListener(this, &ofApp::rollChanged);
	truck.addListener(this, &ofApp::truckChanged);
	boom.addListener(this, &ofApp::boomChanged);
	dolly.addListener(this, &ofApp::dollyChanged);
	rotateAround.addListener(this, &ofApp::rotateAroundChanged);
	front.addListener(this, &ofApp::frontChanged);
	back.addListener(this, &ofApp::backChanged);
	left.addListener(this, &ofApp::leftChanged);
	right.addListener(this, &ofApp::rightChanged);
	top.addListener(this, &ofApp::topChanged);
	down.addListener(this, &ofApp::downChanged);
	perspective.addListener(this, &ofApp::perspectiveChanged);
	resetCamera.addListener(this, &ofApp::resetCameraChanged);
	animationCamera.addListener(this, &ofApp::animationCameraChanged);

	//Light
	ambiantLightOn.addListener(this, &ofApp::ambiantLightOnChanged);	
	directionalLightOn.addListener(this, &ofApp::directionalLightOnChanged);
	directionalLightShow.addListener(this, &ofApp::directionalLightShowChanged);
	pointLightOn1.addListener(this, &ofApp::pointLightOn1Changed);
	pointLightShow1.addListener(this, &ofApp::pointLightShow1Changed);
	pointLightOn2.addListener(this, &ofApp::pointLightOn2Changed);
	pointLightShow2.addListener(this, &ofApp::pointLightShow2Changed);
	pointLightOn3.addListener(this, &ofApp::pointLightOn3Changed);
	pointLightShow3.addListener(this, &ofApp::pointLightShow3Changed);
	pointLightOn4.addListener(this, &ofApp::pointLightOn4Changed);
	pointLightShow4.addListener(this, &ofApp::pointLightShow4Changed);
	spotLightShow.addListener(this, &ofApp::spotLightShowChanged);
	spotLightOn.addListener(this, &ofApp::spotLightOnChanged);
	ambiantColor.addListener(this, &ofApp::ambiantColorChanged);
	directionalColor.addListener(this, &ofApp::directionalColorChanged);
	pointColor.addListener(this, &ofApp::pointColorChanged);
	spotColor.addListener(this, &ofApp::spotColorChanged);
	lightColor.addListener(this, &ofApp::lightColorChanged);

	//Curve
	createBezier.addListener(this, &ofApp::createBezierOnChanged);
	createHermite.addListener(this, &ofApp::createHermiteOnChanged);
	createCatmullRom.addListener(this, &ofApp::createCatmullRomOnChanged);
	modifiedCurve.addListener(this, &ofApp::modifiedCurveOnChanged);
	addCrtlPoint.addListener(this, &ofApp::addCrtlPointOnChanged); 
	coons.addListener(this, &ofApp::coonsOnChanged);
	animateObject.addListener(this, &ofApp::animateObjectOnChanged);
	showCatmullRom.addListener(this, &ofApp::showCatmullRomOnChanged);

	//Create Primitive
	parametersPrimitive.setName("Primitives");
	parametersPrimitive.add(ellipse.set("ellipse", false));
	parametersPrimitive.add(rectangle.set("rectangle", false));
	parametersPrimitive.add(line.set("line", false));
	parametersPrimitive.add(cube.set("cube", false));
	parametersPrimitive.add(sphere.set("sphere", false));
	parametersPrimitive.add(octaedre.set("octaedre", false));
	parametersPrimitive.add(torus.set("torus", false));
	parametersPrimitive.add(hilbertCurve.set("hilbert curve", false));	

	guiCreatePrimitive.setup(); 
	guiCreatePrimitive.setName("Create primitive");
	guiCreatePrimitive.add(parametersPrimitive);	
	//Material
	guiCreatePrimitive.add(materialLabel.setup("Material", ""));
	guiCreatePrimitive.add(materialNone.setup("natural"));
	guiCreatePrimitive.add(materialBrass.setup("brass"));
	guiCreatePrimitive.add(materialCupper.setup("cupper"));
	guiCreatePrimitive.add(materialRuby.setup("ruby"));	

	
	parametersSettings.add(filled.set("bFill", true));
	parametersSettings.add(height.set("height", 140, 0, 600));
	parametersSettings.add(width.set("width", 140, 0, 600));
	parametersSettings.add(depth.set("depth", 100, 0, 600));
	parametersSettings.add(color.set("fill color", ofColor(100, 100, 140), ofColor(0, 0), ofColor(255, 255)));
	parametersSettings.add(strokeWidth.set("stroke width", 2.0f, 0.0f, 10.0f));
	parametersSettings.add(strokeColor.set("stroke color", ofColor(100, 100, 140), ofColor(0, 0), ofColor(255, 255)));
	parametersSettings.add(backgroundColor.set("background color", ofColor(160, 160, 160, 255), ofColor(0, 0), ofColor(255, 255)));
	gui.setup("panel");
	gui.setName("Style settings");
	gui.setPosition(guiCreatePrimitive.getPosition().x, guiCreatePrimitive.getPosition().y + guiCreatePrimitive.getHeight());
	gui.add(parametersSettings);

	hideGui = false;

	
	parametersTransformation.add(allScene.set("all scene", false));
	parametersTransformation.add(translateXY.set("translateXY", false));
	parametersTransformation.add(translateZ.set("translateZ", false));
	parametersTransformation.add(rotateXY.set("rotateXY", false));
	parametersTransformation.add(rotateX.set("rotateX", false));
	parametersTransformation.add(rotateY.set("rotateY", false));
	parametersTransformation.add(rotateZ.set("rotateZ", false));
	parametersTransformation.add(scaleXYZ.set("scaleXYZ uniform", false));
	parametersTransformation.add(scaleXY.set("scaleXY", false));
	parametersTransformation.add(scaleZ.set("scaleZ", false));
	parametersTransformation.add(yGrid.set("XZGrid", true));
	parametersTransformation.add(xGrid.set("YZGrid", false));
	parametersTransformation.add(zGrid.set("XYGrid", false));
	parametersTransformation.add(locator.set("locator", true));
	guiTransformation.setup();
	guiTransformation.setName("Transformations");
	guiTransformation.setPosition(ofGetWidth()- guiTransformation.getWidth() -10, 0);
	guiTransformation.add(parametersTransformation);
	guiTransformation.add(undo.setup("undo"));
	guiTransformation.add(redo.setup("redo"));
	guiTransformation.add(resetTransform.setup("reset"));
	
	parametersCamera.add(fovV.set("Fov Vertical", 60, 0, 180));
	parametersCamera.add(nearPlane.set("near Plane", 50, 0, 90));
	parametersCamera.add(farPlane.set("far Plane", 1750, 10, 9000));
	parametersCamera.add(tilt.set("tilt (around x)", false));
	parametersCamera.add(pan.set("pan (around y)", false));
	parametersCamera.add(roll.set("roll (around z)", false));
	parametersCamera.add(truck.set("truck (translate x)", false));
	parametersCamera.add(boom.set("boom (translate y)", false));
	parametersCamera.add(dolly.set("dolly (translate z)", false));
	parametersCamera.add(rotateAround.set("rotate Around", false));
	parametersCamera.add(perspective.set("PERSPECTIVE", true));
	parametersCamera.add(front.set("FRONT", false));
	parametersCamera.add(back.set("BACK", false));
	parametersCamera.add(left.set("LEFT", false));
	parametersCamera.add(right.set("RIGHT", false));
	parametersCamera.add(top.set("TOP", false));
	parametersCamera.add(down.set("DOWN", false));
	guiCamera.setup();
	guiCamera.setName("Camera");
	guiCamera.setPosition(ofGetWidth() - guiTransformation.getWidth() -10 , guiTransformation.getHeight());
	guiCamera.add(parametersCamera);	
	guiCamera.add(animationCamera.setup("animation"));
	guiCamera.add(resetCamera.setup("reset"));
	guiCamera.minimizeAll();

	parametersLight.setName("Light");
	parametersLight.add(ambiantLightOn.set("ambiant on", true));
	parametersLight.add(directionalLightOn.set("directional on", false));
	parametersLight.add(pointLightOn1.set("point1 on", true));
	parametersLight.add(pointLightOn2.set("point2 on", true));
	parametersLight.add(pointLightOn3.set("point3 on", true));
	parametersLight.add(pointLightOn4.set("point4 on", true));
	parametersLight.add(spotLightOn.set("spot on", false));
	parametersLight.add(directionalLightShow.set("directional show", false));
	parametersLight.add(pointLightShow1.set("point1 show", false));
	parametersLight.add(pointLightShow2.set("point2 show", false));
	parametersLight.add(pointLightShow3.set("point3 show", false));
	parametersLight.add(pointLightShow4.set("point4 show", false));
	parametersLight.add(spotLightShow.set("spot show", false));
	parametersLight.add(ambiantColor.set("ambiant color", false));
	parametersLight.add(directionalColor.set("directional color", false));
	parametersLight.add(pointColor.set("point color", false));
	parametersLight.add(spotColor.set("spot color", false));
	parametersLight.add(lightColor.set("light color", ofColor(255, 255, 255), ofColor(0, 0), ofColor(255, 255)));

	guiLight.setup();
	guiLight.setName("Lights");
	guiLight.setPosition(guiCreatePrimitive.getWidth() + 30, 10);
	guiLight.add(parametersLight);
	
	guiLight.minimizeAll();

	parametersCurve.add(createBezier.set("Bezier", false));
	parametersCurve.add(createHermite.set("Hermite", false));
	parametersCurve.add(createCatmullRom.set("Catmull-Rom", false));
	parametersCurve.add(coons.set("coons surface", false));
	parametersCurve.add(modifiedCurve.set("modified curve", false)); 
	parametersCurve.add(addCrtlPoint.set("add point", false)); 
	parametersCurve.add(animateObject.set("animate Object", false));
	parametersCurve.add(showCatmullRom.set("show CatmullRom", true));
	guiCurve.setup();
	guiCurve.setName("Curve");
	guiCurve.setPosition(guiCreatePrimitive.getWidth()*2 + 40, 10);
	guiCurve.add(parametersCurve);


	guiCurve.minimizeAll();
	
	delay = 0.4;

	ofLog() << endl 
			<< "Glisser et deposer une image dans la fenetre." << endl
			<< "Glisser et deposer un modele .obj." << endl << endl
			<< "Option de touches claviers:" << endl << endl
			<< "touche delete ou supprime : Effacer un objet ou point de controle selectionn�." << endl
			<< "barre d'espacement : Exporter l'image de la scene." << endl << endl
			<< "Pour modifier une image:" << endl
			<< "a : Changer la teinte et l'opacite(menu backgroud color)" << endl
			<< "b : Mettre en noir et blanc" << endl
			<< "c : Retrouver l'image originale" << endl
			<< "d : Effacer l'image de fond" << endl
			<< "e : Changer l'opacite seulement (menu background color (a))" << endl	
			<< "f : Mod�le d'illumination de Phong" << endl
			<< "g : Mod�le d'illumination de Blinn Phong" << endl
			<< "h : Shader de g�om�trie: effet d'explosion" << endl
			<< "i : Shader de g�om�trie: normal des surfaces" << endl
			<< "j : Appliquer le filtre blur gaussien" << endl
			<< "k : Appliquer le filtre emboss" << endl
			<< "l : Appliquer le filtre sharpen" << endl
			<< "m : Appliquer le filtre edge" << endl
			<< "n : Enlever tous les filtres" << endl
			<< "o : Mettre/enlever une texture de brick sur un cube (normal mapping)" << endl;

	
}

//Create object primitive
void ofApp::ellipseChanged(bool & ellipse) {
	if(ellipse == true) {
		rectangle = false;
		line = false;
		cube = false;
		hilbertCurve = false;
		torus = false;
		octaedre = false;
		sphere = false;
		myScene.unSelectAll();
	}
		
}

void ofApp::rectangleChanged(bool & rectangle) {
	if (rectangle == true) {
		ellipse = false;
		line = false;	
		cube = false;
		hilbertCurve = false;
		torus = false;
		octaedre = false;
		sphere = false;
		myScene.unSelectAll();
	}
}

void ofApp::lineChanged(bool & line) {
	if (line == true) {
		ellipse = false;
		rectangle = false;	
		cube = false;
		hilbertCurve = false;
		torus = false;
		octaedre = false;
		sphere = false;
		myScene.unSelectAll();
	}
}

void ofApp::cubeChanged(bool & cube) {
	if (cube== true) {
		ellipse = false;
		rectangle = false;
		line = false;
		hilbertCurve = false;
		torus = false;
		octaedre = false;
		sphere = false;
		myScene.unSelectAll();
	}
}
void ofApp::sphereChanged(bool & sphere) {
	if (sphere == true) {
		ellipse = false;
		cube = false;
		rectangle = false;
		line = false;
		hilbertCurve = false;
		torus = false;
		octaedre = false;
		myScene.unSelectAll();
	}
}
void ofApp::torusChanged(bool & torus) {
	if (torus == true) {
		ellipse = false;
		rectangle = false;
		cube = false;
		line = false;
		hilbertCurve = false;
		sphere = false;
		octaedre = false;
		myScene.unSelectAll();
	}
}
void ofApp::hilbertCurveChanged(bool & hilbertCurve) {
	if (hilbertCurve == true) {
		ellipse = false;
		rectangle = false;
		line = false;
		cube = false;
		torus = false;
		octaedre = false;
		sphere = false;
		myScene.unSelectAll();
	}
}

void ofApp::octaedreChanged(bool & octaedre) {
	if (octaedre == true) {
		ellipse = false;
		rectangle = false;
		line = false;
		cube = false;
		torus = false;
		sphere = false;
		hilbertCurve = false;
		myScene.unSelectAll();
	}
}

//Material
void ofApp::materialNoneChanged() {
	myScene.setMaterial(Material3d::NONE);
}
void ofApp::materialBrassChanged() {
	myScene.setMaterial(Material3d::BRASS);
}
void ofApp::materialCupperChanged() {
	myScene.setMaterial(Material3d::CUPPER);
}
void ofApp::materialRubyChanged() {
	myScene.setMaterial(Material3d::RUBY);
}
//Style settings
void ofApp::filledChanged(bool & filled) {
	if (changedParameterFlag) {
		myScene.setSelectedFilled(filled);	
	}
}

void ofApp::colorChanged(ofColor & pColor) {
	if (changedParameterFlag) {
		myScene.setSelectedColor(color);	
	}
}
void ofApp::strokeWidthChanged(float & strokeWidth) {
	if (changedParameterFlag) {
		myScene.setSelectedStrokeWidth(strokeWidth);	
	}
}

void ofApp::strokeColorChanged(ofColor & pColor) {
	if (changedParameterFlag) {
		myScene.setSelectedStrokeColor(pColor);		
	}
}

void ofApp::heightChanged(float & height) {
	if (changedParameterFlag) {
		myScene.setSelectedHeight(height);		
	}
}

void ofApp::widthChanged(float & width) {
	if (changedParameterFlag) {
		myScene.setSelectedWidth(width);		
	}
}
void ofApp::depthChanged(float & depth) {
	if (changedParameterFlag) {
		myScene.setSelectedDepth(depth);
	}
}

//Transformations
void ofApp::translateXYChanged(bool & translate) {
	if (translate == true) {
		disableCameraButtons();
		checkTrueTransformationButtons("translateXY");
		
	}
}

void ofApp::translateZChanged(bool & translate) {
	if (translate == true) {
		disableCameraButtons();
		checkTrueTransformationButtons("translateZ");
		
	}
}

void ofApp::rotateXYChanged(bool & rotate) {
	if (rotate == true) {
		disableCameraButtons();
		checkTrueTransformationButtons("rotateXY");
	}
}
void ofApp::rotateYChanged(bool & rotate) {
	if (rotate == true) {
		disableCameraButtons();
		checkTrueTransformationButtons("rotateY");		

	}
}
void ofApp::rotateXChanged(bool & rotate) {
	if (rotate == true) {
		disableCameraButtons();
		checkTrueTransformationButtons("rotateX");	

	}
}

void ofApp::rotateZChanged(bool & rotate) {
	if (rotate == true) {
		disableCameraButtons();
		checkTrueTransformationButtons("rotateZ");		
	}
}
void ofApp::scaleXYChanged(bool & scale) {
	if (scale == true) {
		disableCameraButtons();
		checkTrueTransformationButtons("scaleXY");	

	}
}

void ofApp::scaleZChanged(bool & scale) {
	if (scale == true) {
		disableCameraButtons();
		checkTrueTransformationButtons("scaleZ");		
	}
}
void ofApp::scaleXYZChanged(bool & scale) {
	if (scale == true) {
		disableCameraButtons();
		checkTrueTransformationButtons("scaleXYZ");	
	}
}

void ofApp::undoChanged() {	
	if (allScene)
		myScene.undoTransformScene();
	else
		myScene.undoTransformLocal();
}

void ofApp::redoChanged() {
	if (allScene)
		myScene.redoTransformScene();
	else
		myScene.redoTransformLocal();
}
void ofApp::resetTransformChanged() {
	if(allScene)
		myScene.resetTransformScene();
	else
		myScene.resetTransformLocal(); //reset les transformation de rotation et scale local des objet s�lectionner

}

//Camera
void ofApp::fovVChanged(int & fovV) {
	myScene.setFovV(fovV);

}
void ofApp::nearPlaneChanged(int & nearPlane) {
	myScene.setNearPlane(nearPlane);

}

void ofApp::farPlaneChanged(int & farPlane) {
	
	myScene.setFarPlane(farPlane);

}

void ofApp::tiltChanged(bool & tilt) {
	if (tilt == true) {
		disableTransformButtons();
		checkTrueCameraButtons("tilt");
	}
}

void ofApp::panChanged(bool & pan) {
	if (pan == true) {
		disableTransformButtons();
		checkTrueCameraButtons("pan");
	}

}
void ofApp::rollChanged(bool & roll) {
	if (roll == true) {
		disableTransformButtons();
		checkTrueCameraButtons("roll");		
	}

}

void ofApp::truckChanged(bool & truck) {
	if (truck== true) {
		disableTransformButtons();
		checkTrueCameraButtons("truck");		
	}
}

void ofApp::boomChanged(bool & boom) {
	if (boom == true) {
		disableTransformButtons();
		checkTrueCameraButtons("boom");
	}
}

void ofApp::dollyChanged(bool & dolly) {
	if (dolly == true) {
		disableTransformButtons();
		checkTrueCameraButtons("dolly");		
	}

}

void ofApp::rotateAroundChanged(bool & rotateAround) {
	if (rotateAround == true) {
		disableTransformButtons();
		checkTrueCameraButtons("rotateAround");
		centerOrbitCamera = myScene.getPosition();
		myScene.getRadius(centerOrbitCamera);
	}

}

void ofApp::animationCameraChanged() {
	disableTransformButtons();
	disableCameraButtons();
	isCameraAnimated = true;
	myScene.setFovV(60);
	timeLast = ofGetElapsedTimef();
}

void ofApp::resetCameraChanged() {	
	myScene.resetCamera();
	
}

void ofApp::frontChanged(bool & front) {
	if (front == true) {
		disableCameraButtons();
		myScene.frontCamera();
		checkTrueCameraSideButtons("front");
	}
	
}
void ofApp::backChanged(bool & back) {
	if (back == true) {
		disableCameraButtons();
		myScene.backCamera();
		checkTrueCameraSideButtons("back");
	}
	
}
void ofApp::leftChanged(bool & left) {
	if (left == true) {
		disableCameraButtons();
		myScene.leftCamera();
		checkTrueCameraSideButtons("left");
	}
	
}
void ofApp::rightChanged(bool & right) {
	if (right == true) {
		disableCameraButtons();
		myScene.rightCamera();
		checkTrueCameraSideButtons("right");
	}
	
}

void ofApp::topChanged(bool & top) {
	if (top == true) {
		disableCameraButtons();
		myScene.topCamera();
		checkTrueCameraSideButtons("top");
	}	
}
void ofApp::downChanged(bool & down) {
	if (down == true) {
		disableCameraButtons();
		myScene.downCamera();
		checkTrueCameraSideButtons("down");
	}
	
}
void ofApp::perspectiveChanged(bool & perspective) {
	if (perspective == true) {
		disableCameraButtons();
		myScene.perspectiveCamera();
		checkTrueCameraSideButtons("perspective");
	}

}

//Light
void ofApp::ambiantLightOnChanged(bool & ambiantLightOn) {		
		myScene.setAmbiantLightOn(ambiantLightOn);		

}
void ofApp::directionalLightOnChanged(bool & directionalLightOn) {	
		myScene.setDirectionalLightOn(directionalLightOn);	
		if (directionalLightOn == false) {
			directionalLightShow = false;
			myScene.setDirectionalLightShow(directionalLightShow);
		}
}
void ofApp::directionalLightShowChanged(bool & directionalLightShow) {	
		
		if (directionalLightOn == true) {
			myScene.setDirectionalLightShow(directionalLightShow);
		}

}
void ofApp::pointLightOn1Changed(bool & pointLightOn1) {	
		myScene.setPointLightOn1(pointLightOn1);
		if (pointLightOn1 == false) {
			pointLightShow1 = false;
			myScene.setPointLightShow1(pointLightShow1);
		}
}
void ofApp::pointLightShow1Changed(bool & pointLightShow1) {
	
	if (pointLightOn1 == true) {
		myScene.setPointLightShow1(pointLightShow1);
	}
}
void ofApp::pointLightOn2Changed(bool & pointLightOn2) {
	myScene.setPointLightOn2(pointLightOn2);
	if(pointLightOn2 == false) {
		pointLightShow2 = false;
		myScene.setPointLightShow2(pointLightShow2);
	}
}
void ofApp::pointLightShow2Changed(bool & pointLightShow2) {
	if (pointLightOn2 == true) {
		myScene.setPointLightShow2(pointLightShow2);
	}


}
void ofApp::pointLightOn3Changed(bool & pointLightOn3) {
	myScene.setPointLightOn3(pointLightOn3);
	if (pointLightOn3 == false) {
		pointLightShow3 = false;
		myScene.setPointLightShow3(pointLightShow3);
	}
}
void ofApp::pointLightShow3Changed(bool & pointLightShow3) {
	if (pointLightOn3 == true) {
		myScene.setPointLightShow3(pointLightShow3);
	}
	
}
void ofApp::pointLightOn4Changed(bool & pointLightOn4) {
	myScene.setPointLightOn4(pointLightOn4);
	if (pointLightOn4 == false) {
		pointLightShow4 = false;
		myScene.setPointLightShow4(pointLightShow4);
	}
}
void ofApp::pointLightShow4Changed(bool & pointLightShow4) {
	if (pointLightOn4 == true) {
		myScene.setPointLightShow4(pointLightShow4);
	}
}
void ofApp::spotLightOnChanged(bool & spotLightOn) {
	myScene.setSpotLightOn(spotLightOn);
	if (spotLightOn == false) {
		spotLightShow = false;
		myScene.setSpotLightShow(spotLightShow);
	}
}
void ofApp::spotLightShowChanged(bool & spotLightShow) {
	if (spotLightOn == true) {
		myScene.setSpotLightShow(spotLightShow);
	}
}
void ofApp::ambiantColorChanged(bool & ambiantColor) {
	if (ambiantColor == true) {
		directionalColor = false;
		pointColor = false;		
		spotColor = false;
	}

}
void ofApp::directionalColorChanged(bool & directionalColor) {
	if (directionalColor == true) {
		ambiantColor = false;
		pointColor = false;
		spotColor = false;
	}

}
void ofApp::pointColorChanged(bool & pointColor) {
	if (pointColor == true) {
		ambiantColor = false;
		directionalColor = false;
		spotColor = false;
	}
}
void ofApp::spotColorChanged(bool & spotColor) {
	if (spotColor == true) {
		ambiantColor = false;
		directionalColor = false;
		pointColor = false;
	}
}
void ofApp::lightColorChanged(ofColor & lightColor) {

	if(pointColor == true) {
		myScene.setPointDiffuse(lightColor);
	}
	if (ambiantColor == true) {
		myScene.setAmbiantColor(lightColor);
	}
	if (directionalColor == true) {
		myScene.setDirectionalDiffuse(lightColor);
	}
	if (spotColor == true) {
		myScene.setSpotDiffuse(lightColor);
	}
}

//Curve
void ofApp::createBezierOnChanged(bool & createBezier) {
	if (createBezier == true) {
		createHermite = false;
		createCatmullRom = false;
		coons = false;
	}
}

void ofApp::createHermiteOnChanged(bool & createHermite) {
	if (createHermite == true) {
		createBezier = false;
		createCatmullRom = false;
		coons = false;

	}
}
void ofApp::createCatmullRomOnChanged(bool & createCatmullRom) {
	
	if (createCatmullRom == true) {
		createBezier = false;
		createHermite = false;
		coons = false;

	}
}

void ofApp::addCrtlPointOnChanged(bool & addCrtlPoint) {
	
		
}
void ofApp::coonsOnChanged(bool & coons) {
	if (coons == true) {
		createBezier = false;
		createCatmullRom = false;
		createHermite = false;

	}


}


void ofApp::modifiedCurveOnChanged(bool & modifiedCurve) {
	if (!modifiedCurve)
		myScene.unSelectCtrlPoint();
}

void ofApp::animateObjectOnChanged(bool & animateObject) {
	if (animateObject) {
		myScene.animated();
		animateObject = false;
	}
}

void ofApp::showCatmullRomOnChanged(bool & showCatmullRom) {
	if(showCatmullRom == true)
		myScene.setShowCatmullRom(true);
	else
		myScene.setShowCatmullRom(false);
		
}
//usefull for the Gui control
void ofApp::disableTransformButtons() {
	scaleXY = false;
	rotateZ = false;
	rotateXY = false;
	rotateX = false;
	rotateY = false;
	translateZ = false;
	translateXY = false;
	scaleXYZ = false;
	scaleZ = false;

}
void ofApp::disableCameraButtons() {
	pan = false;
	tilt = false;
	roll = false;
	truck = false;
	boom = false;
	dolly = false;
	rotateAround = false;
}

void ofApp::disableCameraSide() {
	front = false;
	back = false;
	left = false;
	right= false;
	top = false;
	down = false;
	perspective = false;
	
}


void ofApp::checkTrueTransformationButtons(string buttons) {
	disableTransformButtons();

	if (buttons.compare("translateXY") == 0)
		translateXY = true;
	if (buttons.compare("translateZ") == 0)
		translateZ = true;
	if (buttons.compare("rotateXY") == 0)
		rotateXY = true;
	if (buttons.compare("rotateY") == 0)
		rotateY = true;
	if (buttons.compare("rotateX") == 0)
		rotateX = true;
	if (buttons.compare("rotateZ") == 0)
		rotateZ = true;
	if (buttons.compare("scaleXYZ") == 0)
		scaleXYZ = true;
	if (buttons.compare("scaleXY") == 0)
		scaleXY = true;
	if (buttons.compare("scaleZ") == 0)
		scaleZ = true;

}

void ofApp::checkTrueCameraButtons(string buttons) {
	disableCameraButtons();	
	if (buttons.compare("pan") == 0)
		pan = true;
	if (buttons.compare("tilt") == 0)
		tilt = true;
	if (buttons.compare("roll") == 0)
		roll = true;
	if (buttons.compare("truck") == 0)
		truck = true;
	if (buttons.compare("boom") == 0)
		boom = true;
	if (buttons.compare("dolly") == 0)
		dolly = true;
	if (buttons.compare("rotateAround") == 0)
		rotateAround = true;
	
}
void ofApp::checkTrueCameraSideButtons(string buttons) {
	disableCameraSide();
	if (buttons.compare("front") == 0)
		front = true;
	if (buttons.compare("back") == 0)
		back = true;
	if (buttons.compare("left") == 0)
		left = true;
	if (buttons.compare("right") == 0)
		right= true;
	if (buttons.compare("top") == 0)
		top = true;
	if (buttons.compare("down") == 0)
		down = true;
	if (buttons.compare("perspective") == 0)
		perspective = true;


}

//--------------------------------------------------------------
void ofApp::update(){	
	
	if(isCameraAnimated) {
		timeCurrent = ofGetElapsedTimef();			
		timeElapsed += timeCurrent - timeLast;
		timeLast = timeCurrent;
		animatedCamera();
	
	}
	myScene.update();
	myScene.backgroundColor = backgroundColor;
	//ofBackground(backgroundColor);
	myScene.setShowXGrid(xGrid);
	myScene.setShowYGrid(yGrid);
	myScene.setShowZGrid(zGrid);
	myScene.setShowLocator(locator);
}

//--------------------------------------------------------------
void ofApp::draw(){
	ofClear(255);
	//if(!myScene.image.isAllocated())
	//	ofBackground(backgroundColor);

	myScene.drawScene();	
	
	if(!hideGui) {
		gui.draw();
		guiCreatePrimitive.draw();
		guiTransformation.draw();
		guiCamera.draw();
		guiLight.draw();
		guiCurve.draw();
	}
	else {
		hideGui = false;
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	
	switch (key)
	{
	case 127: // touche supprimer ou Delete
		if(modifiedCurve)
			myScene.deleteCtrlPoint();
		else
			myScene.deleteSelectedShape();		
		break;   
	case ' ':
		hideGui = true;
		draw();
		myScene.imageExport();
		break;
	case 97: //a tint the image background
		myScene.imageTint(backgroundColor.get(), backgroundColor.get().a);
		break;	
	case 98: //b render the image background in greyscale
		myScene.imageGreyScale();
		break;
	case 99: //c retreive original image			
		myScene.imageGetOriginal();
		break;
	case 100: //d  delete the image background
		myScene.imageDelete();		
		break;
	case 101: //e change opacity of the image
		myScene.imageOpacity(backgroundColor.get().a);
		backgroundColor = ofColor(255, 255, 255, backgroundColor.get().a);
		break;		
	case 102: //f change illumination model shader to Phong
		myScene.setShadersModel(ShaderType::PHONG);
		break;
	case 103: //g change illumination model shader to Blinn Phong
		myScene.setShadersModel(ShaderType::BLINNPHONG);
		break;
	case 104: //h shader de g�motrie qui "explose le model"
		//myScene.isTexture = false;
		myScene.setShadersModel(ShaderType::EXPLODEDGEOM);
		break;
	case 105: //i shader de g�motrie qui montre la normal des surfaces
		//myScene.isTexture = false;
		myScene.setShadersModel(ShaderType::SURFACENORMGEOM);
		break;
	case 106: //j appliquer le filtre blur gaussien
		myScene.embossFilterApply = false;
		myScene.sharpenFilterApply = false;
		myScene.edgeFilterApply = false;
		myScene.blurFilterApply = true;
		break;
	case 107: //k appliquer le filtre emboss
		myScene.blurFilterApply = false;
		myScene.sharpenFilterApply = false;
		myScene.edgeFilterApply = false;
		myScene.embossFilterApply = true;
		break;
	case 108: //l appliquer le filtre sharpen
		myScene.blurFilterApply = false;
		myScene.embossFilterApply = false;
		myScene.edgeFilterApply = false;
		myScene.sharpenFilterApply = true;
		break;
	case 109: //m appliquer le filtre edge
		myScene.blurFilterApply = false;
		myScene.embossFilterApply = false;
		myScene.sharpenFilterApply = false;
		myScene.edgeFilterApply = true;
		break;
	case 110: //n enlever tous les filtres 
		myScene.blurFilterApply = false;
		myScene.embossFilterApply = false;
		myScene.sharpenFilterApply = false;
		myScene.edgeFilterApply = false;
		break; 		
	case 111: //o brick texture 
		myScene.isTexture = !myScene.isTexture;
		break;
	default:	
		break;
	}


}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
	xMousePressed = x;
		yMousePressed = y;
		xMouseMove = x;
		yMouseMove = y;

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
	isDragged = true;	
	float xDelta = xMousePressed -x;
	float yDelta = yMousePressed - y;

	//Transformations
	if(translateXY == true) {		
		if (back)
			xDelta *= -1;
		if (top) {
			float inter =xDelta;
			xDelta = yDelta;
			yDelta = inter*-1;
		}
		if (down) {
			float inter = xDelta;
			xDelta = yDelta;
			yDelta = inter;
		}
		if(!allScene && modifiedCurve)
			myScene.setTranslateXYCtrlPoint(xDelta, yDelta);
		else
			myScene.setTranslateXY(xDelta, yDelta, allScene);			
	}
	if (translateZ == true) {	
		if (left)
			yDelta = xDelta*-1;
		if (right)
			yDelta = xDelta;
		if (top)
			yDelta = xDelta*-1;
		if (down)
			yDelta = xDelta;
		if (!allScene && modifiedCurve)
			myScene.setTranslateZCtrlPoint(xDelta, yDelta);
		else
			myScene.setTranslateZ(yDelta, allScene);
	}
	if (rotateXY == true) {	
		if (back)
			xDelta *= -1;
		if (top)
			xDelta *= -1;
		myScene.setRotateXY(xDelta, yDelta, delay, allScene);
	
	}
	if (rotateX == true) {
		if (down)
			yDelta = xDelta;
		if (top)
			yDelta = xDelta;
		if (right)
			yDelta *= -1;
		myScene.setRotateX(yDelta, delay, allScene);

	}
	if (rotateY == true) {
		if (back)
			xDelta *= -1;
		if (top)
			xDelta *= -1;
		myScene.setRotateY(xDelta, delay, allScene);

	}
	if (rotateZ == true) {	
		if (back)
			xDelta *= -1;
		if (left)
			xDelta = yDelta;
		if (right)
			xDelta = yDelta;
		if (top)
			xDelta = yDelta*-1;
		if (down)
			xDelta = yDelta;
		myScene.setRotateZ(xDelta, delay, allScene);
	}	
	if (scaleXY == true) {		
		myScene.setScale(-xDelta, yDelta, 0, allScene);

	}
	if (scaleZ == true) {
		if (back)
			xDelta *= -1;
		myScene.setScale(0,0,-xDelta, allScene);
	}
	if (scaleXYZ == true) {		
		myScene.setScale(yDelta, yDelta, yDelta, allScene);
	}

	//Camera
	if (pan == true) {
		myScene.setPan(xDelta *delay);
	}
	if (tilt == true) {
		myScene.setTilt(yDelta *delay);
	}
	if (roll == true) {
		myScene.setRoll(xDelta *delay);
	}
	if (truck == true) {
		myScene.setTruck(xDelta * 2 * delay);
	}
	if (boom == true) {
		myScene.setBoom(-yDelta * 2 * delay);
	}
	if (dolly == true) {
		myScene.setDolly(yDelta * 2 * delay);
	}
	if (rotateAround== true) {	
		myScene.setRotateAround(xDelta*delay, yDelta*delay, centerOrbitCamera);
	}	
	
	xMousePressed = x;
	yMousePressed = y;
	
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	int selectedIndex;	
	xMousePressed = x;
	yMousePressed = y;
	int insideCtrlPoint = false;
	if(button == 2) {	
		if (modifiedCurve)
			insideCtrlPoint = myScene.setselectedCtrlPoint(x, y);
		if(!insideCtrlPoint) {
			selectedIndex = myScene.selectShape(x, y);
			if (selectedIndex != -1) { //pas sur que c'est une bonne id�e ou n�cessaire de faire �a
				changedParameterFlag = false;		
				changedParameterFlag = true;
				ellipse = false;
				rectangle = false;
				line = false;
				cube = false;
			}
		}

	}
	if (button == 0) {
	
		if(ellipse) {
			myScene.createPrimitive2d(VectorPrimitive::ELLIPSE, ofVec2f(x, y), height, width, color, filled, strokeWidth, strokeColor);
			ellipse = false;
		}
		if (rectangle) {
			myScene.createPrimitive2d(VectorPrimitive::RECTANGLE, ofVec2f(x, y), height, width, color, filled, strokeWidth, strokeColor);
			rectangle = false;
		}
		if (line) {
			myScene.createPrimitive2d(VectorPrimitive::LINE, ofVec2f(x, y), height, width, color, filled, strokeWidth, strokeColor);
			line = false;
		}
		if (cube) {
			myScene.createPrimitive3d(VectorPrimitive3d::CUBE, ofVec2f(x, y), height, width, depth, color, filled, strokeWidth, strokeColor);
			cube = false;
		}
		if (torus) {
			myScene.createPrimitive3d(VectorPrimitive3d::TORUS, ofVec2f(x, y), height, width, depth, color, filled, strokeWidth, strokeColor);
			torus = false;
		}
		if (hilbertCurve) {
			myScene.createPrimitive3d(VectorPrimitive3d::HILBERTCURVE, ofVec2f(x, y), height, width, depth, color, filled, strokeWidth, strokeColor);
			hilbertCurve = false;
		}
		if (octaedre) {
			myScene.createPrimitive3d(VectorPrimitive3d::OCTAEDRE, ofVec2f(x, y), height, width, depth, color, filled, strokeWidth, strokeColor);
			octaedre = false;
		}
		if (sphere) {
			myScene.createPrimitive3d(VectorPrimitive3d::SPHERE, ofVec2f(x, y), height, width, depth, color, filled, strokeWidth, strokeColor);
			sphere = false;
		}
		if (createBezier) {
			myScene.createPrimitive3d(VectorPrimitive3d::BEZIER, ofVec2f(x, y), height, width, depth, color, filled, strokeWidth, strokeColor);
			createBezier = false;
		}
		if (createHermite) {
			myScene.createPrimitive3d(VectorPrimitive3d::HERMITE, ofVec2f(x, y), height, width, depth, color, filled, strokeWidth, strokeColor);
			createHermite = false;
		}
		if (createCatmullRom) {
			myScene.createPrimitive3d(VectorPrimitive3d::CATMULLROM, ofVec2f(x, y), height, width, depth, color, filled, strokeWidth, strokeColor);
			createCatmullRom = false;
		}
		if (coons) {
			myScene.createPrimitive3d(VectorPrimitive3d::COONS, ofVec2f(x, y), height, width, depth, color, filled, strokeWidth, strokeColor);
			coons = false;
		}
		if (addCrtlPoint) {
			myScene.addCtrlPoint(ofVec3f(x, y, 0));
			addCrtlPoint = false;
		}
	}
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
	//garde l'historique des transformation pour le undo
	if(allScene && isDragged && (rotateXY || rotateZ || rotateX || rotateY || translateZ|| translateXY || scaleZ || scaleXY || scaleXYZ)) {
		myScene.keepTransformHistory();
		
	}
	//garde l'historique des transformation pour le undo
	if (isDragged && (rotateXY || rotateZ || rotateX || rotateY || translateZ || translateXY || scaleZ || scaleXY || scaleXYZ)) {
		myScene.keepTransformHistoryLocal();
		
	}
	isDragged = false;
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
	ofSetWindowShape(w, h);
	guiTransformation.setPosition(ofGetWidth() - guiTransformation.getWidth() - 10, 0);
	guiCamera.setPosition(ofGetWidth() - guiTransformation.getWidth() - 10, guiTransformation.getHeight());

	myScene.fboUpdate();
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
	
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 	

	ofFile file(dragInfo.files.at(0));
	string extension = ofToLower(file.getExtension());
	
	if (extension == "jpg" || extension == "png" || extension == "gif") {

		myScene.image.load(dragInfo.files.at(0));
		myScene.image.setImageType(OF_IMAGE_COLOR_ALPHA);
		myScene.imageOriginal = myScene.image;
		myScene.imageIntermediaire = myScene.image;
		// redimensionner l'image au dimension de la fen�tre
		if (myScene.image.getWidth() > ofGetWidth() && myScene.image.getHeight() > ofGetHeight()) {
			myScene.image.resize(ofGetWidth(), ofGetHeight());
			myScene.imageOriginal.resize(ofGetWidth(), ofGetHeight());
			myScene.imageIntermediaire.resize(ofGetWidth(), ofGetHeight());
			myScene.edgeFilterApply = false;
			myScene.sharpenFilterApply = false;
			myScene.embossFilterApply = false;
			myScene.blurFilterApply = false;
		}
	}
	if (extension == "obj" ) {
		bool isSucced = model.loadModel(dragInfo.files.at(0));
		if (isSucced)
			myScene.createPrimitive3d(model, ofVec3f(0,0,0));
	}
}

void ofApp::animatedCamera() {
	if (timeElapsed <= 25.0) {
		myScene.resetCamera();	
	//prendre la position de l'objet s�lectionner. Si rien de s�lectionner, ce sera le centre de la sc�ne.
	centerOrbitCamera = myScene.getPosition();
	//prendre la distance de l'objet s�lectionn� � la cam�ra (rayon).
	myScene.getRadius(centerOrbitCamera);

	float deltaTime = ofGetFrameNum() * 0.1f;
	// tourner autour de l'objet en x.
	myScene.setRotateAround(deltaTime*2, -timeElapsed*1.5, centerOrbitCamera);
	fovOffset += 0.03;
	//s'approcher de l'objet
	myScene.setDolly(-timeElapsed*15.0);
	myScene.setFovV(fovOffset);

	}
	else {
		isCameraAnimated = false;
		timeElapsed = 0;
		myScene.resetCamera();
		myScene.setFovV(60);
		fovOffset = 60;
	}
}