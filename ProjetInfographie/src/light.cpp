// Programme fait par Sandra Bessette pour le cours IFT3100, 20 mars 2017

#include "light.h"

Light::Light() 
{
	m_ambiant = ofVec3f(0.5f,0.5f,0.5f);
	m_diffuse = ofVec3f(1.0f, 1.0f, 1.0f);
	m_specular = ofVec3f(1.0f, 1.0f, 1.0f);
	m_direction = ofVec4f(300, 0, 0, 0);
	m_position = ofVec3f(0, 0, 0);
	
	m_lightType = LightType::SPOT;
	m_constant= 1.0f;
	m_linear = 0.00014f;
	m_quadratic = 0.000001f;
	//m_color = ofColor(m_diffuse.x * 255, m_diffuse.y * 255, m_diffuse.z * 255);

	m_linear = 0.0033f;
	m_quadratic = 0.0000027f;
	cutOff = cos(ofDegToRad(12.5f));
	outerCutOff = cos(ofDegToRad(17.5f));
	m_isShow = false;
	m_isSelected = false;
	m_isLightOn = true;

	width = 30;
	heigth = 50;
	depth = 30;
	
	m_color = ofColor(255, 255, 255);
}

Light::~Light() {

}

void Light::setup() {
	
}
void Light::update() {

}
void Light::draw() {
	ofPushStyle();
	//ofEnableDepthTest();

	ofPushMatrix();
	ofMultMatrix(localMatrixTranslation);

	ofPushMatrix();
	ofMultMatrix(localMatrixRotation);

	ofPushMatrix();
	ofMultMatrix(localMatrixScale);
	ofVec3f arrowTailPoint(-heigth/2, 0, 0);
	ofVec3f arrowHeadPoint(heigth/2, 0, 0);	
	if(m_isShow) {
		switch (m_lightType)
		{
		case LightType::POINT:
			ofFill();
			ofSetColor(m_color);
			ofDrawSphere(0, 0, 0, width/2);
			break;
		case LightType::DIRECTIONAL:
			ofFill();
			ofSetColor(m_color);
			
			ofDrawArrow(arrowTailPoint, arrowHeadPoint, width / 2);
			//ofDrawBox(0, 0, 0, heigth, width/2, depth);
			break;
		case LightType::SPOT:
			ofFill();
			ofSetColor(m_color);			
			ofPushMatrix();
			ofRotateZ(-180);
			ofDrawCone(0, 0, 0, width / 2, heigth);
			ofPopMatrix();
			break;
		default:
			break;
		}
		if (m_isSelected) {
			ofNoFill();
			ofSetLineWidth(1.0);
			ofSetColor(255, 255, 255);
			drawZone();
		}	
	}
	ofPopMatrix();
	ofPopMatrix();
	ofPopMatrix();
	ofPopStyle();

}
//void draw();
void Light::drawZone() {
	switch (m_lightType)
	{
	case LightType::POINT:
		ofNoFill();
		ofSetColor(ofColor(255,255,255));		
		ofDrawBox(0, 0, 0, width, width, width);
		break;
	case LightType::DIRECTIONAL:
		ofNoFill();
		ofSetColor(ofColor(255, 255, 255));
		ofDrawBox(0, 0, 0, heigth +10, (width / 2) +10, depth +10);
		break;
	case LightType::SPOT:
		ofNoFill();
		ofSetColor(ofColor(255, 255, 255));
		ofDrawBox(0, 0, 0, (width / 2) + 20, heigth + 10, depth + 10);
		break;
	default:
		break;
	}
}

//-------------------------------------------------------------------------------------------------
//setter
void Light::setAmbiant(ofVec3f ambiant) {
	m_ambiant = ambiant;
}
void Light::setDiffuse(ofVec3f diffuse) {
	m_diffuse = diffuse;
	m_color = ofColor(diffuse.x * 255, diffuse.y * 255, diffuse.z * 255);
}
void Light::setSpecular(ofVec3f specular) {
	m_specular = specular;
}
void Light::setPosition(ofVec3f position) {
	m_position = position;
}
void Light::setDirection(ofVec4f direction) {
	m_direction = direction;
}
void Light::setLightType(LightType lightType) {
	m_lightType = lightType;
	
}
void Light::setConstant(float constant) {
	m_constant = constant;
}
void Light::setLinear(float linear) {
	m_linear = linear;
}
void Light::setQuadratic(float quadratic) {
	m_quadratic = quadratic;
}
void Light::setIsShow(bool isShow) {
	m_isShow = isShow;
}
void Light::setIsSelected(bool isSelected) {
	m_isSelected = isSelected;
}
void Light::setLightOn(bool lightOn) {
	m_isLightOn = lightOn;
}

//-------------------------------------------------------------------------------------------------
//getter
ofVec3f Light::getAmbiant() const {
	return m_ambiant;
}
ofVec3f Light::getDiffuse() const {
	return m_diffuse;
}
ofVec3f Light::getSpecular() const {
	return m_specular;
}
ofVec3f Light::getPosition() const {
	//return m_position;
	return ofVec3f(0, 0, 0)* localMatrixRotation * localMatrixTranslation;
}
ofVec4f  Light::getDirection() const {
	//return m_direction;
	return m_direction* localMatrixRotation * localMatrixTranslation;
}
LightType Light::getLightType() const {
	return m_lightType;
} 
float Light::getConstant() const {
	return m_constant;
}
float Light::getLinear() const {
	return m_linear;
}
float Light::getQuadratic() const {
	return m_quadratic;
}
bool Light::getIsShow() const {
	return m_isShow;
}
bool Light::getIsSelected() const {
	return m_isSelected;
}
bool Light::getLightOn() const {
	return m_isLightOn;
}

//-------------------------------------------------------------------------------------------------
//S�lection
bool Light::inside(int px, int py, int pz, ofMatrix4x4 modelMatrix, ofBoxPrimitive myBox, ofCamera * cam) {

	ofVec3f xSmallDistance(0, 0, 0);
	ofVec3f xBigDistance(0, 0, 0);
	ofVec3f ySmallDistance(0, 0, 0);
	ofVec3f yBigDistance(0, 0, 0);
	ofVec3f yMax;
	ofVec3f yMin;
	ofVec3f xMax;
	ofVec3f xMin;
	ofMesh mesh;
	//ofBoxPrimitive myBox(width+10, height+10, depth+10);

	ofVec3f mouse(px, py);
	mesh = myBox.getMesh();

	int n = mesh.getNumVertices();
	for (int i = 0; i < n; i++) {
		ofVec3f cur = cam->worldToScreen(mesh.getVertex(i) * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix);

		if (i == 0 || cur.x <  xSmallDistance.x) {
			xSmallDistance = cur;
		}
		if (i == 0 || cur.x >  xBigDistance.x) {
			xBigDistance = cur;
		}
		if (i == 0 || cur.y <  ySmallDistance.y) {
			ySmallDistance = cur;
		}
		if (i == 0 || cur.y >  yBigDistance.y) {
			yBigDistance = cur;
		}

	}

	if (px >= xSmallDistance.x && py >= ySmallDistance.y  && px <= xBigDistance.x  && py <= yBigDistance.y) {
		xMax = xBigDistance;
		xMin = xSmallDistance;
		yMin = ySmallDistance;
		yMax = yBigDistance;
		return true;
	}

	return false;
}
bool Light::isInsideShape(int xPressed, int yPressed, int z, ofMatrix4x4 modelMatrix, ofCamera * cam, bool changedSelection) {

	ofBoxPrimitive myBox;
	myBox.setPosition(0, 0, 0);
	//myBox.set(width + 10, height + 10, depth + 10);
	bool isInside = false;

	isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);
	switch (m_lightType)
	{
	case LightType::POINT:
		myBox.set(width, width, width);
		isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);		
		break;
	case LightType::DIRECTIONAL:		
		myBox.set(heigth + 10, (width / 2) + 10, depth + 10);
		isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);		
		break;
	case LightType::SPOT:		
		myBox.set((width / 2) + 20, heigth + 10, depth + 10);
		isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);
		break;
	default:
		break;
	}

	if (isInside && changedSelection && m_isShow)
		m_isSelected = !m_isSelected;

	return isInside;
}

//-------------------------------------------------------------------------------------------------
//Transformation

void Light::setOffsetTranslate(ofVec3f offset) {
	xOffsetTranslate += offset.x;
	yOffsetTranslate += offset.y;
	zOffsetTranslate += offset.z;	

	localMatrixTranslation.setTranslation(xOffsetTranslate, yOffsetTranslate, zOffsetTranslate);
}

void Light::setOffsetRotateXY(ofVec3f offset, float delay) {
	
	ofQuaternion yRot((offset.x)*delay, ofVec3f(0, 1, 0));
	ofQuaternion xRot((offset.y)*delay, ofVec3f(-1, 0, 0));
	curRot *= yRot*xRot;	
	localMatrixRotation.setRotate(curRot);
}

void Light::setOffsetRotateZ(ofVec3f offset, float delay) {
	ofQuaternion zRot((offset.z)*delay, ofVec3f(0, 0, 1));
	curRot *= zRot;	
	localMatrixRotation.setRotate(curRot);
}
void Light::setOffsetRotateY(ofVec3f offset, float delay) {
	ofQuaternion yRot((offset.x)*delay, ofVec3f(0, 1, 0));
	curRot *= yRot;	
	localMatrixRotation.setRotate(curRot);
}
void Light::setOffsetRotateX(ofVec3f offset, float delay) {
	ofQuaternion xRot((offset.y)*delay, ofVec3f(-1, 0, 0));
	curRot *= xRot;	
	localMatrixRotation.setRotate(curRot);
}
