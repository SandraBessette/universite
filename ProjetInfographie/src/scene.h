// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "ofMain.h"
#include "primitive2d.h"
#include "primitive3d.h"
#include "CameraScene.h"
#include "light.h"
#include <deque> 


enum class ShaderType { PHONG, BLINNPHONG, EXPLODEDGEOM, SURFACENORMGEOM};
class Scene
{
private:		

	std::vector<Light> pointLight;	
	Light directionalLight;
	Light ambiantLight;
	Light spotLight;


	std::vector<Primitive2d> primitive2dList;
	std::vector<Primitive3d> primitive3dList;
	string extension;
	

	ofMatrix4x4 modelMatrixTranslation;
	ofMatrix4x4 modelMatrixRotation;
	ofMatrix4x4 modelMatrixScale;
	ofMatrix4x4 modelviewMatrix;

	//current state of the rotation  
	ofQuaternion curRot;
	float xOffsetTranslate = 0;
	float yOffsetTranslate = 0;
	float zOffsetTranslate = 0;

	float xOffsetRotate = 0;
	float yOffsetRotate = 0;
	float zOffsetRotate = 0;

	float xOffsetScale = 1;
	float yOffsetScale = 1;
	float zOffsetScale = 1;

	ofQuaternion curRotHistory;
	float xOffsetTranslateHistory = 0;
	float yOffsetTranslateHistory = 0;
	float zOffsetTranslateHistory = 0;

	float xOffsetRotateHistory = 0;
	float yOffsetRotateHistory = 0;
	float zOffsetRotateHistory = 0;

	float xOffsetScaleHistory = 1;
	float yOffsetScaleHistory = 1;
	float zOffsetScaleHistory = 1;

	bool showxGrid;
	bool showyGrid;
	bool showzGrid;
	bool showLocator;

	CameraScene myCamera;

	std::deque<ofMatrix4x4> stackHistory;
	std::deque<ofMatrix4x4> stackRedo;

	ofNode node;

	ofShader * shader;

	ofShader * shaderColorFill;
	ofShader * shaderBlinnPhong;
	ofShader * shaderPhong;
	ofShader * shaderExplodedGeo;
	ofShader * shaderSurfaceNormGeo;
	ofShader * shaderBlinnPhongGeo;
	ofShader * shaderPhongTexture;
	int glVersionMajor;
	int glVersionMinor;
	string shaderVersion;
	ShaderType shaderType;
	
	bool isAnimated = false;
	std::vector<ofVec3f> catmullCurvePoint;
	int indexAnimated;
	int iterationAnimated;

	//filters
	ofFbo fboBlurOnePass;
	ofFbo fboBlurTwoPass;
	ofFbo fbo;
	ofFbo fbofilter;
	ofShader blurXShader;
	ofShader blurYShader;
	ofShader filtersShader;
	
	bool version3 = false;
	ofMatrix3x3 sharpenMatrix;
	ofMatrix3x3 embossMatrix;
	ofMatrix3x3 edgeMatrix;
	
public:
	ofImage image;
	ofImage imageOriginal;
	ofImage imageIntermediaire;
	bool blurFilterApply;
	bool embossFilterApply;
	bool sharpenFilterApply;
	bool edgeFilterApply;
	bool filterApply;
	bool isTexture = false;

	ofColor backgroundColor;
	Scene();
	~Scene();

	void setup();
	void fboUpdate();
	void drawScene();
	void update();
	void drawLocator();
	void setShadersUniforms();
	void setShadersTextureUniforms();
	void setShadersModel(ShaderType shaderTyp);

	//g�rer les primitives
	void unSelectAll();
	void deleteSelectedShape();	 
	void createPrimitive2d(VectorPrimitive pType, ofVec2f positionBases, float pHeight, float pWidth, ofColor pColor, bool pFilled, float pStrokeWidth, ofColor pStrokeColor);
	void createPrimitive3d(VectorPrimitive3d pType, ofVec3f positionBases, float pHeight, float pWidth, float pDepth, ofColor pColor, bool pFilled, float pStrokeWidth, ofColor pStrokeColor);
	void createPrimitive3d(ofxAssimpModelLoader pModel, ofVec3f positionBases);
	int selectShape(int xPressed, int yPressed);
	//int selectShape3d(int xPressed, int yPressed);

	//Material
	void setMaterial(Material3d typeMat);

	//Style settings
	void setSelectedFilled(bool isFilled);
	void setSelectedColor(ofColor color); 
	void setSelectedStrokeWidth(float strokeWidth);
	void setSelectedStrokeColor(ofColor color); 
	void setSelectedHeight(float height);
	void setSelectedWidth(float width);
	void setSelectedDepth(float depth);

	//GlVersionSeting
	void setGlVersion(int major, int minor);
	
	//Transformation
	void setTranslateXY(float xDelta, float yDelta, bool isAllScene);
	void setTranslateZ(float xDelta, bool isAllScene);
	void setRotateXY(float xDelta, float yDelta, float delay, bool isAllScene);
	void setRotateZ(float xDelta, float delay, bool isAllScene);
	void setRotateY(float xDelta, float delay, bool isAllScene);
	void setRotateX(float yDelta, float delay, bool isAllScene);
	void setScale(float xDelta, float yDelta, float zDelta, bool isAllScene);	
	
	//undo redo
	void keepTransformHistory();
	void undoTransformScene();
	void redoTransformScene();
	void resetTransformScene();
	void resetTransformLocal();	
	void undoTransformLocal();
	void redoTransformLocal();
	void keepTransformHistoryLocal();

	void setShowXGrid(bool isXGridShow);
	void setShowYGrid(bool isYGridShow);
	void setShowZGrid(bool isZGridShow);
	void setShowLocator(bool isLocatorShow);

	//Options pour les images
	void imageExport() const;
	void imageTint(ofColor color, int alpha);
	void imageGreyScale();
	void imageOpacity(int alpha);
	void imageGetOriginal();
	void imageDelete();

	//camera
	void setFovV(float fovV);
	void setFarPlane(int farPlane);
	void setNearPlane(int nearPlane);
	void setPan(float xDelta);
	void setTilt(float yDelta);
	void setRoll(float xDelta);
	void setBoom(float yDelta);
	void setTruck(float xDelta);
	void setDolly(float yDelta);
	void setRotateAround(float xDelta, float yDelta, ofVec3f center);
	void resetCamera();
	void frontCamera();
	void backCamera();
	void leftCamera();
	void rightCamera();
	void topCamera();
	void downCamera();
	void perspectiveCamera();
	float getRadius(ofVec3f center); // pour la fonction rotate around de la cam�ra
	ofVec3f getPosition();// pour la fonction rotate around de la cam�ra

	 //Light
	void setAmbiantLightOn(bool ambiantLightOn);
	void setDirectionalLightOn(bool directionalLightOn);
	void setDirectionalLightShow(bool directionalLightShow);
	void setPointLightOn1(bool pointLightOn1);
	void setPointLightShow1(bool pointLightShow1);
	void setPointLightOn2(bool pointLightOn2);
	void setPointLightShow2(bool pointLightShow2);
	void setPointLightOn3(bool pointLightOn3);
	void setPointLightShow3(bool pointLightShow3);
	void setPointLightOn4(bool pointLightOn4);
	void setPointLightShow4(bool pointLightShow4);
	void setSpotLightOn(bool spotLightOn);
	void setSpotLightShow(bool spotLightShow);
	void setPointDiffuse(ofColor lightColor);
	void setDirectionalDiffuse(ofColor lightColor);
	void setSpotDiffuse(ofColor lightColor);
	void setAmbiantColor(ofColor lightColor);

	//Curve
	void setTranslateXYCtrlPoint(int xDelta, int yDelta);
	void setTranslateZCtrlPoint(int xDelta, int yDelta);
	bool setselectedCtrlPoint(int xPressed, int yPressed);
	void unSelectCtrlPoint();
	void addCtrlPoint(ofVec3f pts);
	void deleteCtrlPoint();
	void animated();
	void animated(int index, int iteration);
	void setShowCatmullRom(bool isShow);
	//void fboUpdate();

};