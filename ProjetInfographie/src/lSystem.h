// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "ofMain.h"

class LSystem
{
private:
		
	//style pas utiliser pour l'instant
	ofParameter<ofColor> color;
	ofParameter<bool> filled;
	ofParameter<float> strokeWidth;
	ofParameter<ofColor> strokeColor;
	ofVec3f positionBase;

	std::vector<char> lSystemString;
	string instruction;

	ofColor c = ofColor(0);
	float distance;
	float depth;

	string createLSystem(int numIters, string axiom);
	string processString(string oldString);
	string applyRules(char ch);
	void drawLsystem(string instructions, float angle, float distance, ofShader * shader, bool isNaturelColor);

public:
	LSystem() { };
	LSystem(ofVec3f positionBases, ofColor pColor, bool pFilled, float pStrokeWidth, ofColor pStrokeColor);	
	~LSystem() { };
	void setup(float pDistance, int step);
	void draw(ofShader * shader, bool isNaturelColor);

	

};