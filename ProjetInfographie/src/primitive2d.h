// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "ofMain.h"

// �num�ration des diff�rents types de primitives vectorielles
enum class VectorPrimitive { LINE, RECTANGLE, ELLIPSE };

class Primitive2d
{
private:	
	//Style settings
	VectorPrimitive type;
	ofVec3f positionBase;
	ofParameterGroup parameters;
	ofParameter<float> height;
	ofParameter<float> width;
	ofParameter<ofColor> color;		
	ofParameter<bool> filled;
	ofParameter<float> strokeWidth;
	ofParameter<ofColor> strokeColor;
	ofParameter<ofVec2f> center;
	std::string name;
	
	bool isRotate = false;

	//Offset for transformations
	float xOffsetTranslate = 0;
	float yOffsetTranslate = 0;
	float zOffsetTranslate = 0;

	float xOffsetScale = 1;
	float yOffsetScale = 1;
	float zOffsetScale = 1;

	float xOffsetTranslateHistory = 0;
	float yOffsetTranslateHistory = 0;
	float zOffsetTranslateHistory = 0;

	float xOffsetScaleHistory = 1;
	float yOffsetScaleHistory = 1;
	float zOffsetScaleHistory = 1;

	ofMatrix4x4 localMatrixTranslation;
	ofMatrix4x4 localMatrixRotation;
	ofMatrix4x4 localMatrixScale;
	
	//current state of the rotation  
	ofQuaternion curRot;
	ofQuaternion curRotHistory;

	std::deque<ofMatrix4x4> stackHistory;
	std::deque<ofMatrix4x4> stackRedo;

	ofVboMesh meshPlane;

public:
	bool isSelected = false;	

	Primitive2d(VectorPrimitive pType, ofVec3f positionBases, float pHeight, float pWidth, ofColor pColor, bool pFilled, float pStrokeWidth, ofColor pStrokeColor);
	~Primitive2d();

	void setup();
	void draw(ofShader * shader);
	void drawZone();

	//Style settings
	void setFilled(bool isFilled);
	void setColor(ofColor pColor);
	void setStrokeWidth(float pStrokeWidth);
	void setStrokeColor(ofColor pColor);
	void setHeight(float pHeight);
	void setWidth(float pWidth);

	// S�lection
	bool inside(int px, int py, int pz, ofBoxPrimitive myBox, ofMatrix4x4 modelMatrix, ofCamera * cam);
	bool isInsideShape(int xPressed, int yPressed, int z, ofMatrix4x4 modelMatrix, ofCamera * cam, bool changedSelection = true);

	// Transformation
	void setOffsetTranslate(ofVec3f offset);
	void setOffsetRotateXY(ofVec3f offset, float delay);
	void setOffsetRotateZ(ofVec3f offset, float delay);
	void setOffsetRotateY(ofVec3f offset, float delay);
	void setOffsetRotateX(ofVec3f offset, float delay);
	void setOffsetScale(ofVec3f offset);

	// Undo redo
	void keepTransformHistory();
	void undoTransform();
	void redoTransform();
	void resetTransform();

	// Utile pour la cam�ra
	ofVec3f getPosition(ofMatrix4x4 modelMatrix);
	
};
