// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "scene.h"
#include <sstream>

Scene::Scene() {
}

Scene::~Scene() {
	delete shaderPhong;
	delete shaderExplodedGeo;
	delete shaderBlinnPhong;
	delete shaderSurfaceNormGeo;
	delete shaderPhongTexture;
}

void Scene::setup() {
	
	myCamera.setup();
	showxGrid = false;
	showyGrid = true;
	showzGrid = false;
	showLocator = true;

	shaderPhong = new ofShader();
	shaderBlinnPhong = new ofShader();
	shaderExplodedGeo = new ofShader();
	shaderSurfaceNormGeo = new ofShader();
	shaderPhongTexture = new ofShader();

	if(glVersionMajor == 3) {	
		if(glVersionMinor == 3)
			shaderVersion = "330";
		else if (glVersionMinor == 2) {
			shaderVersion = "150";
			
		}
		

		//shader pour les filtres
		blurXShader.load(
			"shader/filter/shaderBlurX.vert",
			"shader/filter/shaderBlurX.frag");
		blurYShader.load(
			"shader/filter/shaderBlurY.vert",
			"shader/filter/shaderBlurY.frag");
		filtersShader.load(
			"shader/filter/shaderFilters.vert",
			"shader/filter/shaderFilters.frag");
		blurYShader.begin();
		blurYShader.setUniform1f("blur", 1.0);
		blurYShader.end();
		blurXShader.begin();
		blurXShader.setUniform1f("blur", 1.0);
		blurXShader.end();
		filtersShader.begin();
		filtersShader.setUniform1f("offset", 1.0);
		filtersShader.end();

		//shader pout le normal mapping 
		shaderPhongTexture->load(
			"shader/" + shaderVersion + "/PhongTextureVS.glsl",
			"shader/" + shaderVersion + "/PhongTextureFS.glsl");
		
		version3 = true;
	}
	else {
		shaderVersion = "120";
	}
	shaderPhong->load(
		"shader/" + shaderVersion + "/PhongVS.glsl",
		"shader/" + shaderVersion + "/PhongFS.glsl");

	shaderBlinnPhong->load(
		"shader/" + shaderVersion + "/BlinnPhongVS.glsl",
		"shader/" + shaderVersion + "/BlinnPhongFS.glsl");
	if(glVersionMajor == 3 && glVersionMinor == 3) {
		shaderSurfaceNormGeo ->setGeometryInputType(GL_TRIANGLES);
		shaderSurfaceNormGeo ->setGeometryOutputType(GL_LINE_STRIP);
		shaderSurfaceNormGeo ->setGeometryOutputCount(6);
		shaderExplodedGeo->setGeometryInputType(GL_TRIANGLES);
		shaderExplodedGeo->setGeometryOutputType(GL_TRIANGLE_STRIP);
		shaderExplodedGeo->setGeometryOutputCount(3);
		shaderSurfaceNormGeo ->load(
			"shader/" + shaderVersion + "/VSGeom.glsl",
			"shader/" + shaderVersion + "/FSGeom.glsl", "shader/" + shaderVersion + "/Geom.glsl");
		shaderExplodedGeo->load(
				"shader/" + shaderVersion + "/PhongVS.glsl",
			"shader/" + shaderVersion + "/PhongFS.glsl", "shader/" + shaderVersion + "/PhongGeom.glsl");	

		
	}
	shader = shaderPhong;


	//initialisation des lumi�res
	for (int i = 0; i<4; i++) {
		Light light;
		pointLight.push_back(light);
	}
	

	for (int i = 0; i<pointLight.size(); i++) {
		pointLight[i].setLightType(LightType::POINT);
	}
	pointLight[0].setOffsetTranslate(ofVec3f(0, 0, -300));
	pointLight[1].setOffsetTranslate(ofVec3f(0, 0, 300));
	pointLight[2].setOffsetTranslate(ofVec3f(300, 0, 0));
	pointLight[3].setOffsetTranslate(ofVec3f(-300, 0, 0));

	directionalLight.setLightType(LightType::DIRECTIONAL);
	directionalLight.setOffsetTranslate(ofVec3f(-400, 0, 0));
	directionalLight.setLightOn(false);

	ambiantLight.setLightType(LightType::AMBIANT);

	spotLight.setLightType(LightType::SPOT);
	spotLight.setOffsetTranslate(ofVec3f(0, 300, 0));
	spotLight.setDirection(ofVec4f(0, -300, 0,0));
	spotLight.setLightOn(false);	
	
	//Filter
	fboUpdate();	

	blurFilterApply = false;
	embossFilterApply = false;
	sharpenFilterApply = false;
	edgeFilterApply = false;
	filterApply = false;

	embossMatrix = ofMatrix3x3(-2.0, -1.0, 0.0, -1.0, 1.0, 1.0, 0.0, 1.0, 2.0);
	sharpenMatrix = ofMatrix3x3(0.0, -1.0, 0.0, -1.0, 5.0, -1.0, 0.0, -1.0, 0.0);
	edgeMatrix = ofMatrix3x3(-1.0, -1.0, -1.0, -1.0, 8.0, -1.0, -1.0, -1.0,-1.0);
}
void Scene::update() {

	if (isAnimated) {		
		animated(indexAnimated, iterationAnimated);	

	}

}
void Scene::fboUpdate() {
	//Filter
	if(ofGetWidth() > 0 && ofGetHeight()> 0) {
		fboBlurOnePass.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
		fboBlurTwoPass.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
		fbo.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);
		fbofilter.allocate(ofGetWidth(), ofGetHeight(), GL_RGBA);

		fboBlurOnePass.begin();
		ofClear(255, 255, 255, 0);
		fboBlurOnePass.end();

		fboBlurTwoPass.begin();
		ofClear(255, 255, 255, 0);
		fboBlurTwoPass.end();

		fbo.begin();
		ofClear(255, 255, 255, 0);
		fbo.end();


		fbofilter.begin();
		ofClear(255, 255, 255, 0);
		fbofilter.end();
	}
}

void Scene::drawScene() {	
	if (!image.isAllocated())
		ofBackground(backgroundColor);
	if ((blurFilterApply || embossFilterApply || sharpenFilterApply || edgeFilterApply)  && version3) {
		fbo.begin();
		ofClear(0);
		 
	}	
	
	if (image.isAllocated()) {		
		ofEnableAlphaBlending();		
		image.draw(0, 0, ofGetWindowWidth(), ofGetWindowHeight());		
		ofDisableAlphaBlending();
	}
	
	
	ofEnableDepthTest();
	myCamera.camera->begin();
	myCamera.drawCamera();


	ofPushMatrix();
	ofMultMatrix(modelMatrixTranslation);

	ofPushMatrix();
	ofMultMatrix(modelMatrixRotation);

	ofPushMatrix();
	ofMultMatrix(modelMatrixScale);

	for (int i = 0; i<pointLight.size(); i++) {
		pointLight[i].draw();

	}
	directionalLight.draw();
	spotLight.draw();
	ofEnableLighting();
	
	shader->begin();
	setShadersUniforms();	
	shader->end();

	if(version3 && isTexture) {
		shaderPhongTexture->begin();
		setShadersTextureUniforms();
		shaderPhongTexture->end();
	}
	
	for (int i = 0; i<primitive3dList.size(); i++) {
		primitive3dList[i].draw(shader, version3 && isTexture, shaderPhongTexture);
	}	

	for (int i = 0; i<primitive2dList.size(); i++) {
		primitive2dList[i].draw(shader);
	}
	ofDisableLighting();
	ofDrawGrid(54, 8, false, showxGrid, showyGrid, showzGrid);

	if (showLocator)
		ofDrawRotationAxes(45);//drawLocator();
	modelviewMatrix = ofGetCurrentMatrix(OF_MATRIX_MODELVIEW);
	ofPopMatrix();
	ofPopMatrix();
	ofPopMatrix();
	myCamera.camera->end();
	ofDisableDepthTest();	

	if ((embossFilterApply || sharpenFilterApply || edgeFilterApply) && version3) {

		fbo.end();
		
		fbofilter.begin();
		ofClear(0);

		filtersShader.begin();
		
		filtersShader.setUniformTexture("fboTexture", fbo.getTextureReference(0), 0);
		if(embossFilterApply)
			filtersShader.setUniformMatrix3f("filterMatrix", embossMatrix);
		else if(sharpenFilterApply)
			filtersShader.setUniformMatrix3f("filterMatrix", sharpenMatrix);
		else if (edgeFilterApply)
			filtersShader.setUniformMatrix3f("filterMatrix", edgeMatrix);
		fbo.draw(0, 0);
		filtersShader.end();
		fbofilter.end();

		fbofilter.draw(0, 0);
	
	}
	if (blurFilterApply && version3) {
		fbo.end();

		//pass number one
		fboBlurOnePass.begin();
		ofClear(0);
		blurXShader.begin();
		blurXShader.setUniformTexture("fboTexture", fbo.getTextureReference(0), 0);
		fbo.draw(0,0);
		blurXShader.end();
		fboBlurOnePass.end();
		
		//pass number 2
		fboBlurTwoPass.begin();
		ofClear(0);
		blurYShader.begin();
		blurYShader.setUniformTexture("fboTexture", fboBlurOnePass.getTextureReference(0), 1);
		fboBlurOnePass.draw(0, 0);
		blurYShader.end();
		fboBlurTwoPass.end();

		//fboBlurOnePass.draw(0, 0);
		fboBlurTwoPass.draw(0, 0);
	

	}
}

void Scene::drawLocator()
{
	ofFill();
	ofSetColor(196);
	ofPushMatrix();
	ofScale(4, 4);
	node.draw();
	ofPopMatrix();
}

void Scene::setShadersUniforms() {
	if(shaderType != ShaderType::SURFACENORMGEOM) {
		shader->setUniform3f("lightColorAmbient", ambiantLight.getAmbiant());
		shader->setUniform1f("aLightIsOn1", ambiantLight.getLightOn());

		shader->setUniform1f("constant", pointLight[0].getConstant());
		shader->setUniform1f("linear", pointLight[0].getLinear());
		shader->setUniform1f("quadratic", pointLight[0].getQuadratic());		
		for (int i = 0; i<pointLight.size(); i++) {
			int j = i + 1;
			std::ostringstream s;
			s << j;
			shader->setUniform3f("pLightPosition" + s.str(), pointLight[i].getPosition() *ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
			shader->setUniform3f("pLightColorDiffuse" + s.str(), pointLight[i].getDiffuse());
			shader->setUniform3f("pLightColorSpecular" + s.str(), pointLight[i].getSpecular());
			shader->setUniform1f("pLightIsOn" + s.str(), pointLight[i].getLightOn());
			
		}
		shader->setUniform4f("lightDir", directionalLight.getDirection() * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
		shader->setUniform3f("dLightColorDiffuse1", directionalLight.getDiffuse());
		shader->setUniform3f("dLightColorSpecular1", directionalLight.getSpecular());
		shader->setUniform1f("dLightIsOn1", directionalLight.getLightOn());

		shader->setUniform3f("sLightPosition1", spotLight.getPosition() *ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
		shader->setUniform4f("spotLightDir", spotLight.getDirection() * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
		shader->setUniform3f("sLightColorDiffuse1", spotLight.getDiffuse());
		shader->setUniform3f("sLightColorSpecular1", spotLight.getSpecular());
		shader->setUniform1f("sLightIsOn1", spotLight.getLightOn());
	
		shader->setUniform1f("cutOff", spotLight.cutOff);
		shader->setUniform1f("outerCutOff", spotLight.outerCutOff);		
		
	}
	if (shaderType == ShaderType::EXPLODEDGEOM || shaderType == ShaderType::SURFACENORMGEOM) {
		shader->setUniform1f("time", glfwGetTime());
		shader->setUniform1f("isSphere", false);
	}
	
	
}

void Scene::setShadersTextureUniforms() {
	shaderPhongTexture->setUniform3f("lightColorAmbient", ambiantLight.getAmbiant());
	shaderPhongTexture->setUniform1f("aLightIsOn1", ambiantLight.getLightOn());

	shaderPhongTexture->setUniform1f("constant", pointLight[0].getConstant());
	shaderPhongTexture->setUniform1f("linear", pointLight[0].getLinear());
	shaderPhongTexture->setUniform1f("quadratic", pointLight[0].getQuadratic());	
	for (int i = 0; i<pointLight.size(); i++) {
		int j = i + 1;
		std::ostringstream s;
		s << j;
		shaderPhongTexture->setUniform3f("pLightPosition" + s.str(), pointLight[i].getPosition() *ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
		shaderPhongTexture->setUniform3f("pLightColorDiffuse" + s.str(), pointLight[i].getDiffuse());
		shaderPhongTexture->setUniform3f("pLightColorSpecular" + s.str(), pointLight[i].getSpecular());
		shaderPhongTexture->setUniform1f("pLightIsOn" + s.str(), pointLight[i].getLightOn());

	}
	shaderPhongTexture->setUniform4f("lightDir", directionalLight.getDirection() * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
	shaderPhongTexture->setUniform3f("dLightColorDiffuse1", directionalLight.getDiffuse());
	shaderPhongTexture->setUniform3f("dLightColorSpecular1", directionalLight.getSpecular());
	shaderPhongTexture->setUniform1f("dLightIsOn1", directionalLight.getLightOn());

	shaderPhongTexture->setUniform3f("sLightPosition1", spotLight.getPosition() *ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
	shaderPhongTexture->setUniform4f("spotLightDir", spotLight.getDirection() * ofGetCurrentMatrix(OF_MATRIX_MODELVIEW));
	shaderPhongTexture->setUniform3f("sLightColorDiffuse1", spotLight.getDiffuse());
	shaderPhongTexture->setUniform3f("sLightColorSpecular1", spotLight.getSpecular());
	shaderPhongTexture->setUniform1f("sLightIsOn1", spotLight.getLightOn());

	shaderPhongTexture->setUniform1f("cutOff", spotLight.cutOff);
	shaderPhongTexture->setUniform1f("outerCutOff", spotLight.outerCutOff);
}

//G�rer les primitives
void Scene::unSelectAll() {

	for (int i = 0; i< primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true)
			primitive2dList[i].isSelected = false;
	}
	for (int i = 0; i< primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true)
			primitive3dList[i].isSelected = false;
	}
	
	for (int i = 0; i<pointLight.size(); i++) {
		if (pointLight[i].getIsSelected() == true)
			pointLight[i].setIsSelected(false);
	}
	if (directionalLight.getIsSelected() == true)
		directionalLight.setIsSelected(false);
	
}

void Scene::deleteSelectedShape() {
	int i_delete = 0;
	if(!isAnimated) {
		while (i_delete < primitive2dList.size()) {
			if (primitive2dList[i_delete].isSelected == true) {
				primitive2dList.erase(primitive2dList.begin() + i_delete);
				i_delete = 0;
			
			}
			else
				i_delete++;
		}
		i_delete = 0;
		while (i_delete < primitive3dList.size()) {
			if (primitive3dList[i_delete].isSelected == true) {
				primitive3dList.erase(primitive3dList.begin() + i_delete);
				i_delete = 0;
			
			}
			else
				i_delete++;
		}
	}
}


void Scene::createPrimitive2d(VectorPrimitive pType, ofVec2f positionBases, float pHeight, float pWidth, ofColor pColor, bool pFilled, float pStrokeWidth, ofColor pStrokeColor) {
	
	ofVec3f vec3Result(0,0,0);
	
	Primitive2d myPrimitive(pType, vec3Result, pHeight, pWidth, pColor, pFilled, pStrokeWidth, pStrokeColor);
	myPrimitive.setup();
	primitive2dList.push_back(myPrimitive);
	
}

void Scene::createPrimitive3d(VectorPrimitive3d pType, ofVec3f positionBases, float pHeight, float pWidth, float pDepth, ofColor pColor, bool pFilled, float pStrokeWidth, ofColor pStrokeColor) {
	Primitive3d myPrimitive(pType, ofVec3f(0, 0, 0), pHeight, pWidth, pDepth, pColor, pFilled, pStrokeWidth, pStrokeColor);
	myPrimitive.setup();
	primitive3dList.push_back(myPrimitive);

	
}
void Scene::createPrimitive3d(ofxAssimpModelLoader pModel, ofVec3f positionBases) {
	Primitive3d myPrimitive(pModel, ofVec3f(0, 0, 0));
	myPrimitive.setup();
	primitive3dList.push_back(myPrimitive);
}

int Scene::selectShape(int xPressed, int yPressed) {
	bool isSelected;
	int selectedIndex = -1;	
	
	ofMatrix4x4 modelMatrix = modelMatrixScale *  modelMatrixRotation * modelMatrixTranslation;

	//Light
	for (int i = 0; i<pointLight.size(); i++) {
		if (pointLight[i].getIsShow()) {
			isSelected = pointLight[i].isInsideShape(xPressed, yPressed, 0, modelMatrix, myCamera.camera);
			if (isSelected) {
				selectedIndex = 0;
				return selectedIndex; //retourne le premier �l�ment. Permet dans s�lectionner un seul si plusieur un par dessus les autre
			}
		}
	}
	if (directionalLight.getIsShow()) {
		isSelected = directionalLight.isInsideShape(xPressed, yPressed, 0, modelMatrix, myCamera.camera);
		if (isSelected) {
			selectedIndex = 0;
			return selectedIndex; //retourne le premier �l�ment. Permet dans s�lectionner un seul si plusieur un par dessus les autre
		}
	}
	if (spotLight.getIsShow()) {
		isSelected = spotLight.isInsideShape(xPressed, yPressed, 0, modelMatrix, myCamera.camera);
		if (isSelected) {
			selectedIndex = 0;
			return selectedIndex; //retourne le premier �l�ment. Permet dans s�lectionner un seul si plusieur un par dessus les autre
		}
	}
	//Primitive
	for (int i = 0; i < primitive3dList.size(); i++) {
		isSelected = primitive3dList[i].isInsideShape(xPressed, yPressed, 0, modelMatrix, myCamera.camera);
		if (isSelected) {
			selectedIndex = i;
			return selectedIndex; //retourne le premier �l�ment. Permet dans s�lectionner un seul si plusieur un par dessus les autre
		}

	}
	for (int i = 0; i < primitive2dList.size(); i++) {
		isSelected = primitive2dList[i].isInsideShape(xPressed, yPressed, 0, modelMatrix, myCamera.camera);
		if (isSelected) {
			selectedIndex = i;
			return selectedIndex; //retourne le premier �l�ment. Permet dans s�lectionner un seul si plusieur un par dessus les autre
		}
		
	}
	
	
	return selectedIndex;
}

//-------------------------------------------------------------------------------------------------
//Material
void Scene::setMaterial(Material3d typeMat) {
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true)
			primitive3dList[i].setMaterial(typeMat);
	}
	
}
//-------------------------------------------------------------------------------------------------
//Style setting
void  Scene::setSelectedFilled(bool isFilled) {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true)
			primitive2dList[i].setFilled(isFilled);
	}
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true)
			primitive3dList[i].setFilled(isFilled);
	}
}

void Scene::setSelectedColor(ofColor color) {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true)
			primitive2dList[i].setColor(color);
	}
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true)
			primitive3dList[i].setColor(color);
	}
}
void Scene::setSelectedStrokeWidth(float strokeWidth) {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true)
			primitive2dList[i].setStrokeWidth(strokeWidth);
	}
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true)
			primitive3dList[i].setStrokeWidth(strokeWidth);
	}
}
void Scene::setSelectedStrokeColor(ofColor color) {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true)
			primitive2dList[i].setStrokeColor(color);
	}
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true)
			primitive3dList[i].setStrokeColor(color);
	}
}
void Scene::setSelectedHeight(float height) {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true)
			primitive2dList[i].setHeight(height);

	}
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true)
			primitive3dList[i].setHeight(height);

	}

}
void Scene::setSelectedWidth(float width) {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true)
			primitive2dList[i].setWidth(width);

	}
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true)
			primitive3dList[i].setWidth(width);

	}

}

void Scene::setSelectedDepth(float depth) {
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true)
			primitive3dList[i].setDepth(depth);

	}

}

void Scene::setGlVersion(int major, int minor) {
	glVersionMajor = major;
	glVersionMinor = minor;

}
//-------------------------------------------------------------------------------------------------
//Transformation

void Scene::setTranslateXY(float xDelta, float yDelta, bool isAllScene) {

	if (isAllScene) {
		xOffsetTranslate += xDelta;
		yOffsetTranslate += yDelta;
		xOffsetTranslateHistory += xDelta;
		yOffsetTranslateHistory += yDelta;
		modelMatrixTranslation.setTranslation(xOffsetTranslate, yOffsetTranslate, zOffsetTranslate);
		
	}
	else {
		for (int i = 0; i < primitive2dList.size(); i++) {
			if (primitive2dList[i].isSelected == true) {
				primitive2dList[i].setOffsetTranslate(ofVec3f(xDelta, yDelta,0));
			
			}
		}
		for (int i = 0; i < primitive3dList.size(); i++) {
			if (primitive3dList[i].isSelected == true) {
				primitive3dList[i].setOffsetTranslate(ofVec3f(xDelta, yDelta, 0));
			}
		}
		for (int i = 0; i<pointLight.size(); i++) {
			if (pointLight[i].getIsSelected() == true) {
				pointLight[i].setOffsetTranslate(ofVec3f(xDelta, yDelta, 0));
			}
		}
		if (directionalLight.getIsSelected() == true) {
			directionalLight.setOffsetTranslate(ofVec3f(xDelta, yDelta, 0));
		}
		if (spotLight.getIsSelected() == true) {
			spotLight.setOffsetTranslate(ofVec3f(xDelta, yDelta, 0));
		}
		
		
	}	

}

void Scene::setTranslateZ(float yDelta, bool isAllScene) {

	if (isAllScene) {
		zOffsetTranslate += yDelta;
		zOffsetTranslateHistory += yDelta;
		modelMatrixTranslation.setTranslation(xOffsetTranslate, yOffsetTranslate, zOffsetTranslate);
		
	}
	else {
		for (int i = 0; i < primitive2dList.size(); i++) {
			if (primitive2dList[i].isSelected == true) {
			primitive2dList[i].setOffsetTranslate(ofVec3f(0, 0, yDelta));
			

			}
		}
		for (int i = 0; i < primitive3dList.size(); i++) {
			if (primitive3dList[i].isSelected == true) {
				primitive3dList[i].setOffsetTranslate(ofVec3f(0, 0, yDelta));
			}
		}
		for (int i = 0; i<pointLight.size(); i++) {
			if (pointLight[i].getIsSelected() == true) {
				pointLight[i].setOffsetTranslate(ofVec3f(0, 0, yDelta));
			}
		}
		if (directionalLight.getIsSelected() == true) {
			directionalLight.setOffsetTranslate(ofVec3f(0, 0, yDelta));
		}
		if (spotLight.getIsSelected() == true) {
			spotLight.setOffsetTranslate(ofVec3f(0, 0, yDelta));
		}
	}	

}


void Scene::setScale(float xDelta, float yDelta, float zDelta, bool isAllScene) {

	if (isAllScene) {
		if(xDelta == yDelta && yDelta == zDelta ) { //this means we are doing an uniform scale
			if(((xOffsetScale + xDelta / 20.0) > 0) && ((yOffsetScale + yDelta / 20.0) > 0) && ((zOffsetScale + zDelta / 20.0) > 0)) {
				xOffsetScale += xDelta / 20.0;
				yOffsetScale += yDelta / 20.0;
				zOffsetScale += zDelta / 20.0;			
			}
		}
		else { //for non uniform scale transformation
			if (xOffsetScale + xDelta / 20.0 > 0) {
				xOffsetScale += xDelta/ 20.0;				
			}
			if (yOffsetScale + yDelta / 20.0 > 0) {
				yOffsetScale += yDelta / 20.0;				
			}
			if (zOffsetScale + zDelta / 20.0 > 0) {
				zOffsetScale += zDelta / 20.0;				
			}
		}
		modelMatrixScale.makeScaleMatrix(xOffsetScale, yOffsetScale, zOffsetScale);
	}
	else {
		for (int i = 0; i < primitive2dList.size(); i++) {
			if (primitive2dList[i].isSelected == true) {
				primitive2dList[i].setOffsetScale(ofVec3f(xDelta, yDelta, zDelta));

			}
		}
		for (int i = 0; i < primitive3dList.size(); i++) {
			if (primitive3dList[i].isSelected == true) {
				primitive3dList[i].setOffsetScale(ofVec3f(xDelta, yDelta, zDelta));
			}
		}
	}

}

void Scene::setRotateXY(float xDelta, float yDelta, float delay, bool isAllScene) {
	if(isAllScene) {		

		ofQuaternion yRot((xDelta)*delay, ofVec3f(0, 1, 0));
		ofQuaternion xRot((yDelta)*delay, ofVec3f(-1, 0, 0));
		curRot *= yRot*xRot;
		curRotHistory *= yRot*xRot;		
		modelMatrixRotation.setRotate(curRot);
	
	}
	else {
		for (int i = 0; i < primitive2dList.size(); i++) {
			if (primitive2dList[i].isSelected == true) {
			primitive2dList[i].setOffsetRotateXY(ofVec3f(xDelta, yDelta, 0), delay);

			}
		}
		for (int i = 0; i < primitive3dList.size(); i++) {
			if (primitive3dList[i].isSelected == true) {
				primitive3dList[i].setOffsetRotateXY(ofVec3f(xDelta, yDelta, 0), delay);
			}
		}
		if (directionalLight.getIsSelected() == true) {
			directionalLight.setOffsetRotateXY(ofVec3f(xDelta, yDelta, 0), delay);
		}
		if (spotLight.getIsSelected() == true) {
			spotLight.setOffsetRotateXY(ofVec3f(xDelta, yDelta, 0), delay);
		}
	}	

}
void Scene::setRotateZ(float xDelta, float delay, bool isAllScene) {
	if(isAllScene) {		
		ofQuaternion zRot((xDelta)*delay, ofVec3f(0, 0, 1));		
		curRot *= zRot;
		curRotHistory *= zRot;		
		modelMatrixRotation.setRotate(curRot);
		
	}
	else {
		for (int i = 0; i < primitive2dList.size(); i++) {
			if ( primitive2dList[i].isSelected == true) {
				primitive2dList[i].setOffsetRotateZ(ofVec3f(0, 0, xDelta), delay);

			}
		}
		for (int i = 0; i < primitive3dList.size(); i++) {
			if (primitive3dList[i].isSelected == true) {
				primitive3dList[i].setOffsetRotateZ(ofVec3f(0, 0, xDelta), delay);
			}
		}
		if (directionalLight.getIsSelected() == true) {
			directionalLight.setOffsetRotateZ(ofVec3f(0, 0, xDelta), delay);
		}
		if (spotLight.getIsSelected() == true) {
			spotLight.setOffsetRotateZ(ofVec3f(0, 0, xDelta), delay);
		}
	}	

}
void Scene::setRotateY(float xDelta, float delay, bool isAllScene) {
	if (isAllScene) {
		ofQuaternion yRot((xDelta)*delay, ofVec3f(0, 1, 0));
		curRot *= yRot;
		curRotHistory *= yRot;		
		modelMatrixRotation.setRotate(curRot);
	}
	else {
		for (int i = 0; i < primitive2dList.size(); i++) {
			if (primitive2dList[i].isSelected == true) {
				primitive2dList[i].setOffsetRotateY(ofVec3f(xDelta, 0,0 ), delay);

			}
		}
		for (int i = 0; i < primitive3dList.size(); i++) {
			if (primitive3dList[i].isSelected == true) {
				primitive3dList[i].setOffsetRotateY(ofVec3f(xDelta, 0, 0), delay);
			}
		}
		if (directionalLight.getIsSelected() == true) {
			directionalLight.setOffsetRotateY(ofVec3f(xDelta, 0, 0), delay);
		}
		if (spotLight.getIsSelected() == true) {
			spotLight.setOffsetRotateY(ofVec3f(xDelta, 0, 0), delay);
		}
	}

}

void Scene::setRotateX(float yDelta, float delay, bool isAllScene) {
	if (isAllScene) {
		ofQuaternion xRot((yDelta)*delay, ofVec3f(-1, 0, 0));
		curRot *= xRot;
		curRotHistory *= xRot;		
		modelMatrixRotation.setRotate(curRot);
		
	}
	else {
		for (int i = 0; i < primitive2dList.size(); i++) {
			if (primitive2dList[i].isSelected == true) {
				primitive2dList[i].setOffsetRotateX(ofVec3f(0, yDelta,0), delay);

			}
		}
		for (int i = 0; i < primitive3dList.size(); i++) {
			if (primitive3dList[i].isSelected == true) {
				primitive3dList[i].setOffsetRotateX(ofVec3f(0, yDelta, 0), delay);
			}
		}
		if (directionalLight.getIsSelected() == true) {
			directionalLight.setOffsetRotateX(ofVec3f(0, yDelta, 0), delay);
		}
		if (spotLight.getIsSelected() == true) {
			spotLight.setOffsetRotateX(ofVec3f(0, yDelta, 0), delay);
		}
	}

}




//-------------------------------------------------------------------------------------------------
//Transformation : undo redo
void Scene::keepTransformHistory() {
	
	//translation
	ofMatrix4x4 modelMatrixTranslationHistory;
	modelMatrixTranslationHistory.setTranslation(xOffsetTranslateHistory, yOffsetTranslateHistory, zOffsetTranslateHistory);
	xOffsetTranslateHistory = 0;
	yOffsetTranslateHistory = 0;
	zOffsetTranslateHistory = 0;

	//rotation
	ofMatrix4x4 modelMatrixRotationHistory;	
	modelMatrixRotationHistory.setRotate(curRotHistory);
	ofQuaternion start;
	curRotHistory = start;
	
	//scale
	ofMatrix4x4 modelMatrixScaleHistory;	
	modelMatrixScaleHistory.makeScaleMatrix(xOffsetScaleHistory, yOffsetScaleHistory, zOffsetScaleHistory);
	ofVec3f newOffset = modelMatrixScale.getScale();
	xOffsetScaleHistory = newOffset.x;
	yOffsetScaleHistory = newOffset.y;
	zOffsetScaleHistory = newOffset.z;
		
	//update the stack history	
	stackHistory.push_front(modelMatrixScaleHistory);
	stackHistory.push_front(modelMatrixRotationHistory);
	stackHistory.push_front(modelMatrixTranslationHistory);
	

	stackRedo.clear();
	if(stackHistory.size() > 15) { //5 transformations maximum
		stackHistory.pop_back();
		stackHistory.pop_back();
		stackHistory.pop_back();
	}
}

void Scene::undoTransformScene() {
//prends l'inverse des derni�res transformation pour faire le undo (sauf pour le scale)

ofVec3f newOffset;
	if (!stackHistory.empty()) {

		//translation		
		modelMatrixTranslation = modelMatrixTranslation * stackHistory.front().getInverse();
		newOffset = modelMatrixTranslation.getTranslation();
		xOffsetTranslate = newOffset.x;
		yOffsetTranslate = newOffset.y;
		zOffsetTranslate = newOffset.z;
		stackRedo.push_front(stackHistory.front());
		stackHistory.pop_front();

		//rotation		
		modelMatrixRotation = modelMatrixRotation * stackHistory.front().getInverse();
		curRot = modelMatrixRotation.getRotate();
		stackRedo.push_front(stackHistory.front());
		stackHistory.pop_front();

		//scale			
		stackRedo.push_front(modelMatrixScale);
		modelMatrixScale = stackHistory.front();	
		newOffset = modelMatrixScale.getScale();
		xOffsetScale = newOffset.x;
		yOffsetScale = newOffset.y;
		zOffsetScale = newOffset.z;	
		stackHistory.pop_front();
		

		if (stackRedo.size() > 15) { //5 transformations maximum
			stackRedo.pop_back();
			stackRedo.pop_back();
			stackRedo.pop_back();
		}
	}
}

void Scene::redoTransformScene() {

	ofVec3f newOffset;
	if (!stackRedo.empty()) {
		//scale		
		stackHistory.push_front(modelMatrixScale);
		modelMatrixScale = stackRedo.front();//modelMatrixScale * stackRedo.front();
		newOffset = modelMatrixScale.getScale();
		xOffsetScale = newOffset.x;
		yOffsetScale = newOffset.y;
		zOffsetScale = newOffset.z;		
		stackRedo.pop_front();

		//rotation		
		modelMatrixRotation = modelMatrixRotation * stackRedo.front();
		curRot = modelMatrixRotation.getRotate();
		stackHistory.push_front(stackRedo.front());
		stackRedo.pop_front();
		
		//translation
		modelMatrixTranslation = modelMatrixTranslation * stackRedo.front();
		newOffset = modelMatrixTranslation.getTranslation();
		xOffsetTranslate = newOffset.x;
		yOffsetTranslate = newOffset.y;
		zOffsetTranslate = newOffset.z;	
		stackHistory.push_front(stackRedo.front());
		stackRedo.pop_front();		

		if (stackHistory.size() > 15) { //5 transformations maximum
			stackHistory.pop_back();
			stackHistory.pop_back();
			stackHistory.pop_back();
		}

	}
}

void Scene::resetTransformScene() {
	stackHistory.clear();
	stackRedo.clear();

	ofQuaternion start;
	curRot = start;
	xOffsetTranslate = 0;
	yOffsetTranslate = 0;
	zOffsetTranslate = 0;

	xOffsetRotate = 0;
	yOffsetRotate = 0;
	zOffsetRotate = 0;

	xOffsetScale = 1;
	yOffsetScale = 1;
	zOffsetScale = 1;

	curRotHistory = start;
	xOffsetTranslateHistory = 0;
	yOffsetTranslateHistory = 0;
	zOffsetTranslateHistory = 0;

	xOffsetRotateHistory = 0;
	yOffsetRotateHistory = 0;
	zOffsetRotateHistory = 0;

	xOffsetScaleHistory = 1;
	yOffsetScaleHistory = 1;
	zOffsetScaleHistory = 1;

	modelMatrixTranslation.setTranslation(xOffsetTranslate, yOffsetTranslate, zOffsetTranslate);	
	modelMatrixRotation.setRotate(curRot);
	modelMatrixScale.makeScaleMatrix(xOffsetScale, yOffsetScale, zOffsetScale);
}

void Scene::resetTransformLocal() {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true) {
			primitive2dList[i].resetTransform();

		}
	}

	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true) {
			primitive3dList[i].resetTransform();

		}
	}

}

void Scene::undoTransformLocal() {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true) {
			primitive2dList[i].undoTransform();

		}
	}

	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true) {
			primitive3dList[i].undoTransform();

		}
	}

}

void Scene::redoTransformLocal() {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true) {
			primitive2dList[i].redoTransform();

		}
	}

	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true) {
			primitive3dList[i].redoTransform();

		}
	}

}


void Scene::keepTransformHistoryLocal() {
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true) {
			primitive2dList[i].keepTransformHistory();

		}
	}

	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true) {
			primitive3dList[i].keepTransformHistory();

		}
	}

}

void Scene::setShowXGrid(bool isXGridShow) {
	showxGrid = isXGridShow;
}
void Scene::setShowYGrid(bool isYGridShow) {
	showyGrid = isYGridShow;
}
void Scene::setShowZGrid(bool isZGridShow) {
	showzGrid = isZGridShow;
}
void Scene::setShowLocator(bool isLocatorShow) {
	showLocator = isLocatorShow;
}

//-------------------------------------------------------------------------------------------------
//Controle d'image

void Scene::imageExport() const
{
	ofImage imageTemp;
	imageTemp.grabScreen(0, 0, ofGetWindowWidth(), ofGetWindowHeight());

	ofFileDialogResult saveFileResult = ofSystemSaveDialog(ofGetTimestampString() + "." + ofToLower(extension), "Save your file");
	if (saveFileResult.bSuccess) {
		imageTemp.save(saveFileResult.filePath);

	}
}

void Scene::imageTint(ofColor color, int alpha)
{
	if (image.isAllocated()) {
		image = imageIntermediaire;
		int w = image.getWidth();
		int h = image.getHeight();
		int r;
		int g;
		int b;
		for (int x = 0; x<w; ++x) {
			for (int y = 0; y<h; ++y) {
				ofColor c = image.getColor(x, y);
				r = c.r  * color.r / 255;
				g = c.g * color.g / 255;
				b = c.b  * color.b / 255;
				image.setColor(x, y, ofColor(r, g, b, alpha));

			}
		}
	}

	image.update();

}

void Scene::imageGreyScale()
{
	//convertir l'image en teinte de gris
	if (image.isAllocated()) {
		image = imageOriginal;
		int l;
		int w = image.getWidth();
		int h = image.getHeight();
		for (int x = 0; x<w; ++x) {
			for (int y = 0; y<h; ++y) {
				ofColor c = image.getColor(x, y);
				l = 0.3*c.r + 0.59*c.g + 0.11*c.b;
				image.setColor(x, y, ofColor(l, l, l, 255));
			}
		}
	}
	image.update();
	imageIntermediaire = image;
}

void Scene::imageOpacity(int alpha) {

	if (image.isAllocated()) {

		imageTint(ofColor(255, 255, 255), alpha);
	}
	image.update();
}

void Scene::imageGetOriginal() {
	if (image.isAllocated()) {
		image = imageOriginal;
		imageIntermediaire = imageOriginal;
	}

}

void Scene::imageDelete() {
	if (image.isAllocated()) {
		image.clear();
		imageOriginal.clear();
		imageIntermediaire.clear();
		
	}
}

//-------------------------------------------------------------------------------------------------
//Camera

void Scene::setFovV(float fov) {
	myCamera.setFovV(fov);
}
void Scene::setFarPlane(int farPlane) {
	myCamera.setFarPlane(farPlane);
}
void Scene::setNearPlane(int nearPlane) {
	myCamera.setNearPlane(nearPlane);
}

void Scene::setPan(float xDelta) {
	myCamera.setPan(xDelta);
}
void Scene::setTilt(float yDelta) {
	myCamera.setTilt(yDelta);
}
void Scene::setRoll(float xDelta) {
	myCamera.setRoll(xDelta);
}

void Scene::setBoom(float yDelta) {
	myCamera.setBoom(yDelta);

}
void Scene::setTruck(float xDelta) {
	myCamera.setTruck(xDelta);

}
void Scene::setDolly(float yDelta) {
	myCamera.setDolly(yDelta);

}

void Scene::resetCamera() {
	myCamera.reset();
}

void Scene::setRotateAround(float xDelta, float yDelta, ofVec3f center) {
	myCamera.setRotateAround(xDelta, yDelta, center);
}

float Scene::getRadius(ofVec3f center) {
	return myCamera.getRadius(center);
}

ofVec3f Scene::getPosition() {
	ofMatrix4x4 modelMatrix = modelMatrixScale *  modelMatrixRotation * modelMatrixTranslation;
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true) {
			return primitive3dList[i].getPosition(modelMatrix);

		}
	}
	for (int i = 0; i < primitive2dList.size(); i++) {
		if (primitive2dList[i].isSelected == true) {
			return primitive2dList[i].getPosition(modelMatrix);

		}
	}
	ofVec3f positionVide; //0,0,0
	return positionVide;
}

void Scene::frontCamera() {
	myCamera.frontCamera();
}

void Scene::backCamera() {
	myCamera.backCamera();
}
void Scene::leftCamera() {
	myCamera.leftCamera();
}

void Scene::rightCamera() {
	myCamera.rightCamera();
}
void Scene::topCamera() {
	myCamera.topCamera();
}

void Scene::downCamera() {
	myCamera.downCamera();
}
void Scene::perspectiveCamera() {
	myCamera.perspectiveCamera();
}

//-------------------------------------------------------------------------------------------------
//Light
void Scene::setAmbiantLightOn(bool ambiantLightOn) {
	ambiantLight.setLightOn(ambiantLightOn);
}
void Scene::setDirectionalLightOn(bool directionalLightOn) {
	directionalLight.setLightOn(directionalLightOn);
}
void Scene::setDirectionalLightShow(bool directionalLightShow) {
	directionalLight.setIsShow(directionalLightShow);
}
void Scene::setPointLightOn1(bool pointLightOn1) {
	pointLight[0].setLightOn(pointLightOn1);
}
void Scene::setPointLightShow1(bool pointLightShow1) {
	pointLight[0].setIsShow(pointLightShow1);
}
void Scene::setPointLightOn2(bool pointLightOn2) {
	pointLight[1].setLightOn(pointLightOn2);
}
void Scene::setPointLightShow2(bool pointLightShow2) {
	pointLight[1].setIsShow(pointLightShow2);
}
void Scene::setPointLightOn3(bool pointLightOn3) {
	pointLight[2].setLightOn(pointLightOn3);
}
void Scene::setPointLightShow3(bool pointLightShow3) {
	pointLight[2].setIsShow(pointLightShow3);
}
void Scene::setPointLightOn4(bool pointLightOn4) {
	pointLight[3].setLightOn(pointLightOn4);
}
void Scene::setPointLightShow4(bool pointLightShow4) {
	pointLight[3].setIsShow(pointLightShow4);
}
void Scene::setSpotLightOn(bool spotLightOn) {
	spotLight.setLightOn(spotLightOn);
}
void Scene::setSpotLightShow(bool spotLightShow) {
	spotLight.setIsShow(spotLightShow);
}
void Scene::setPointDiffuse(ofColor lightColor) {
	ofVec3f theColor = ofVec3f(lightColor.r / 255.0, lightColor.g / 255.0, lightColor.b / 255.0);
	for (int i = 0; i<pointLight.size(); i++) {
		if (pointLight[i].getIsSelected() == true)
			pointLight[i].setDiffuse(theColor);
	}
}

void Scene::setDirectionalDiffuse(ofColor lightColor) {
	ofVec3f theColor = ofVec3f(lightColor.r / 255.0, lightColor.g / 255.0, lightColor.b / 255.0);	
	directionalLight.setDiffuse(theColor);	
}

void Scene::setSpotDiffuse(ofColor lightColor) {
	ofVec3f theColor = ofVec3f(lightColor.r / 255.0, lightColor.g / 255.0, lightColor.b / 255.0);
	spotLight.setDiffuse(theColor);
}

void Scene::setAmbiantColor(ofColor lightColor) {
	ofVec3f theColor = ofVec3f(lightColor.r / 255.0, lightColor.g / 255.0, lightColor.b / 255.0);
	ambiantLight.setAmbiant(theColor);
}

void Scene::setShadersModel(ShaderType shaderTyp) {	

	
	switch (shaderTyp)
	{
	case ShaderType::PHONG:
		shaderType = shaderTyp;
		shader = shaderPhong;
		break;

	case ShaderType::BLINNPHONG:
		shaderType = shaderTyp;
		shader = shaderBlinnPhong;
		break;

	case ShaderType::EXPLODEDGEOM:
		if (glVersionMajor == 3 && glVersionMinor == 3) {
			shader = shaderExplodedGeo;
			shaderType = shaderTyp;
		}
		break;

	case ShaderType::SURFACENORMGEOM:
		if (glVersionMajor == 3 && glVersionMinor == 3) {
			shader = shaderSurfaceNormGeo;
			shaderType = shaderTyp;
		}
		break;

	default:
		shader = shaderPhong;
		shaderType = ShaderType::PHONG;
		break;
	}
}

//Curve
void Scene::setTranslateXYCtrlPoint(int xDelta, int yDelta) {
	/*ofQuaternion rotation = myCamera.camera->getGlobalOrientation();
	ofMatrix4x4 rotationMat;
	rotationMat.makeRotationMatrix(rotation);	*/	
	
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true) {
			
			primitive3dList[i].setTranslateCtrlPoint(ofVec3f(xDelta, yDelta, 0));
		}
	}
}

void Scene::setTranslateZCtrlPoint(int xDelta, int yDelta) {
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected == true) {
			primitive3dList[i].setTranslateCtrlPoint(ofVec3f(0,0, yDelta));
		}
	}
}

bool Scene::setselectedCtrlPoint(int xPressed, int yPressed) {
	bool isSelected = false;
	int selectedIndex = -1;

	ofMatrix4x4 modelMatrix = modelMatrixScale *  modelMatrixRotation * modelMatrixTranslation;
	for (int i = 0; i < primitive3dList.size(); i++) {
		isSelected = primitive3dList[i].setselectedCtrlPoint(xPressed, yPressed, 0, modelMatrix, myCamera.camera);
		if (isSelected) {			
			return isSelected; //retourne le premier �l�ment. Permet dans s�lectionner un seul si plusieur un par dessus les autre
		}

	}
	return isSelected;
}

void Scene::unSelectCtrlPoint() {
	for (int i = 0; i < primitive3dList.size(); i++) {
		primitive3dList[i].unSelectCtrlPoint();
		
		}
}

void Scene::addCtrlPoint(ofVec3f pts) {
	for (int i = 0; i < primitive3dList.size(); i++) {
		ofMatrix4x4 modelMatrix = modelMatrixScale *  modelMatrixRotation * modelMatrixTranslation;		
		ofVec3f result =  myCamera.camera->screenToWorld(ofVec3f(pts.x, pts.y,0.92) * modelMatrix) ;
		if(primitive3dList[i].isSelected)
			primitive3dList[i].addCtrlPoint(pts, modelMatrix, myCamera.camera);

	}
}

void Scene::deleteCtrlPoint() {
	for (int i = 0; i < primitive3dList.size(); i++) {		
		if (primitive3dList[i].isSelected)
			primitive3dList[i].deleteCtrlPoint();

	}

}

void Scene::animated() {	
	
	indexAnimated = -1;
	bool curveSelected = false;
	iterationAnimated = 0;
	
	for (int i = 0; i < primitive3dList.size(); i++) {
		if (primitive3dList[i].isSelected) {
			if (primitive3dList[i].get3dType() == VectorPrimitive3d::CATMULLROM) {
				catmullCurvePoint.clear();
				catmullCurvePoint = primitive3dList[i].getLineMeshCurve();
				curveSelected = true;
			}
			else
				indexAnimated = i;
		}
	}	
	
	if(indexAnimated != -1 && curveSelected) {		
		isAnimated = true;		
		primitive3dList[indexAnimated].isSelected = false;
	}
	
}


void Scene::animated(int index, int iteration) {
	if (iterationAnimated < catmullCurvePoint.size()) {
		primitive3dList[index].setPosition(catmullCurvePoint[iteration]);
		ofVec3f orientation;
		if (iteration == 0)
			primitive3dList[index].setOrientation(catmullCurvePoint[iteration], (catmullCurvePoint[iteration + 2]));
		else if(iteration < catmullCurvePoint.size()-2)
			primitive3dList[index].setOrientation(catmullCurvePoint[iteration],(catmullCurvePoint[iteration+1] ));
		iterationAnimated +=3;
	}
	else {
		isAnimated = false;	
		primitive3dList[indexAnimated].isSelected = true;

	}
}

void Scene::setShowCatmullRom(bool isShow) {
	for (int i = 0; i < primitive3dList.size(); i++) {		
			if (primitive3dList[i].get3dType() == VectorPrimitive3d::CATMULLROM) 
				primitive3dList[i].showCatmullRom(isShow);
			
			
	}

}
