// Programme fait par Sandra Bessette pour le cours IFT3100, 20 mars 2017

#include "ofMain.h"


enum class LightType { POINT, DIRECTIONAL, AMBIANT, SPOT };
class Light
{
private:
	ofVec3f m_ambiant; 
	ofVec3f m_diffuse;
	ofVec3f m_specular;
	ofVec4f m_direction;
	ofVec3f m_position;
	ofColor m_color;
	LightType m_lightType;

	

	float m_constant;
	float m_linear;
	float m_quadratic;

	bool m_isShow;
	bool m_isSelected;
	bool m_isLightOn;

	float width;
	float heigth;
	float depth;

	//Offset for transformations
	float xOffsetRotate = 0;
	float yOffsetRotate = 0;
	float zOffsetRotate = 0;

	float xOffsetTranslate = 0;
	float yOffsetTranslate = 0;
	float zOffsetTranslate = 0;

	float xOffsetScale = 1;
	float yOffsetScale = 1;
	float zOffsetScale = 1;

	//current state of the rotation  
	ofQuaternion curRot;

	ofMatrix4x4 localMatrixTranslation;
	ofMatrix4x4 localMatrixRotation;
	ofMatrix4x4 localMatrixScale;

public:
	//spot
	float cutOff;
	float outerCutOff;
	Light();
	~Light();
	void setup();
	void update();
	//void draw(ofShader * shader);
	void draw();
	void drawZone();
	//setter
	void setAmbiant(ofVec3f ambiant);
	void setDiffuse(ofVec3f diffuse);
	void setSpecular(ofVec3f specular);
	void setPosition(ofVec3f position);
	void setDirection(ofVec4f direction);
	void setLightType(LightType lightType);
	void setConstant(float constant);
	void setLinear(float linear);
	void setQuadratic(float quadratic);
	void setIsShow(bool isShow);
	void setIsSelected(bool isSelected);
	void setLightOn(bool lightOn);
	//getter
	ofVec3f getAmbiant() const;
	ofVec3f getDiffuse() const;
	ofVec3f getSpecular() const;
	ofVec3f getPosition() const;
	ofVec4f  getDirection() const;
	LightType getLightType() const;
	float getConstant() const;
	float getLinear() const;
	float getQuadratic() const;
	bool getIsShow() const;
	bool getIsSelected() const;
	bool getLightOn() const;

	//Selections
	bool inside(int px, int py, int pz, ofMatrix4x4 modelMatrix, ofBoxPrimitive myBox, ofCamera * cam);	
	bool isInsideShape(int xPressed, int yPressed, int z, ofMatrix4x4 modelMatrix, ofCamera * cam, bool changedSelection = true);

	//Transformations
	void setOffsetTranslate(ofVec3f offset);
	void setOffsetRotateXY(ofVec3f offset, float delay);
	void setOffsetRotateZ(ofVec3f offset, float delay);
	void setOffsetRotateY(ofVec3f offset, float delay);
	void setOffsetRotateX(ofVec3f offset, float delay);	


};