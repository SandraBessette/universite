// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "cameraScene.h"

void CameraScene::setup() {
	// param�tres
	cameraFov = 60.0f;
	cameraNear = 50.0f;
	cameraFar = 1750.0f;


	cameraTarget = { 0.0f, 0.0f, 0.0f };
	
	// cam�ra par d�fault
	cameraActive = Camera::PERSPECTIVE;

	reset();
	cameraSetup();
}

void CameraScene::update() {

}
void CameraScene::drawCamera() {


	if (cameraActive == Camera::FRONT)
		cameraFront.draw();
	else if (cameraActive == Camera::BACK)
		cameraBack.draw();
	else if (cameraActive == Camera::LEFT)
		cameraLeft.draw();
	else if (cameraActive == Camera::RIGHT)
		cameraRight.draw();
	else if (cameraActive == Camera::TOP)
		cameraTop.draw();
	else if (cameraActive == Camera::DOWN)
		cameraDown.draw();	
	else {		
		
		cameraPerspective.draw();
	}
}
void CameraScene::reset() {	

	cameraTarget = { 0.0f, 0.0f, 0.0f };	
		
	xOffset = 180;
	yOffset = 0;
	xOffsetback = 0;
	yOffsetback = 0;
	xOffsetLeft = -90;
	yOffsetLeft = 0;
	xOffsetRight = 90;
	yOffsetRight = 0;
	xOffsetTop = -90;
	yOffsetTop = -90;
	xOffsetDown = 90;
	yOffsetDown = 90;
	xOffsetPerspective = 180;
	yOffsetPerspective = -17;
	cameraOffset = 800;

	// position initiale de chaque cam�ra
	cameraPerspective.setPosition(0, 214, -770);
	cameraFront.setPosition(0, 0, -cameraOffset);
	cameraBack.setPosition(0, 0, cameraOffset);
	cameraLeft.setPosition(-cameraOffset, 0, 0);
	cameraRight.setPosition(cameraOffset, 0, 0);
	cameraTop.setPosition(0, cameraOffset, 0);
	cameraDown.setPosition(0, -cameraOffset, 0);
	
	// orientation de chaque cam�ra
	cameraPerspective.lookAt(cameraTarget);
	cameraFront.lookAt(cameraTarget);
	cameraBack.lookAt(cameraTarget);
	cameraLeft.lookAt(cameraTarget);
	cameraRight.lookAt(cameraTarget);
	cameraTop.lookAt(cameraTarget, ofVec3f(1, 0, 0));
	cameraDown.lookAt(cameraTarget, ofVec3f(1, 0, 0));

	

}

void CameraScene::cameraSetup()
{
	switch (cameraActive)
	{
	case Camera::PERSPECTIVE:
		camera = &cameraPerspective;		

		break;

	case Camera::FRONT:
		camera = &cameraFront;		
		
		break;

	case Camera::BACK:
		camera = &cameraBack;	
		
		break;

	case Camera::LEFT:
		camera = &cameraLeft;		
		break;

	case Camera::RIGHT:
		camera = &cameraRight;		
		break;

	case Camera::TOP:
		camera = &cameraTop;		
		break;

	case Camera::DOWN:
		camera = &cameraDown;		
		break;

	default:	// au cas que aucune camera ne soit s�lectionner	
		camera = &cameraPerspective;		
		cameraActive = Camera::PERSPECTIVE;
		break;
	}

	cameraPosition = camera->getPosition();

	cameraOrientation = camera->getOrientationQuat();

	
	camera->setupPerspective(false, cameraFov, cameraNear, cameraFar, ofVec2f(0, 0));
	

	camera->setPosition(cameraPosition);
	camera->setOrientation(cameraOrientation);
		
}


void CameraScene::setFovV(float fov) {
	camera->setFov(fov);

}

void CameraScene::setFarPlane(int farPlane) {
	camera->setFarClip(farPlane);
	cameraFar = farPlane;
}
void CameraScene::setNearPlane(int nearPlane) {
	camera->setNearClip(nearPlane);
	cameraNear = nearPlane;

}

void CameraScene::setPan(float xDelta) {
	camera->pan(xDelta);
	
}
void CameraScene::setRoll(float xDelta) {
	camera->roll(xDelta);
	
}
void CameraScene::setTilt(float yDelta) {
	camera->tilt(yDelta);

}
void CameraScene::setBoom(float yDelta) {
	camera->boom(yDelta);

}
void CameraScene::setTruck(float xDelta) {
	camera->truck(xDelta);

}
void CameraScene::setDolly(float yDelta) {
	camera->dolly(yDelta);

}

void CameraScene::setRotateAround(float xDelta, float yDelta, ofVec3f center) {
	ofMatrix4x4 matrice;
	ofQuaternion yRot;
	ofQuaternion xRot;
	if (cameraActive == Camera::FRONT) {	
		xOffset += xDelta;
		yOffset += yDelta;
		yRot = ofQuaternion((xOffset), ofVec3f(0, 1, 0));
		xRot = ofQuaternion((yOffset), ofVec3f(1, 0, 0));
	
	}
	else if (cameraActive == Camera::BACK) {
		xOffsetback += xDelta;
		yOffsetback += yDelta;
		yRot = ofQuaternion((xOffsetback), ofVec3f(0, 1, 0));
		xRot = ofQuaternion((yOffsetback), ofVec3f(1, 0, 0));		
	}
	else if (cameraActive == Camera::LEFT) {
		xOffsetLeft += xDelta;
		yOffsetLeft += yDelta;
		yRot = ofQuaternion((xOffsetLeft), ofVec3f(0, 1, 0));
		xRot = ofQuaternion((yOffsetLeft), ofVec3f(1, 0, 0));
	}
	else if (cameraActive == Camera::RIGHT) {
		xOffsetRight += xDelta;
		yOffsetRight += yDelta;
		yRot = ofQuaternion((xOffsetRight), ofVec3f(0, 1, 0));
		xRot = ofQuaternion((yOffsetRight), ofVec3f(1, 0, 0));
	}
	else if (cameraActive == Camera::TOP) {
		xOffsetTop += xDelta;
		yOffsetTop += yDelta;
		yRot = ofQuaternion((xOffsetTop), ofVec3f(0, 1, 0));
		xRot = ofQuaternion((yOffsetTop), ofVec3f(1, 0, 0));
	}

	else if (cameraActive == Camera::DOWN) {
		xOffsetDown+= xDelta;
		yOffsetDown += yDelta;
		yRot = ofQuaternion((xOffsetDown), ofVec3f(0, 1, 0));
		xRot = ofQuaternion((yOffsetDown), ofVec3f(1, 0, 0));
	}

	else{ // camera perspective
		cameraActive = Camera::PERSPECTIVE;
		xOffsetPerspective += xDelta;
		yOffsetPerspective += yDelta;
		yRot = ofQuaternion((xOffsetPerspective), ofVec3f(0, 1, 0));
		xRot = ofQuaternion((yOffsetPerspective), ofVec3f(1, 0, 0));		

	}

	
	ofQuaternion curRot2 = xRot*yRot;	

	ofVec4f p = ofVec4f(0, 0, 1, 0);
	
	matrice.setRotate(curRot2);
	p = p * matrice;
	p = p * radius;

	
	camera->setGlobalPosition(center + p);
	camera->setOrientation(curRot2);
	
}

float CameraScene::getRadius(ofVec3f center) {
	ofVec3f position = camera->getPosition();
	radius = center.distance(position);
	return radius;
}
void CameraScene::frontCamera() {
	cameraActive = Camera::FRONT;
	cameraSetup();
}

void CameraScene::backCamera() {
	cameraActive = Camera::BACK;
	cameraSetup();
}

void CameraScene::leftCamera() {
	cameraActive = Camera::LEFT;
	cameraSetup();
}

void CameraScene::rightCamera() {
	cameraActive = Camera::RIGHT;
	cameraSetup();
}
void CameraScene::topCamera() {
	cameraActive = Camera::TOP;
	cameraSetup();
}

void CameraScene::downCamera() {
	cameraActive = Camera::DOWN;
	cameraSetup();
}

void CameraScene::perspectiveCamera() {
	cameraActive = Camera::PERSPECTIVE;
	cameraSetup();
}

