// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "lSystem.h"

LSystem::LSystem(ofVec3f positionBases,  ofColor pColor,bool pFilled, float pStrokeWidth, ofColor pStrokeColor) : positionBase(positionBases),
					color(pColor), filled(pFilled), strokeWidth(pStrokeWidth), strokeColor(pStrokeColor)
{
	
}

void LSystem::setup(float pDistance, int step) {
	
	distance = pDistance;
	depth= step;
	instruction = createLSystem(3, "X");

}

void LSystem::draw(ofShader * shader, bool isNaturelColor) {
	ofPushStyle();
	ofPushMatrix();	
	ofTranslate(-distance*7/2, distance * 7/2, -distance * 7 / 2);
	drawLsystem(instruction, 90, distance, shader, isNaturelColor);
	ofPopMatrix();
	ofPopStyle();
}

string LSystem::createLSystem(int numIters, string axiom)
{
	string startString(axiom);
	string endString("");
	for (int i = 0; i < numIters; i++) {
		endString = processString(startString);
		startString = endString;
	}
	return endString;
}

string LSystem::processString(string oldString)
{
	string newstr = "";
	for (int i = 0; i < oldString.length(); i++) {
		newstr = newstr + applyRules(oldString[i]);
	}
	return newstr;
}

string LSystem::applyRules(char ch)
{
	string newstr = "";
	stringstream ss;
	string target;
	ss << ch;
	ss >> target;
	if (target == "X")
		newstr = "^<XF^<XFX-F^>>XFX&F+>>XFX-F>X->"; //Rule 1													
	
	else
		newstr = ch;  

	return newstr;
}

void LSystem::drawLsystem(string instructions, float angle, float distance, ofShader * shader, bool isNaturelColor)
{
	c.setHsb(0, 255, 255);	
	float lineWidth = 1;
	float colorOffset =instructions.length() / 255.0;
	float color3 = instructions.length() / 3.0;
	ofSetLineWidth(lineWidth);
	int color2 = 0;
	float interator = 0;

	for (int i = 0; i < instructions.length(); i++) {
		char cmd = instructions[i];
		interator++;
		if (interator >= colorOffset) {
			color2++;
			interator = 0;			

		}
		c.setHsb(color2 % 360, 255, 255);
		ofSetColor(c);
		if (cmd == 'F') {					

			ofTranslate(0, distance / -2, 0);
			if(isNaturelColor) {
				shader->begin();
				shader->setUniform3f("materialColorAmbient", ofVec3f(c.r/255.0, c.g / 255.0, c.b / 255.0));
				shader->setUniform3f("materialColorDiffuse", ofVec3f(c.r / 255.0, c.g / 255.0, c.b / 255.0));
				shader->setUniform3f("materialColorSpecular", ofVec3f(0.5f, 0.5f, 0.5f));
				shader->setUniform1f("materialBrightness",32.0);			
				ofDrawBox(0, 0, 0, distance / 6, distance, distance / 6);
				shader->end();
			}
			else {
				ofDrawBox(0, 0, 0, distance / 6, distance, distance / 6);
			}
			ofNoFill();
			ofSetLineWidth(1);
			ofSetColor(0);			
			ofDrawBox(0, 0, 0, distance / 6, distance, distance / 6);					
			ofTranslate(0, -distance / 2, 0);
			
			ofFill();

		}		
		else if (cmd == '+') {
			
			ofRotateX(-angle);							  
			
		}		
		else if (cmd == 'X') {

		}
		else if (cmd == '-') {
			
			ofRotateX(angle);						 
			
		}
		else if (cmd == '^') {
			
			ofRotateZ(angle);							
			
		}
		else if (cmd == '&') {
			
			ofRotateZ(-angle);
							  
			
		}
		else if (cmd == '>') {
			
			ofRotateY(+angle);							  
			
		}
		else if (cmd == '<') {			
			ofRotateY(-angle);							 

		}	

		
	}
}