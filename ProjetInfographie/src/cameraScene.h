// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "ofMain.h"

enum class Camera { FRONT, BACK, LEFT, RIGHT, TOP, DOWN, PERSPECTIVE };

class CameraScene
{
private:	
	Camera cameraActive;

	ofCamera cameraFront;
	ofCamera cameraPerspective;
	ofCamera cameraBack;
	ofCamera cameraLeft;
	ofCamera cameraRight;
	ofCamera cameraTop;
	ofCamera cameraDown;		

	ofQuaternion cameraOrientation;
	ofQuaternion curRot;

	ofVec3f cameraPosition;
	ofVec3f cameraTarget;

	//Offset pour la fonction setRotateAround (orbit de la cam�ra)
	float xOffset;	
	float yOffset;
	float xOffsetback;
	float yOffsetback;
	float xOffsetLeft;
	float yOffsetLeft;
	float xOffsetRight;
	float yOffsetRight;
	float xOffsetTop;
	float yOffsetTop;
	float xOffsetDown;
	float yOffsetDown;
	float xOffsetPerspective;
	float yOffsetPerspective;

	float radius;		

	float cameraFov;
	float cameraNear;
	float cameraFar;
	float cameraOffset;

	float fovDelta;
public:
	ofCamera * camera;

	CameraScene() { };

	~CameraScene() { };


	void setup();

	void update();

	void drawCamera();

	void reset();
	void cameraSetup();

	void setFovV(float fov);
	void setFarPlane(int farPlane);
	void setNearPlane(int nearPlane);
	void setPan(float xDelta);
	void setTilt(float yDelta);
	void setRoll(float xDelta);
	void setBoom(float yDelta);
	void setTruck(float xDelta);
	void setDolly(float yDelta);
	void setRotateAround(float xDelta, float yDelta, ofVec3f center);
	float getRadius(ofVec3f center);
	void perspectiveCamera();
	void frontCamera();
	void backCamera();
	void leftCamera();
	void rightCamera();
	void topCamera();
	void downCamera();



};