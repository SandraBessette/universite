// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#pragma once

#include "ofMain.h"
#include "scene.h"
#include "ofxGui.h"
#include <vector>

class ofApp : public ofBaseApp{

	public:

		float timeCurrent;
		float timeLast;
		float timeElapsed = 0;
		float fovOffset = 60;
		bool isCameraAnimated = false;

		int xMousePressed;
		int yMousePressed;
		int xMouseMove;
		int yMouseMove;
		bool hideGui;
		bool isDragged = false;

		int glVersionMajor;
		int glVersionMinor;
		ofVec3f centerOrbitCamera;

		//slows down movement
		float delay;

		Scene myScene;
		
		//Create Primitive
		ofParameterGroup parametersPrimitive;
		ofxPanel guiCreatePrimitive;
		ofParameter<bool> ellipse;
		ofParameter<bool> rectangle;
		ofParameter<bool> line;
		ofParameter<bool> cube;
		ofParameter<bool> sphere;
		ofParameter<bool> torus;
		ofParameter<bool> octaedre;
		ofParameter<bool> hilbertCurve;
		//Material
		ofParameterGroup parametersMaterial;
		ofxLabel materialLabel;
		ofxButton materialNone;
		ofxButton materialBrass;
		ofxButton materialCupper;
		ofxButton materialRuby;

		//Style settings
		ofParameterGroup parametersSettings;
		ofxPanel gui;		
		ofParameter<float> height;
		ofParameter<float> width;
		ofParameter<float> depth;
		ofParameter<float> strokeWidth;
		ofParameter<ofColor> strokeColor;
		ofParameter<ofColor> color;
		ofParameter<bool> filled;
		ofParameter<ofColor> backgroundColor;
		ofParameter<ofVec2f> center;

		ofParameterGroup parametersTransformation;
		ofxPanel guiTransformation;
		ofParameter<bool> allScene;
		ofParameter<bool> translateXY;
		ofParameter<bool> translateZ;
		ofParameter<bool> rotateXY;
		ofParameter<bool> rotateZ;
		ofParameter<bool> rotateY;
		ofParameter<bool> rotateX;
		ofParameter<bool> scaleXY;
		ofParameter<bool> scaleXYZ;
		ofParameter<bool> scaleZ;
		ofxButton undo;
		ofxButton redo;
		ofxButton resetTransform;
		ofParameter<bool> xGrid;
		ofParameter<bool> yGrid;
		ofParameter<bool> zGrid;
		ofParameter<bool> locator;

		ofParameterGroup parametersCamera;
		ofxPanel guiCamera;
		ofParameter<int> fovV;
		ofParameter<int> fovH;
		ofParameter<int> nearPlane;
		ofParameter<int> farPlane;
		ofParameter<bool> tilt;
		ofParameter<bool> pan;
		ofParameter<bool> roll;
		ofParameter<bool> truck;
		ofParameter<bool> boom;
		ofParameter<bool> dolly;
		ofParameter<bool> front;
		ofParameter<bool> back;
		ofParameter<bool> perspective;
		ofParameter<bool> left;
		ofParameter<bool> right;
		ofParameter<bool> down;
		ofParameter<bool> top;
		ofParameter<bool> rotateAround;
		ofxButton animationCamera;
		ofxButton resetCamera;

		ofParameterGroup parametersLight;
		ofxPanel guiLight;
		ofParameter<bool> ambiantLightOn;
		ofParameter<bool> directionalLightOn;
		ofParameter<bool> directionalLightShow;
		ofParameter<bool> pointLightOn1;
		ofParameter<bool> pointLightShow1;
		ofParameter<bool> pointLightOn2;
		ofParameter<bool> pointLightShow2;
		ofParameter<bool> pointLightOn3;
		ofParameter<bool> pointLightShow3;
		ofParameter<bool> pointLightOn4;
		ofParameter<bool> pointLightShow4;
		ofParameter<bool> spotLightOn;
		ofParameter<bool> spotLightShow;
		ofParameter<bool> ambiantColor;
		ofParameter<bool> directionalColor;
		ofParameter<bool> pointColor;
		ofParameter<bool> spotColor;
		ofParameter<ofColor> lightColor;
		
		ofParameterGroup parametersCurve;
		ofxPanel guiCurve;
		ofParameter<bool> createBezier;
		ofParameter<bool> createHermite;
		ofParameter<bool> createCatmullRom;
		ofParameter<bool> modifiedCurve;
		ofParameter<bool> addCrtlPoint;
		ofParameter<bool> coons;
		ofParameter<bool> animateObject;
		ofParameter<bool> showCatmullRom;

		ofxAssimpModelLoader model;

		ofApp(int glVerMajor, int glVerMinor);

		~ofApp() { };
		bool changedParameterFlag = true;
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		//Create Object
		void ellipseChanged(bool & ellipse);
		void rectangleChanged(bool & rectangle);
		void lineChanged(bool & line);
		void cubeChanged(bool & cube);
		void sphereChanged(bool & sphere);
		void torusChanged(bool & torus);
		void octaedreChanged(bool & octaedre);
		void hilbertCurveChanged(bool & hilbertCurve);

		//Material
		void materialNoneChanged();
		void materialBrassChanged();
		void materialCupperChanged();
		void materialRubyChanged();

		//Style setting
		void filledChanged(bool & filled);
		void colorChanged(ofColor & pColor);
		void strokeWidthChanged(float & strokeWidth);
		void strokeColorChanged(ofColor & pColor);
		void heightChanged(float & height);
		void widthChanged(float & width);
		void depthChanged(float & depth);

		//Transformations
		void translateXYChanged(bool & translate);
		void translateZChanged(bool & translate);
		void rotateXYChanged(bool & rotate);
		void rotateYChanged(bool & rotate);
		void rotateXChanged(bool & rotate);
		void rotateZChanged(bool & rotate);
		void scaleXYChanged(bool & scale);
		void scaleXYZChanged(bool & scale);
		void scaleZChanged(bool & scale);
		void undoChanged();
		void redoChanged();
		void resetTransformChanged();

		//Camera
		void fovVChanged(int & fovV);
		void fovHChanged(int & fovH);
		void nearPlaneChanged(int & nearPlane);
		void farPlaneChanged(int & farPlane);
		void tiltChanged(bool & tilt);
		void panChanged(bool & pan);
		void rollChanged(bool & roll);
		void truckChanged(bool & truck);
		void boomChanged(bool & boom);
		void dollyChanged(bool & dolly);
		void rotateAroundChanged(bool & rotateAround);
		void animationCameraChanged();
		void frontChanged(bool & front);
		void backChanged(bool & back);
		void leftChanged(bool & left);
		void rightChanged(bool & right);
		void topChanged(bool & top);
		void downChanged(bool & down);
		void perspectiveChanged(bool & perspective);
		void resetCameraChanged();

		//Light
		void ambiantLightOnChanged(bool & ambiantLightOn);		
		void directionalLightOnChanged(bool & directionalLightOn);
		void directionalLightShowChanged(bool & directionalLightShow);
		void pointLightOn1Changed(bool & pointLightOn1);
		void pointLightShow1Changed(bool & pointLightShow1);
		void pointLightOn2Changed(bool & pointLightOn2);
		void pointLightShow2Changed(bool & pointLightShow2);
		void pointLightOn3Changed(bool & pointLightOn3);
		void pointLightShow3Changed(bool & pointLightShow3);
		void pointLightOn4Changed(bool & pointLightOn4);
		void pointLightShow4Changed(bool & pointLightShow4);
		void spotLightShowChanged(bool & spotLightShow);
		void spotLightOnChanged(bool & spotLightOn);
		void ambiantColorChanged(bool & ambiantColor);
		void directionalColorChanged(bool & directionalColor);
		void pointColorChanged(bool &pointColor);
		void spotColorChanged(bool &spotColor);
		void lightColorChanged(ofColor & lightColor);		
		
		//Curve
		void createBezierOnChanged(bool & createBezier);
		void createHermiteOnChanged(bool & createHermite);
		void createCatmullRomOnChanged(bool & catmullRom);
		void modifiedCurveOnChanged(bool & modifiedCurve); 
		void addCrtlPointOnChanged(bool &addCrtlPoint);
		void coonsOnChanged(bool &coons);
		void animateObjectOnChanged(bool &animateObject);
		void showCatmullRomOnChanged(bool &showCatmullRom);

		//usefull for the Gui
		void disableTransformButtons();
		void disableCameraButtons();
		void disableCameraSide();
		void checkTrueTransformationButtons(string buttons);
		void checkTrueCameraButtons(string buttons);
		void checkTrueCameraSideButtons(string buttons);

		void animatedCamera();		

		
};
