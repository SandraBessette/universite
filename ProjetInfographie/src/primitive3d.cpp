// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "primitive3d.h"


Primitive3d::Primitive3d(VectorPrimitive3d pType, ofVec3f positionBases, float pHeight, float pWidth, float pDepth, ofColor pColor,
	bool pFilled, float pStrokeWidth, ofColor pStrokeColor) : type(pType), positionBase(positionBases), height(pHeight),
	width(pWidth), depth(pDepth), color(pColor), filled(pFilled), strokeWidth(pStrokeWidth), strokeColor(pStrokeColor)
{
	model.createEmptyModel();
	
}
Primitive3d::Primitive3d(ofxAssimpModelLoader pModel, ofVec3f positionBases) : model(pModel), positionBase(positionBases),type(VectorPrimitive3d::OBJET_OBJ)
{
	modelMesh= model.getMesh(0);
	color = ofColor(100, 100, 140);
	
}
void Primitive3d::setup() {	
	materialNone = make_shared<Material>();
	materialBrass = make_shared<Material>();
	materialCupper = make_shared<Material>();
	materialRuby = make_shared<Material>();

	//material None
	materialNone->ambiant = ofVec3f(color.get().r/255.0, color.get().g / 255.0, color.get().b / 255.0);
	materialNone->diffuse = ofVec3f(color.get().r / 255.0, color.get().g / 255.0, color.get().b / 255.0);
	materialNone->specular = ofVec3f(0.5f, 0.5f, 0.5f);
	materialNone->brightness = 32.0;

	//material brass
	materialBrass->ambiant = ofVec3f(0.329412f, 0.223529f, 0.027451f);
	materialBrass->diffuse = ofVec3f(0.780392f, 0.568627f, 0.113725f);
	materialBrass->specular = ofVec3f(0.992157f, 0.941176f, 0.807843f);
	materialBrass->brightness = 0.21794872 * 128;

	//material cupper
	materialCupper->ambiant = ofVec3f(0.19125f, 0.0735f, 0.0225f);
	materialCupper->diffuse = ofVec3f(0.7038f, 0.27048f, 0.0828f);
	materialCupper->specular = ofVec3f(0.256777f, 0.137622f, 0.086014f);
	materialCupper->brightness = 0.1 * 128;

	//material ruby
	materialRuby->ambiant = ofVec3f(0.1745f, 0.01175f, 0.01175f);
	materialRuby->diffuse = ofVec3f(0.61424f, 0.04136f, 0.04136f);
	materialRuby->specular = ofVec3f(0.727811f, 0.626959f, 0.626959f);
	materialRuby->brightness = 0.6 * 128;
	
	material = materialNone;

	lineResolution = 800;
	radiusCtrlPoint = 10;
	isShowCatmullRom = true;
	typeMaterial = Material3d::NONE;
	if(type == VectorPrimitive3d::HILBERTCURVE)
	{   // la curve est compos� de 7X7X7 boites de hauteur de 40. 
		hilbertCurve.setup(40,3);
		width = 40 * 7;
		height = 40 * 7;
		depth = 40 * 7;

	}

	if (type == VectorPrimitive3d::OCTAEDRE)
	{  
		updatePrimitive();

	}
	if (type == VectorPrimitive3d::TORUS)
	{
		updatePrimitive();
	}
	if (type == VectorPrimitive3d::BEZIER || type == VectorPrimitive3d::HERMITE || type == VectorPrimitive3d::CATMULLROM || type == VectorPrimitive3d::COONS)
	{
		

		ctrlLine1.addVertex(ofPoint());
		ctrlLine1.addVertex(ofPoint());
		ctrlLine2.addVertex(ofPoint());
		ctrlLine2.addVertex(ofPoint());
		ctrlLine3.addVertex(ofPoint());
		ctrlLine3.addVertex(ofPoint());
		ctrlLine4.addVertex(ofPoint());
		ctrlLine4.addVertex(ofPoint());
		
		ofVec3f point4 = ofVec3f(-300, -200, 0);
		ofVec3f point3 = ofVec3f(-300, 200, 0);
		ofVec3f point2 = ofVec3f(300, 200, 0);
		ofVec3f point1 = ofVec3f(300, -200, 0);
	
	
		if (type == VectorPrimitive3d::CATMULLROM ) {
			ctrlPointCurve.push_back(point1);
			ctrlPointCurve.push_back(point2);			
		}
		else if (type == VectorPrimitive3d::COONS) {	
			ofVec3f point1C1 = ofVec3f(-200, 0, 0);
			ofVec3f point2C1 = ofVec3f(-100, 0, 100);
			ofVec3f point3C1 = ofVec3f(0, 0, 100);
			ofVec3f point4C1 = ofVec3f(100, 0, 0);

			ctrlPointCurve.push_back(point1C1);
			ctrlPointCurve.push_back(point2C1);
			ctrlPointCurve.push_back(point3C1);
			ctrlPointCurve.push_back(point4C1);

			ofVec3f point1C2 = ofVec3f(-200, 300, 0);
			ofVec3f point2C2 = ofVec3f(-100, 300, 100);
			ofVec3f point3C2 = ofVec3f(0, 300, 100);
			ofVec3f point4C2 = ofVec3f(100, 300, 0);

			ctrlPointCurve.push_back(point1C2);
			ctrlPointCurve.push_back(point2C2);
			ctrlPointCurve.push_back(point3C2);
			ctrlPointCurve.push_back(point4C2);

			ofVec3f point1C3 = ofVec3f(-200, 0, 0);
			ofVec3f point2C3 = ofVec3f(-200, 100, 100);
			ofVec3f point3C3 = ofVec3f(-200, 200, -300);
			ofVec3f point4C3 = ofVec3f(-200, 300, 0);

			ctrlPointCurve.push_back(point1C3);
			ctrlPointCurve.push_back(point2C3);
			ctrlPointCurve.push_back(point3C3);
			ctrlPointCurve.push_back(point4C3);

			ofVec3f point1C4 = ofVec3f(100, 0, 0);
			ofVec3f point2C4 = ofVec3f(100, 100, 100);
			ofVec3f point3C4 = ofVec3f(100, 200, 100);
			ofVec3f point4C4 = ofVec3f(100, 300, 0);

			ctrlPointCurve.push_back(point1C4);
			ctrlPointCurve.push_back(point2C4);
			ctrlPointCurve.push_back(point3C4);
			ctrlPointCurve.push_back(point4C4);

			
		}
		else {
			ctrlPointCurve.push_back(point1);
			ctrlPointCurve.push_back(point2);
			ctrlPointCurve.push_back(point3);		
			ctrlPointCurve.push_back(point4);
		}
		
		ofSetSphereResolution(32);	
		if(type == VectorPrimitive3d::COONS)
			lineMeshCurve.setMode(OF_PRIMITIVE_TRIANGLES);	
		else
			lineMeshCurve.setMode(OF_PRIMITIVE_LINE_STRIP);
		update();	
		
	}
	imageBrick.load("brickwall.jpg");
	imageBrickNormalMap.load("brickwall_normal.jpg");
	
	myBoxDraw.set(width, height, depth);
	myBoxDraw.setPosition(0, 0, 0);
	myBoxDraw.mapTexCoordsFromTexture(imageBrick.getTextureReference());
}

void Primitive3d::update() {
	int index;	
	ofVec3f tangent1;
	ofVec3f tangent2;
	int u;
	int v;
	ofVec3f lerpu;
	ofVec3f lerpv;
	ofVec3f blerp;
	ofVec3f surface;
	int delta;
	std::vector<ofVec3f> intermediate;	
	
	
		// param�tres selon le type de courbe
		switch (type)
		{
		case VectorPrimitive3d::BEZIER:			
			lineMeshCurve.clearVertices();			
			for (index = 0; index <= lineResolution; ++index)
			{
			bezierCubic(
				index / (float)lineResolution,
				ctrlPointCurve[0].x, ctrlPointCurve[0].y, ctrlPointCurve[0].z,
				ctrlPointCurve[1].x, ctrlPointCurve[1].y, ctrlPointCurve[1].z,
				ctrlPointCurve[2].x, ctrlPointCurve[2].y, ctrlPointCurve[2].z,
				ctrlPointCurve[3].x, ctrlPointCurve[3].y, ctrlPointCurve[3].z,
				positionCurve.x, positionCurve.y, positionCurve.z);
				lineMeshCurve.addVertex(positionCurve);			
			}
			break;		
		case VectorPrimitive3d::HERMITE:
			tangent1 = ctrlPointCurve[1] - ctrlPointCurve[0];
			tangent2 = ctrlPointCurve[2] - ctrlPointCurve[3];			
			lineMeshCurve.clearVertices();			
			for (index = 0; index <= lineResolution; ++index)
			{
				hermite(
					index / (float)lineResolution,
					ctrlPointCurve[0].x, ctrlPointCurve[0].y, ctrlPointCurve[0].z,
					tangent1.x, tangent1.y, tangent1.z,
					tangent2.x, tangent2.y, tangent2.z,
					ctrlPointCurve[3].x, ctrlPointCurve[3].y, ctrlPointCurve[3].z,
					positionCurve.x, positionCurve.y, positionCurve.z);
				lineMeshCurve.addVertex(positionCurve);				
			}
			break;
		case VectorPrimitive3d::CATMULLROM:			
			lineMeshCurve.clearVertices();		

			for (int i = 0; i< ctrlPointCurve.size()-1; i++) {
				ofVec3f p0 = i == 0 ? ctrlPointCurve[i] : ctrlPointCurve[i - 1];
				ofVec3f p1 = ctrlPointCurve[i];
				ofVec3f p2 = ctrlPointCurve[i + 1];
				ofVec3f p3 = i+2 == ctrlPointCurve.size() ? ctrlPointCurve[i + 1] : ctrlPointCurve[i +2];				
					
				for (index = 0; index <= lineResolution; ++index)
				{
					catmullRom(
						index / (float)lineResolution,
						p0.x, p0.y, p0.z,
						p1.x, p1.y, p1.z,
						p2.x, p2.y, p2.z,
						p3.x, p3.y, p3.z,
						positionCurve.x, positionCurve.y, positionCurve.z);					
					lineMeshCurve.addVertex(positionCurve);					
				}
			}
			break;
		case VectorPrimitive3d::COONS:			
			lineMeshCurve.clearVertices();
			//intermediate.clear();
			delta = 1;
			for (u= 0; u < 100 ; u = u + delta)	
			{
				
				for (v = 0; v< 100 ;v = v + delta)
				{
					float uN = (u) / (float)100;
					float vN = (v) / (float)100;

					ofVec3f positionCurve1;
					ofVec3f positionCurve2;
					ofVec3f positionCurve3;
					ofVec3f positionCurve4;
					bezierCurve(ctrlPointCurve, uN , positionCurve1, 0);
					bezierCurve(ctrlPointCurve, uN , positionCurve2, 4);
					bezierCurve(ctrlPointCurve, vN , positionCurve3, 8);
					bezierCurve(ctrlPointCurve, vN , positionCurve4, 12);
					lerpu = (1 - vN) * positionCurve1 + vN * positionCurve2;
					lerpv = (1 - uN) * positionCurve3 + uN * positionCurve4;

					ofVec3f positionCurve1_0;
					ofVec3f positionCurve1_1;
					ofVec3f positionCurve2_0;
					ofVec3f positionCurve2_1;
					bezierCurve(ctrlPointCurve, 0, positionCurve1_0, 0);
					bezierCurve(ctrlPointCurve, 1, positionCurve1_1, 0);
					bezierCurve(ctrlPointCurve, 0, positionCurve2_0, 4);
					bezierCurve(ctrlPointCurve, 1, positionCurve2_1, 4);

					blerp = (1 - uN) * (1 - vN) * positionCurve1_0 + uN * (1 - vN) * positionCurve1_1 + (1 - uN) * vN * positionCurve2_0 + uN * vN * positionCurve2_1;

					surface = lerpu + lerpv - blerp;
					intermediate.push_back(surface);				
				}
					
				
			}
			//triangle grid
			delta = 1;
			for (int i = 0; i<= 100 -delta; i = i + delta)
			{
				for (int j = 0; j<= 100-delta; j = j + delta)

				{
					ofVec3f normal;
					ofVec3f normal2;
				
					if (i == 100-delta)
						normal = -1*(intermediate[(100 * (i - delta) + (j))] - intermediate[(100 * i + j)]);
					else
						normal = intermediate[(100 * (i + delta) + (j))] - intermediate[(100 * i + j)];
					normal = normal.normalize();
					if(j + delta < 100)
						normal2 = intermediate[((100 * i) + j+delta)] - intermediate[(100 * i + j)];
					else
						normal2 = -1 *(intermediate[(100 * i + j - delta)] - intermediate[(100 * i + j)]);
					normal2 = normal2.normalize();
					normal = normal.perpendicular(normal2);
					normal = normal.normalize();
					lineMeshCurve.addVertex(intermediate[100 * i + j]);
					lineMeshCurve.addNormal(normal);					
				}
			}		

			for (int y = 0; y<100/delta - 1; y++) {
				for (int x = 0; x<100 / delta -1; x++) {
					lineMeshCurve.addIndex(x + y* 100 / delta);               
					lineMeshCurve.addIndex((x + 1) + y* 100 / delta);          
					lineMeshCurve.addIndex(x + (y +1)* 100 / delta);           

					lineMeshCurve.addIndex((x + 1) + y* 100 / delta);           
					lineMeshCurve.addIndex((x + 1) + (y + 1)* 100 / delta);       
					lineMeshCurve.addIndex(x + (y + 1)* 100 / delta);           
				}
			}			

			
		default:
			break;
		}	
		
	
	
}

void Primitive3d::updatePrimitive() {

	if (type == VectorPrimitive3d::OCTAEDRE)
	{
		depth = width;
		height = width;
		meshOctaedre.setMode(OF_PRIMITIVE_TRIANGLES);
		//meshOctaedre.addNormal();
		meshOctaedre.clear();
		meshOctaedre.addNormal(ofVec3f(0, 1, 0));
		meshOctaedre.addVertex(ofVec3f(0, width / 2, 0));
		meshOctaedre.addNormal(ofVec3f(1, 0, 0));
		meshOctaedre.addVertex(ofVec3f(width / 2, 0, 0));
		meshOctaedre.addNormal(ofVec3f(0, 0, 1));
		meshOctaedre.addVertex(ofVec3f(0, 0, width / 2));

		meshOctaedre.addNormal(ofVec3f(0, 1, 0));
		meshOctaedre.addVertex(ofVec3f(0, width / 2, 0));
		meshOctaedre.addNormal(ofVec3f(0, 0, 1));
		meshOctaedre.addVertex(ofVec3f(0, 0, width / 2));
		meshOctaedre.addNormal(ofVec3f(-1, 0, 0));
		meshOctaedre.addVertex(ofVec3f(-width / 2, 0, 0));

		meshOctaedre.addNormal(ofVec3f(0, 1, 0));
		meshOctaedre.addVertex(ofVec3f(0, width / 2, 0));
		meshOctaedre.addNormal(ofVec3f(-1, 0, 0));
		meshOctaedre.addVertex(ofVec3f(-width / 2, 0, 0));
		meshOctaedre.addNormal(ofVec3f(0, 0, -1));
		meshOctaedre.addVertex(ofVec3f(0, 0, -width / 2));

		meshOctaedre.addNormal(ofVec3f(0, 1, 0));
		meshOctaedre.addVertex(ofVec3f(0, width / 2, 0));
		meshOctaedre.addNormal(ofVec3f(0, 0, -1));
		meshOctaedre.addVertex(ofVec3f(0, 0, -width / 2));
		meshOctaedre.addNormal(ofVec3f(1, 0, 0));
		meshOctaedre.addVertex(ofVec3f(width / 2, 0, 0));

		meshOctaedre.addNormal(ofVec3f(0, -1, 0));
		meshOctaedre.addVertex(ofVec3f(0, -width / 2, 0));
		meshOctaedre.addNormal(ofVec3f(1, 0, 0));
		meshOctaedre.addVertex(ofVec3f(width / 2, 0, 0));
		meshOctaedre.addNormal(ofVec3f(0, 0, 1));
		meshOctaedre.addVertex(ofVec3f(0, 0, width / 2));

		meshOctaedre.addNormal(ofVec3f(0, -1, 0));
		meshOctaedre.addVertex(ofVec3f(0, -width / 2, 0));
		meshOctaedre.addNormal(ofVec3f(0, 0, 1));
		meshOctaedre.addVertex(ofVec3f(0, 0, width / 2));
		meshOctaedre.addNormal(ofVec3f(-1, 0, 0));
		meshOctaedre.addVertex(ofVec3f(-width / 2, 0, 0));

		meshOctaedre.addNormal(ofVec3f(0, -1, 0));
		meshOctaedre.addVertex(ofVec3f(0, -width / 2, 0));
		meshOctaedre.addNormal(ofVec3f(-1, 0, 0));
		meshOctaedre.addVertex(ofVec3f(-width / 2, 0, 0));
		meshOctaedre.addNormal(ofVec3f(0, 0, -1));
		meshOctaedre.addVertex(ofVec3f(0, 0, -width / 2));

		meshOctaedre.addNormal(ofVec3f(0, -1, 0));
		meshOctaedre.addVertex(ofVec3f(0, -width / 2, 0));
		meshOctaedre.addNormal(ofVec3f(0, 0, -1));
		meshOctaedre.addVertex(ofVec3f(0, 0, -width / 2));
		meshOctaedre.addNormal(ofVec3f(1, 0, 0));
		meshOctaedre.addVertex(ofVec3f(width / 2, 0, 0));

	}
	if (type == VectorPrimitive3d::TORUS)
	{
		meshTorus.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
		int x, y, z;
		int delta = 5;
		int r = depth / 2;
		int R = width / 2 - r;
		height = width;
		meshTorus.clear();
		for (int u = 0; u<360 + delta; u = u + delta)
		{
			for (int v = 0; v < 360 + delta; v = v + delta) {
				meshTorus.addNormal(ofVec3f(
					(R + r*r*cos(ofDegToRad(v)))*cos(ofDegToRad(u)) - (R + r*cos(ofDegToRad(v)))*cos(ofDegToRad(u)),
					(R + r*r*cos(ofDegToRad(v)))*sin(ofDegToRad(u)) - (R + r*cos(ofDegToRad(v)))*sin(ofDegToRad(u)),
					(r*r*sin(ofDegToRad(v)) - r*sin(ofDegToRad(v)))));
				x = (R + r * cos(ofDegToRad(v)))*cos(ofDegToRad(u));
				y = (R + r * cos(ofDegToRad(v)))*sin(ofDegToRad(u));
				z = r * sin(ofDegToRad(v));
				meshTorus.addVertex(ofVec3f(x, y, z));
				x = (R + r * cos(ofDegToRad(v + delta)))*cos(ofDegToRad(u + delta));
				y = (R + r * cos(ofDegToRad(v + delta)))*sin(ofDegToRad(u + delta));
				z = r * sin(ofDegToRad(v + delta));
				meshTorus.addNormal(ofVec3f(
					(R + r*r*cos(ofDegToRad(v + delta)))*cos(ofDegToRad(u + delta)) - (R + r*cos(ofDegToRad(v + delta)))*cos(ofDegToRad(u + delta)),
					(R + r*r*cos(ofDegToRad(v + delta)))*sin(ofDegToRad(u + delta)) - (R + r*cos(ofDegToRad(v + delta)))*sin(ofDegToRad(u + delta)),
					(r*r*sin(ofDegToRad(v + delta)) - r*sin(ofDegToRad(v + delta)))));
				meshTorus.addVertex(ofVec3f(x, y, z));

			}
		}

	}

}

void Primitive3d::draw(ofShader * shader, bool isTexture, ofShader * shaderTexture) {

	ofPushStyle();
	ofEnableDepthTest();

	ofPushMatrix();
	ofMultMatrix(localMatrixTranslation);

	ofPushMatrix();
	ofMultMatrix(localMatrixRotation);

	ofPushMatrix();
	ofMultMatrix(localMatrixScale);

	ofEnableLighting();
	//ofDisableLighting();
	switch (type)
	{
	case VectorPrimitive3d::CUBE:
		ofSetCircleResolution(48);
		if (filled) {
			ofSetLineWidth(0);
			ofFill();
			ofSetColor(color);
			myBoxDraw.setPosition(0, 0, 0);
			myBoxDraw.set(width, height, depth);			
			if (isTexture) {
				shaderTexture->begin();
				shaderTexture->setUniform3f("materialColorAmbient", ofVec3f(color.get().r / 255.0, color.get().g / 255.0, color.get().b / 255.0));
				shaderTexture->setUniform3f("materialColorDiffuse", ofVec3f(color.get().r / 255.0, color.get().g / 255.0, color.get().b / 255.0));
				shaderTexture->setUniform3f("materialColorSpecular", ofVec3f(0.5f, 0.5f, 0.5f));
				shaderTexture->setUniform1f("materialBrightness", 32.0);
				shaderTexture->setUniform1f("alpha", color.get().a / 255.0);
				shaderTexture->setUniformTexture("diffuseMap", imageBrick.getTextureReference(), 1);
				shaderTexture->setUniformTexture("normalMap", imageBrickNormalMap.getTextureReference(), 2);
			}
			else {
				shader->begin();
				shader->setUniform3f("materialColorAmbient", material->ambiant);
				shader->setUniform3f("materialColorDiffuse", material->diffuse);
				shader->setUniform3f("materialColorSpecular", material->specular);
				shader->setUniform1f("materialBrightness", material->brightness);
				shader->setUniform1f("alpha", color.get().a / 255.0);
			}
					
			//ofDrawBox(0, 0, 0, width, height, depth);
			myBoxDraw.draw();
			if (isTexture)
				shaderTexture->end();
			else
				shader->end();
		}
		else {
			ofNoFill();
		}
		ofNoFill();

		if (strokeWidth != 0.0f) {
			ofDisableLighting();
			ofSetLineWidth(strokeWidth);
			ofSetColor(strokeColor);			
			ofDrawBox(0, 0, 0, width, height, depth);
			ofEnableLighting();
		}
		break;
	case VectorPrimitive3d::SPHERE:
		ofSetCircleResolution(48);
		if (filled) {
			ofSetLineWidth(0);
			ofFill();
			ofSetColor(color);
			shader->begin();	
			shader->setUniform3f("materialColorAmbient", material->ambiant);
			shader->setUniform3f("materialColorDiffuse", material->diffuse);
			shader->setUniform3f("materialColorSpecular", material->specular);
			shader->setUniform1f("materialBrightness", material->brightness);
			shader->setUniform1f("alpha", color.get().a/255.0);
			shader->setUniform1f("isSphere", true);			
			ofDrawSphere(0, 0, 0, width/2);
			shader->end();
		}
		else {
			ofNoFill();
		}
		ofNoFill();

		if (strokeWidth != 0.0f) {
			ofDisableLighting();
			ofSetLineWidth(strokeWidth);
			ofSetColor(strokeColor);
			ofDrawSphere(0, 0, 0, width/2);
			ofEnableLighting();
		}
		break;
	case VectorPrimitive3d::TORUS:			
		if (filled) {
			ofSetLineWidth(0);
			ofFill();
			ofSetColor(color);
			shader->begin();
			shader->setUniform3f("materialColorAmbient", material->ambiant);
			shader->setUniform3f("materialColorDiffuse", material->diffuse);
			shader->setUniform3f("materialColorSpecular", material->specular);
			shader->setUniform1f("materialBrightness", material->brightness);
			shader->setUniform1f("alpha", color.get().a / 255.0);			
			meshTorus.draw();
			shader->end();
		}
		else {
			ofNoFill();
			if (strokeWidth != 0.0f) {
				ofDisableLighting();
				ofSetLineWidth(strokeWidth);
				ofSetColor(strokeColor);
				meshTorus.drawWireframe();
				ofEnableLighting();
			}
		}
		
		break;
	case VectorPrimitive3d::OCTAEDRE:
		ofSetCircleResolution(48);
		if (filled) {
			ofSetLineWidth(0);
			ofFill();
			ofSetColor(color);
			shader->begin();			
			shader->setUniform3f("materialColorAmbient", material->ambiant);
			shader->setUniform3f("materialColorDiffuse", material->diffuse);
			shader->setUniform3f("materialColorSpecular", material->specular);
			shader->setUniform1f("materialBrightness", material->brightness);
			shader->setUniform1f("alpha", color.get().a/255.0);			
			meshOctaedre.draw();
			shader->end();
		}
		else {
			ofNoFill();
		}
		ofNoFill();

		if (strokeWidth != 0.0f) {
			ofDisableLighting();
			ofSetLineWidth(strokeWidth);
			ofSetColor(strokeColor);
			meshOctaedre.drawWireframe();
			ofEnableLighting();
		}
		break;
	case VectorPrimitive3d::HILBERTCURVE:
		if(typeMaterial == Material3d::NONE) {
			shader->begin();			
			shader->setUniform1f("alpha", color.get().a / 255.0);
			shader->end();
			hilbertCurve.draw(shader, true);			
		}
		else {
			shader->begin();			
			shader->setUniform3f("materialColorAmbient", material->ambiant);
			shader->setUniform3f("materialColorDiffuse", material->diffuse);
			shader->setUniform3f("materialColorSpecular", material->specular);
			shader->setUniform1f("materialBrightness", material->brightness);
			shader->setUniform1f("alpha", color.get().a / 255.0);				
			hilbertCurve.draw(shader, false);
			shader->end();
		}
		break;
	case VectorPrimitive3d::OBJET_OBJ:
		
		shader->begin();
		model.disableMaterials();			
		shader->setUniform3f("materialColorAmbient", material->ambiant);
		shader->setUniform3f("materialColorDiffuse", material->diffuse);
		shader->setUniform3f("materialColorSpecular", material->specular);
		shader->setUniform1f("materialBrightness", material->brightness);
		shader->setUniform1f("alpha", 1.0f);		
		model.draw(OF_MESH_FILL);
		shader->end();

		
		break;
	case VectorPrimitive3d::BEZIER:	
		ofDisableLighting();
		ofFill();		
		ofSetLineWidth(strokeWidth);		
		ofSetColor(strokeColor);
		lineMeshCurve.draw(OF_MESH_FILL);
		break;
	case VectorPrimitive3d::HERMITE:
		ofDisableLighting();
		ofFill();		
		ofSetLineWidth(strokeWidth);
		ofSetColor(strokeColor);
		lineMeshCurve.draw(OF_MESH_FILL);		
		break;
	case VectorPrimitive3d::CATMULLROM:
		ofDisableLighting();
		ofFill();		
		ofSetLineWidth(strokeWidth);
		ofSetColor(strokeColor);
		if(isShowCatmullRom)
			lineMeshCurve.draw(OF_MESH_FILL);
		break;
	case VectorPrimitive3d::COONS:		
		ofFill();		
		ofSetLineWidth(strokeWidth);
		ofSetColor(strokeColor);		
		shader->begin();
		shader->setUniform3f("materialColorAmbient", material->ambiant);
		shader->setUniform3f("materialColorDiffuse", material->diffuse);
		shader->setUniform3f("materialColorSpecular", material->specular);
		shader->setUniform1f("materialBrightness", material->brightness);
		shader->setUniform1f("alpha", color.get().a / 255.0);		
		lineMeshCurve.draw(OF_MESH_FILL);
		shader->end();
		break;
	default:
		break;
	}
	
	if (isSelected) {
		ofDisableLighting();
		ofNoFill();
		ofSetLineWidth(1.0);
		ofSetColor(255, 255, 255);
		drawZone();
		drawCtrlPointZone();
	}
	ofEnableLighting();	
	ofPopMatrix();
	ofPopMatrix();
	ofPopMatrix();
	ofPopStyle();

}

void Primitive3d::drawZone() {
	float offset = 30.0f;
	ofPoint centerPoint;
	ofPoint minPoint;
	ofPoint maxPoint;

	switch (type)
	{
	case VectorPrimitive3d::CUBE:
		ofDrawBox(0, 0, 0, width + offset, height + offset, depth + offset);
		break;
	case VectorPrimitive3d::SPHERE:
		ofDrawBox(0, 0, 0, width + offset, width + offset, width + offset);
		break;
	case VectorPrimitive3d::OBJET_OBJ:
		ofPushStyle();
		model.disableMaterials();
		model.disableTextures();
		ofSetLineWidth(2.0);
		ofSetColor(255, 255, 255);
		model.draw(OF_MESH_WIREFRAME);
		model.enableMaterials();
		model.enableTextures();
		ofPopStyle();
		break;
	case VectorPrimitive3d::HILBERTCURVE:
		ofDrawBox(0, 0, 0, width + offset, height + offset, depth + offset);
		break;
	case VectorPrimitive3d::TORUS:		
		ofDrawBox(0, 0, 0, width + offset, height + offset, depth + offset);
		break;
	case VectorPrimitive3d::OCTAEDRE:		
		ofDrawBox(0, 0, 0, width + offset, height + offset, depth + offset);
		break;
	case VectorPrimitive3d::BEZIER:		
		drawControlPoint();
		break;
	case VectorPrimitive3d::HERMITE:		
		drawControlPoint();
		break;		
	case VectorPrimitive3d::CATMULLROM:		
		if(isShowCatmullRom)
			drawControlPoint();
		break;
	case VectorPrimitive3d::COONS:
		drawControlPoint();
		break;
	default:
		break;

	}
}
void Primitive3d::drawCtrlPointZone() {	
	
	if (ctrlPointSelectedIndex != -1)
		ofDrawBox(ctrlPointCurve[ctrlPointSelectedIndex].x, ctrlPointCurve[ctrlPointSelectedIndex].y, ctrlPointCurve[ctrlPointSelectedIndex].z, radiusCtrlPoint * 2, radiusCtrlPoint * 2, radiusCtrlPoint * 2);
}

void Primitive3d::drawControlPoint() {
	ofPushStyle();
	ofFill();
	// dessiner la ligne contour
	ofSetColor(255, 0, 0);
	
	ofSetLineWidth(2);		
	
	if (type == VectorPrimitive3d::BEZIER){
		ctrlLine1[0] = ctrlPointCurve[0];
		ctrlLine1[1] = ctrlPointCurve[1];
		ctrlLine2[0] = ctrlPointCurve[1];
		ctrlLine2[1] = ctrlPointCurve[2];
		ctrlLine3[0] = ctrlPointCurve[2];
		ctrlLine3[1] = ctrlPointCurve[3];
		ctrlLine4[0] = ctrlPointCurve[3];
		ctrlLine4[1] = ctrlPointCurve[0];
		ctrlLine1.draw();
		ctrlLine3.draw();
		ctrlLine2.draw();
		ctrlLine4.draw();
		ofSetColor(0, 0, 255);
		for (int i = 0; i< ctrlPointCurve.size(); i++) {
			ofDrawSphere(ctrlPointCurve[i].x, ctrlPointCurve[i].y, ctrlPointCurve[i].z, radiusCtrlPoint);

		}
		
	}
	if (type == VectorPrimitive3d::HERMITE) {
		ctrlLine1[0] = ctrlPointCurve[0];
		ctrlLine1[1] = ctrlPointCurve[1];
		ctrlLine2[0] = ctrlPointCurve[1];
		ctrlLine2[1] = ctrlPointCurve[2];
		ctrlLine3[0] = ctrlPointCurve[2];
		ctrlLine3[1] = ctrlPointCurve[3];
		ctrlLine4[0] = ctrlPointCurve[3];
		ctrlLine4[1] = ctrlPointCurve[0];
		ofSetColor(255, 0, 0);
		ctrlLine1.draw();
		ctrlLine3.draw();
		ofSetColor(0, 0, 255);
		for (int i = 0; i< ctrlPointCurve.size(); i++) {
			ofDrawSphere(ctrlPointCurve[i].x, ctrlPointCurve[i].y, ctrlPointCurve[i].z, radiusCtrlPoint);
		}	
		
	}
	if (type == VectorPrimitive3d::CATMULLROM) {
		ofSetColor(0, 0, 255);
		for (int i = 0; i< ctrlPointCurve.size(); i++) {
			ofDrawSphere(ctrlPointCurve[i].x, ctrlPointCurve[i].y, ctrlPointCurve[i].z, radiusCtrlPoint);

		}		

	}
	if (type == VectorPrimitive3d::COONS) {

		for (int i = 0; i<= ctrlPointCurve.size()-4; i= i+4) {
			ctrlLine1[0] = ctrlPointCurve[i];
			ctrlLine1[1] = ctrlPointCurve[i+1];
			ctrlLine2[0] = ctrlPointCurve[i + 1];
			ctrlLine2[1] = ctrlPointCurve[i + 2];
			ctrlLine3[0] = ctrlPointCurve[i + 2];
			ctrlLine3[1] = ctrlPointCurve[i + 3];
			ctrlLine4[0] = ctrlPointCurve[i + 3];
			ctrlLine4[1] = ctrlPointCurve[i];
			ctrlLine1.draw();
			ctrlLine3.draw();
			ctrlLine2.draw();
			ctrlLine4.draw();

		}
		
		ofSetColor(0, 0, 255);
		for (int i = 0; i< ctrlPointCurve.size(); i++) {
			ofDrawSphere(ctrlPointCurve[i].x, ctrlPointCurve[i].y, ctrlPointCurve[i].z, radiusCtrlPoint);

		}

	}
	

	ofPopStyle();
}

//-------------------------------------------------------------------------------------------------
//Material
void Primitive3d::setMaterial(Material3d typeMat) {
	typeMaterial = typeMat;
	switch (typeMat)
	{
	case Material3d::NONE:
		material = materialNone;
		break;

	case Material3d::BRASS:
		material = materialBrass;
		break;

	case Material3d::CUPPER:
		material = materialCupper;
		break;

	case Material3d::RUBY:
		material = materialRuby;
		break;	

	default:
		material = materialNone;
		typeMaterial = Material3d::NONE;
		break;
	}

}
//-------------------------------------------------------------------------------------------------
//Style setting
void Primitive3d::setFilled(bool isFilled) {
	filled = isFilled;
}

void Primitive3d::setColor(ofColor pColor) {
	color = pColor;
	//update material None
	materialNone->ambiant = ofVec3f(color.get().r / 255.0, color.get().g / 255.0, color.get().b / 255.0);
	materialNone->diffuse = ofVec3f(color.get().r / 255.0, color.get().g / 255.0, color.get().b / 255.0);
	materialNone->specular = ofVec3f(0.5f, 0.5f, 0.5f);
	materialNone->brightness = 32.0;
}

void Primitive3d::setStrokeWidth(float pStrokeWidth) {
	strokeWidth = pStrokeWidth;
	
}

void Primitive3d::setHeight(float pHeight) {
	if (type != VectorPrimitive3d::HILBERTCURVE && type != VectorPrimitive3d::TORUS && type != VectorPrimitive3d::OCTAEDRE)
		height = pHeight;
}
void Primitive3d::setWidth(float pWidth) {
	if (type != VectorPrimitive3d::HILBERTCURVE) {
		width = pWidth;
		updatePrimitive(); 
	}

}
void Primitive3d::setDepth(float pDepth) {
	if (type != VectorPrimitive3d::HILBERTCURVE && type != VectorPrimitive3d::OCTAEDRE) {
		depth = pDepth;
		updatePrimitive();
	}
}
void Primitive3d::setStrokeColor(ofColor pColor) {
	strokeColor = pColor;
}

//-------------------------------------------------------------------------------------------------
//S�lection
bool Primitive3d::inside(int px, int py, int pz, ofMatrix4x4 modelMatrix, ofBoxPrimitive myBox, ofCamera * cam) {

	ofVec3f xSmallDistance(0, 0, 0);
	ofVec3f xBigDistance(0, 0, 0);
	ofVec3f ySmallDistance(0, 0, 0);
	ofVec3f yBigDistance(0, 0, 0);

	ofMesh mesh;
	//ofBoxPrimitive myBox(width+10, height+10, depth+10);
	
	ofVec3f mouse(px, py);
	mesh = myBox.getMesh();

	int n = mesh.getNumVertices();
	for (int i = 0; i < n; i++) {
		ofVec3f cur = cam->worldToScreen(mesh.getVertex(i) * myBox.getLocalTransformMatrix() * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix);

		if (i == 0 || cur.x <  xSmallDistance.x) {
			xSmallDistance = cur;
		}
		if (i == 0 || cur.x >  xBigDistance.x) {
			xBigDistance = cur;
		}
		if (i == 0 || cur.y <  ySmallDistance.y) {
			ySmallDistance = cur;
		}
		if (i == 0 || cur.y >  yBigDistance.y) {
			yBigDistance = cur;
		}

	}

	if (px >= xSmallDistance.x && py >= ySmallDistance.y  && px <= xBigDistance.x  && py <= yBigDistance.y) {
		xMax = xBigDistance;
		xMin = xSmallDistance;
		yMin = ySmallDistance;
		yMax = yBigDistance;
		return true;
	}

	return false;
}

bool Primitive3d::insideCurve(int px, int py, int pz, ofMatrix4x4 modelMatrix, ofVboMesh myMesh, ofCamera * cam) {

	ofVec3f xSmallDistance(0, 0, 0);
	ofVec3f xBigDistance(0, 0, 0);
	ofVec3f ySmallDistance(0, 0, 0);
	ofVec3f yBigDistance(0, 0, 0);

	ofMesh mesh = myMesh;
	//ofBoxPrimitive myBox(width+10, height+10, depth+10);

	ofVec3f mouse(px, py);	

	int n = mesh.getNumVertices();
	for (int i = 0; i < n; i++) {
		ofVec3f cur = cam->worldToScreen(mesh.getVertex(i) * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix);

		if ((px >= cur.x - 5) && (py >= cur.y - 5) && (px <= cur.x + 5 && py <= cur.y + 5)) {
			return true;
		}
		
	}
	

	return false;
}

bool Primitive3d::insideOBJ(int px, int py, int pz, ofMatrix4x4 modelMatrix, ofCamera* cam) {

	ofVec3f xSmallDistance(0, 0, 0);
	ofVec3f xBigDistance(0, 0, 0);
	ofVec3f ySmallDistance(0, 0, 0);
	ofVec3f yBigDistance(0, 0, 0);


	ofMesh  mesh;
	ofxAssimpMeshHelper meshIndex;
	int numMesh = model.getMeshCount();

	ofVec3f mouse(px, py);
	int n;
	for (int j = 0; j < numMesh; j++) {

		mesh = model.getCurrentAnimatedMesh(j);
		n = mesh.getNumVertices();

		for (int i = 0; i < n; i++) {
			ofVec3f cur = cam->worldToScreen(mesh.getVertex(i)* model.getModelMatrix() *localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix);

			if ((i == 0 && j == 0) || cur.x <  xSmallDistance.x) {
				xSmallDistance = cur;
			}
			if ((i == 0 && j == 0) || cur.x >  xBigDistance.x) {
				xBigDistance = cur;
			}
			if ((i == 0 && j == 0) || cur.y <  ySmallDistance.y) {
				ySmallDistance = cur;
			}
			if ((i == 0 && j == 0) || cur.y >  yBigDistance.y) {
				yBigDistance = cur;
			}
		}
	}

	if (px >= xSmallDistance.x && py >= ySmallDistance.y  && px <= xBigDistance.x  && py <= yBigDistance.y) {
		xMax = xBigDistance;
		xMin = xSmallDistance;
		yMin = ySmallDistance;
		yMax = yBigDistance;
		return true;
	}

	return false;
}

bool Primitive3d::isInsideShape(int xPressed, int yPressed, int z, ofMatrix4x4 modelMatrix, ofCamera * cam, bool changedSelection) {

	ofBoxPrimitive myBox;
	myBox.setPosition(0, 0, 0);	
	myBox.set(width + 10, height + 10, depth + 10);
	bool isInside = false;
	switch (type)
	{
	case VectorPrimitive3d::CUBE:
		
		isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);
		break;
	case VectorPrimitive3d::SPHERE:
		myBox.set(width + 10, width + 10, width + 10);
		isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);
		break;
	case VectorPrimitive3d::OBJET_OBJ:
		isInside = insideOBJ(xPressed, yPressed, z, modelMatrix, cam);
		break;

	case VectorPrimitive3d::HILBERTCURVE:
		isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);
		break;
	case VectorPrimitive3d::TORUS:	
		isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);
		break;
	case VectorPrimitive3d::OCTAEDRE:		
		isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);
		break;
	case VectorPrimitive3d::BEZIER:		
		isInside = insideCurve(xPressed, yPressed, z, modelMatrix, lineMeshCurve, cam);
		break;
	case VectorPrimitive3d::HERMITE:
		isInside = insideCurve(xPressed, yPressed, z, modelMatrix, lineMeshCurve, cam);
		break;
	case VectorPrimitive3d::CATMULLROM:
		isInside = insideCurve(xPressed, yPressed, z, modelMatrix, lineMeshCurve, cam);
		break;
	case VectorPrimitive3d::COONS:
		isInside = insideCurve(xPressed, yPressed, z, modelMatrix, lineMeshCurve, cam);
		break;
	default:
		break;

	}
	
	if (isInside && changedSelection) {
		if(isSelected)
			ctrlPointSelectedIndex = -1;
		isSelected = !isSelected;
		
	}
	return isInside;
}


//-------------------------------------------------------------------------------------------------
//Transformation

void Primitive3d::setOffsetTranslate(ofVec3f offset) {
	xOffsetTranslate +=  offset.x;
	yOffsetTranslate += offset.y;
	zOffsetTranslate += offset.z;

	xOffsetTranslateHistory += offset.x;
	yOffsetTranslateHistory += offset.y;
	zOffsetTranslateHistory += offset.z;

	localMatrixTranslation.setTranslation(xOffsetTranslate, yOffsetTranslate, zOffsetTranslate);
}

void Primitive3d::setOffsetRotateXY(ofVec3f offset, float delay) {	
	
	ofQuaternion yRot((offset.x)*delay, ofVec3f(0, 1, 0));
	ofQuaternion xRot((offset.y)*delay, ofVec3f(-1, 0, 0));
	curRot *= yRot*xRot;
	curRotHistory *= yRot*xRot;	
	localMatrixRotation.setRotate(curRot);
}

void Primitive3d::setOffsetRotateZ(ofVec3f offset, float delay) {
	ofQuaternion zRot((offset.z)*delay, ofVec3f(0, 0, 1));
	curRot *= zRot;

	curRotHistory *= zRot;	
	localMatrixRotation.setRotate(curRot);
}
void Primitive3d::setOffsetRotateY(ofVec3f offset, float delay) {
	ofQuaternion yRot((offset.x)*delay, ofVec3f(0, 1, 0));
	curRot *= yRot;

	curRotHistory *= yRot;	
	localMatrixRotation.setRotate(curRot);
}
void Primitive3d::setOffsetRotateX(ofVec3f offset, float delay) {
	ofQuaternion xRot((offset.y)*delay, ofVec3f(-1, 0, 0));
	curRot *= xRot;

	curRotHistory *= xRot;	
	localMatrixRotation.setRotate(curRot);
}

void Primitive3d::setOffsetScale(ofVec3f offset) {
	if (offset.x == offset.y &&  offset.y == offset.z) { //this means we are doing an uniform scale
		if (((xOffsetScale + offset.x / 20.0) > 0) && ((yOffsetScale + offset.y / 20.0) > 0) && ((zOffsetScale + offset.z / 20.0) > 0)) {
			xOffsetScale += offset.x / 20.0;
			yOffsetScale += offset.y / 20.0;
			zOffsetScale += offset.z / 20.0;
		}
	}
	else { //for non uniform scale transformation
		if (xOffsetScale + offset.x / 20.0 > 0)
			xOffsetScale += offset.x / 20.0;
		if (yOffsetScale + offset.y / 20.0 > 0)
			yOffsetScale += offset.y / 20.0;
		if (zOffsetScale + offset.z / 20.0 > 0)
			zOffsetScale += offset.z / 20.0;
	}
	localMatrixScale.makeScaleMatrix(xOffsetScale, yOffsetScale, zOffsetScale);
}

//-------------------------------------------------------------------------------------------------
//Undo redo

void Primitive3d::keepTransformHistory() {

	//translation
	ofMatrix4x4 modelMatrixTranslationHistory;
	modelMatrixTranslationHistory.setTranslation(xOffsetTranslateHistory, yOffsetTranslateHistory, zOffsetTranslateHistory);
	xOffsetTranslateHistory = 0;
	yOffsetTranslateHistory = 0;
	zOffsetTranslateHistory = 0;

	//rotation
	ofMatrix4x4 modelMatrixRotationHistory;	
	modelMatrixRotationHistory.setRotate(curRotHistory);
	ofQuaternion start;
	curRotHistory = start;

	//scale
	ofMatrix4x4 modelMatrixScaleHistory;
	modelMatrixScaleHistory.makeScaleMatrix(xOffsetScaleHistory, yOffsetScaleHistory, zOffsetScaleHistory);
	ofVec3f newOffset = localMatrixScale.getScale();
	xOffsetScaleHistory = newOffset.x;
	yOffsetScaleHistory = newOffset.y;
	zOffsetScaleHistory = newOffset.z;

	//update the stack history	
	stackHistory.push_front(modelMatrixScaleHistory);
	stackHistory.push_front(modelMatrixRotationHistory);
	stackHistory.push_front(modelMatrixTranslationHistory);


	stackRedo.clear();
	if (stackHistory.size() > 15) { //5 transformations maximum
		stackHistory.pop_back();
		stackHistory.pop_back();
		stackHistory.pop_back();
	}
}

void Primitive3d::undoTransform() {
	//prends l'inverse des derni�res transformation pour faire le undo (sauf pour le scale)

	ofVec3f newOffset;
	if (!stackHistory.empty()) {

		//translation		
		localMatrixTranslation = localMatrixTranslation * stackHistory.front().getInverse();
		newOffset = localMatrixTranslation.getTranslation();
		xOffsetTranslate = newOffset.x;
		yOffsetTranslate = newOffset.y;
		zOffsetTranslate = newOffset.z;
		stackRedo.push_front(stackHistory.front());
		stackHistory.pop_front();

		//rotation		
		localMatrixRotation = localMatrixRotation * stackHistory.front().getInverse();
		curRot = localMatrixRotation.getRotate();
		stackRedo.push_front(stackHistory.front());
		stackHistory.pop_front();

		//scale			
		stackRedo.push_front(localMatrixScale);
		localMatrixScale = stackHistory.front();
		newOffset = localMatrixScale.getScale();
		xOffsetScale = newOffset.x;
		yOffsetScale = newOffset.y;
		zOffsetScale = newOffset.z;
		stackHistory.pop_front();


		if (stackRedo.size() > 15) { //5 transformations maximum
			stackRedo.pop_back();
			stackRedo.pop_back();
			stackRedo.pop_back();
		}
	}
}

void Primitive3d::redoTransform() {

	ofVec3f newOffset;
	if (!stackRedo.empty()) {
		//scale		
		stackHistory.push_front(localMatrixScale);
		localMatrixScale = stackRedo.front();//modelMatrixScale * stackRedo.front();
		newOffset = localMatrixScale.getScale();
		xOffsetScale = newOffset.x;
		yOffsetScale = newOffset.y;
		zOffsetScale = newOffset.z;
		stackRedo.pop_front();

		//rotation		
		localMatrixRotation = localMatrixRotation * stackRedo.front();
		curRot = localMatrixRotation.getRotate();
		stackHistory.push_front(stackRedo.front());
		stackRedo.pop_front();

		//translation
		localMatrixTranslation = localMatrixTranslation * stackRedo.front();
		newOffset = localMatrixTranslation.getTranslation();
		xOffsetTranslate = newOffset.x;
		yOffsetTranslate = newOffset.y;
		zOffsetTranslate = newOffset.z;
		stackHistory.push_front(stackRedo.front());
		stackRedo.pop_front();

		if (stackHistory.size() > 15) { //5 transformations maximum
			stackHistory.pop_back();
			stackHistory.pop_back();
			stackHistory.pop_back();
		}

	}
}
void Primitive3d::resetTransform() {
	//reset toutes les rotation et scale. Ne reset pas les translation x et y pour �viter que tous les objets se retrouve aglutiner au centre d'un coup

	stackHistory.clear();
	stackRedo.clear();
	ofQuaternion start;
	curRot = start;

	xOffsetScale = 1;
	yOffsetScale = 1;
	zOffsetScale = 1;


	xOffsetScaleHistory = 1;
	yOffsetScaleHistory = 1;
	zOffsetScaleHistory = 1;
	curRotHistory = start;

	zOffsetTranslate = 0;

	localMatrixTranslation.setTranslation(xOffsetTranslate, yOffsetTranslate, zOffsetTranslate);
	localMatrixRotation.setRotate(curRot);
	localMatrixScale.makeScaleMatrix(xOffsetScale, yOffsetScale, zOffsetScale);
	
}

//-------------------------------------------------------------------------------------------------
//Fonction utile pour la cam�ra
ofVec3f Primitive3d::getPosition(ofMatrix4x4 modelMatrix) {	
	ofVec3f position;
	switch (type)
	{
	case VectorPrimitive3d::CUBE:
		position = ofVec3f(0,0,0) * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix;
		break;
	case VectorPrimitive3d::OBJET_OBJ:
		position = model.getSceneCenter() * model.getModelMatrix()* localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix;		
		break;
	case VectorPrimitive3d::HILBERTCURVE:
		position = ofVec3f(0, 0, 0) * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix;
		break;
	case VectorPrimitive3d::TORUS:
		position = ofVec3f(0, 0, 0) * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix;
		break;
	case VectorPrimitive3d::OCTAEDRE:
		position = ofVec3f(0, 0, 0) * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix;
		break;
	case VectorPrimitive3d::SPHERE:
		position = ofVec3f(0, 0, 0) * localMatrixScale * localMatrixRotation * localMatrixTranslation * modelMatrix;
		break;
	default:
		break;

	}
	return position;
}

//-------------------------------------------------------------------------------------------------
//Curve
bool Primitive3d::setselectedCtrlPoint(int xPressed, int yPressed, int z, ofMatrix4x4 modelMatrix, ofCamera * cam, bool changedSelection) {

	ofBoxPrimitive myBox;
	myBox.setPosition(0, 0, 0);
	myBox.set(radiusCtrlPoint*2, radiusCtrlPoint*2, radiusCtrlPoint*2);
	bool isInside = false;
	
	for (int i = 0; i< ctrlPointCurve.size(); i++) {
		myBox.setPosition(ctrlPointCurve[i].x, ctrlPointCurve[i].y, ctrlPointCurve[i].z);
		isInside = inside(xPressed, yPressed, z, modelMatrix, myBox, cam);
		if (isInside && changedSelection) {
			if (ctrlPointSelectedIndex == i)
				ctrlPointSelectedIndex = -1;
			else {
				unSelectCtrlPoint();
				ctrlPointSelectedIndex = i;
			}
			return isInside;
		}
	}
	return isInside;
}

void Primitive3d::unSelectCtrlPoint() {
	ctrlPointSelectedIndex = -1;	

}

void Primitive3d::setTranslateCtrlPoint(ofVec3f offset) {
	ofMatrix4x4 localmodel = localMatrixScale *  localMatrixRotation ;

	if (type == VectorPrimitive3d::COONS) {
		
			if(ctrlPointSelectedIndex == 0 || ctrlPointSelectedIndex == 8) {
				ctrlPointCurve[0] += offset *localmodel.getInverse();
				ctrlPointCurve[8] += offset *localmodel.getInverse();
			}
			else if (ctrlPointSelectedIndex == 3 || ctrlPointSelectedIndex == 12) {
				ctrlPointCurve[3] += offset *localmodel.getInverse();
				ctrlPointCurve[12] += offset *localmodel.getInverse();
			}
			else if (ctrlPointSelectedIndex == 11 || ctrlPointSelectedIndex == 4) {
				ctrlPointCurve[11] += offset *localmodel.getInverse();
				ctrlPointCurve[4] += offset *localmodel.getInverse();
			}
			else if (ctrlPointSelectedIndex == 7 || ctrlPointSelectedIndex == 15) {
				ctrlPointCurve[7] += offset *localmodel.getInverse();
				ctrlPointCurve[15] += offset *localmodel.getInverse();
			}
			else {
				if (ctrlPointSelectedIndex != -1) {
					ctrlPointCurve[ctrlPointSelectedIndex] += offset *localmodel.getInverse();
					
				}
				
			}

			update();
		
	}
	else {
		if (ctrlPointSelectedIndex != -1) {
			ctrlPointCurve[ctrlPointSelectedIndex] += offset *localmodel.getInverse();
			update();
		}	
	}
	
	
}
void Primitive3d::addCtrlPoint(ofVec3f pts, ofMatrix4x4 modelMatrix, ofCamera * cam) {
	if (type == VectorPrimitive3d::CATMULLROM) {		                                                                           	
		ofMatrix4x4 localmodel = localMatrixScale *  localMatrixRotation * localMatrixTranslation;
		ofVec3f result2 = cam->worldToScreen(ofVec3f(pts.x, pts.y, 0));
		ofVec3f result = cam->screenToWorld(ofVec3f(pts.x, pts.y, result2.z)) * modelMatrix.getInverse() * localmodel.getInverse() ;
		ctrlPointCurve.push_back(result); //0.92
		update();
	}
}


void Primitive3d::deleteCtrlPoint() {
	if (type == VectorPrimitive3d::CATMULLROM) {
		if (ctrlPointCurve.size() > 2) {
			int i_delete = 0;
			while (i_delete < ctrlPointCurve.size()) {
				if (ctrlPointSelectedIndex == i_delete) {				
					ctrlPointCurve.erase(ctrlPointCurve.begin() + i_delete);
					i_delete = 0;
					ctrlPointSelectedIndex = -1;

				}
				else
					i_delete++;
			}
		}
		update();
	}
}

void Primitive3d::bezierCurve(std::vector<ofVec3f> ctrlP, float t, ofVec3f& positionCurve1, int i) {
	
	
	bezierCubic(
		t,
		ctrlP[i].x, ctrlP[i].y, ctrlP[i].z,
		ctrlP[i + 1].x, ctrlP[i + 1].y, ctrlP[i + 1].z,
		ctrlP[i + 2].x, ctrlP[i + 2].y, ctrlP[i + 2].z,
		ctrlP[i + 3].x, ctrlP[i + 3].y, ctrlP[i + 3].z,
		positionCurve1.x, positionCurve1.y, positionCurve1.z);
	
	
}

void Primitive3d::setPosition(ofVec3f position) {	
	xOffsetTranslate = position.x;
	yOffsetTranslate = position.y;
	zOffsetTranslate = position.z;

	localMatrixTranslation.setTranslation(xOffsetTranslate, yOffsetTranslate, zOffsetTranslate);

}

void Primitive3d::setOrientation(ofVec3f Orientation, ofVec3f position) {
	
	ofVec3f up(0,-1,0);
	
	ofVec3f zaxis = ( position - Orientation).getNormalized();
	
	ofVec3f xaxis = up.getCrossed(zaxis).getNormalized();	
	ofVec3f yaxis = zaxis.getCrossed(xaxis);	
	
	ofMatrix4x4 matrice = ofMatrix4x4(xaxis.x, xaxis.y, xaxis.z, 0, yaxis.x, yaxis.y, yaxis.z, 0, zaxis.x, zaxis.y, zaxis.z, 0, 1, 1, 1, 1);
	curRot = matrice.getRotate();
	localMatrixRotation = matrice;	

}

std::vector<ofVec3f> Primitive3d::getLineMeshCurve() {
	std::vector<ofVec3f> catmullCurvePoint = lineMeshCurve.getVertices();

	for (int i = 0; i < catmullCurvePoint.size(); i++) {
		catmullCurvePoint[i] = catmullCurvePoint[i] * localMatrixScale * localMatrixRotation * localMatrixTranslation;
	}
	return catmullCurvePoint;
}

VectorPrimitive3d  Primitive3d::get3dType() {
	return type;
}

void Primitive3d::showCatmullRom(bool isShow) {
	isShowCatmullRom = isShow;


}
Primitive3d::~Primitive3d() {


	
}