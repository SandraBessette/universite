// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "lSystem.h"
#include <memory>


// �num�ration des diff�rents types de primitives vectorielles
enum class VectorPrimitive3d { CUBE, SPHERE, OBJET_OBJ,HILBERTCURVE, TORUS, OCTAEDRE, BEZIER, HERMITE, CATMULLROM, COONS};
// �num�ration des diff�rents types de mat�riaux
enum class Material3d { NONE, BRASS, CUPPER, RUBY};

// fonction d'�valuation d'une courbe de B�zier cubique (4 points de contr�le)
inline void bezierCubic(
	float t,
	float p1x, float p1y, float p1z,
	float p2x, float p2y, float p2z,
	float p3x, float p3y, float p3z,
	float p4x, float p4y, float p4z,
	float&  x, float& y, float&  z)
{
	float u = 1 - t;
	float uu = u * u;
	float uuu = uu * u;
	float tt = t * t;
	float ttt = tt * t;

	x = uuu * p1x + 3 * uu * t * p2x + 3 * u * tt * p3x + ttt * p4x;
	y = uuu * p1y + 3 * uu * t * p2y + 3 * u * tt * p3y + ttt * p4y;
	z = uuu * p1z + 3 * uu * t * p2z + 3 * u * tt * p3z + ttt * p4z;
}

// fonction d'�valuation d'une courbe de hermite (4 points de contr�le)
inline void hermite(
	float t,
	float p1x, float p1y, float p1z,
	float p2x, float p2y, float p2z,
	float p3x, float p3y, float p3z,
	float p4x, float p4y, float p4z,
	float&  x, float& y, float&  z)
{
	float u = 1 - t;
	float uu = u * u;
	float uuu = uu * u;
	float tt = t * t;
	float ttt = tt * t;

	x = (2 * ttt - 3 * tt + 1) * p1x + (ttt - 2 * tt + t) * p2x + (ttt - tt) * p3x + (-2 * ttt + 3 * tt) * p4x;
	y = (2 * ttt - 3 * tt + 1) * p1y + (ttt - 2 * tt + t) * p2y + (ttt - tt) * p3y + (-2 * ttt + 3 * tt) * p4y;
	z = (2 * ttt - 3 * tt + 1) * p1z + (ttt - 2 * tt + t) * p2z + (ttt - tt) * p3z + (-2 * ttt + 3 * tt) * p4z;
}

inline void catmullRom(
	float t,
	float p1x, float p1y, float p1z,
	float p2x, float p2y, float p2z,
	float p3x, float p3y, float p3z,
	float p4x, float p4y, float p4z,
	float&  x, float& y, float&  z){
	
	

	ofVec3f p0 = ofVec3f(p1x, p1y, p1z);
	ofVec3f p1 = ofVec3f(p2x, p2y, p2z);
	ofVec3f p2 = ofVec3f(p3x, p3y, p3z);
	ofVec3f p3 = ofVec3f(p4x, p4y, p4z);
	ofVec3f result;
	result = 0.5f * ((2 * p1) + (p2 - p0) * t +	(2 * p0 - 5 * p1 + 4 * p2 - p3) * t * t + (3 * p1 - p0 - 3 * p2 + p3) * t * t * t);

	x = result.x;
	y = result.y;
	z = result.z;
}

class Primitive3d
{
private:	

	//Style setting
	struct Material {		
		ofVec3f ambiant;
		ofVec3f diffuse;
		ofVec3f specular;
		float brightness;
	};

	std::shared_ptr<Material> material ;
	std::shared_ptr<Material> materialNone ;
	std::shared_ptr<Material> materialBrass;
	std::shared_ptr<Material> materialCupper;
	std::shared_ptr<Material> materialRuby;	

	Material3d typeMaterial;

	VectorPrimitive3d type;	
	ofVec3f positionBase;
	ofParameterGroup parameters;
	ofParameter<float> height;
	ofParameter<float> width;
	ofParameter<float> depth;
	ofParameter<ofColor> color;
	ofParameter<bool> filled;
	ofParameter<float> strokeWidth;
	ofParameter<ofColor> strokeColor;
	ofParameter<ofVec2f> center;
	std::string name;
	

	//Offset for transformations
	float xOffsetRotate = 0;
	float yOffsetRotate = 0;
	float zOffsetRotate = 0;

	float xOffsetTranslate = 0;
	float yOffsetTranslate = 0;
	float zOffsetTranslate = 0;

	float xOffsetScale = 1;
	float yOffsetScale = 1;
	float zOffsetScale = 1;

	float xOffsetTranslateHistory = 0;
	float yOffsetTranslateHistory = 0;
	float zOffsetTranslateHistory = 0;

	float xOffsetScaleHistory = 1;
	float yOffsetScaleHistory = 1;
	float zOffsetScaleHistory = 1;

	

	ofMatrix4x4 localMatrixTranslation;
	ofMatrix4x4 localMatrixRotation;
	ofMatrix4x4 localMatrixScale;

	ofLight light;

	//current state of the rotation  
	ofQuaternion curRot;
	ofQuaternion curRotHistory;

	std::deque<ofMatrix4x4> stackHistory;
	std::deque<ofMatrix4x4> stackRedo;

	ofVec3f yMax;
	ofVec3f yMin;
	ofVec3f xMax;
	ofVec3f xMin;

	ofxAssimpModelLoader model;
	ofMesh modelMesh;
	LSystem hilbertCurve;	

	ofVboMesh meshTorus;
	ofVboMesh meshOctaedre;

	//Curve
	std::vector<ofVec3f> ctrlPointCurve;	
	
	ofVboMesh lineMeshCurve;
	int ctrlPointSelectedIndex = -1;
	ofVec3f positionCurve;
	int lineResolution;
	int radiusCtrlPoint;
	ofPolyline ctrlLine1;
	ofPolyline ctrlLine2;
	ofPolyline ctrlLine3;
	ofPolyline ctrlLine4;
	bool isShowCatmullRom;

public:
	bool isSelected = false;
	ofImage imageBrick;
	ofImage imageBrickNormalMap;
	ofBoxPrimitive myBoxDraw; 

	Primitive3d(VectorPrimitive3d pType, ofVec3f positionBases, float pHeight, float pWidth, float pdepth, ofColor pColor, bool pFilled, float pStrokeWidth, ofColor pStrokeColor);
	Primitive3d(ofxAssimpModelLoader pModel, ofVec3f positionBases);
	~Primitive3d();

	void setup();
	void updatePrimitive();
	void draw(ofShader * shader, bool isTexture, ofShader * shaderTexture);
	void update();
	void drawZone();
	void drawControlPoint();

	//material
	void setMaterial(Material3d typeMat);

	//Style setting
	void setFilled(bool isFilled);
	void setColor(ofColor pColor);
	void setStrokeWidth(float pStrokeWidth);
	void setStrokeColor(ofColor pColor);
	void setHeight(float pHeight);
	void setWidth(float pWidth);
	void setDepth(float pDepth);

	//Slections
	bool inside(int px, int py, int pz, ofMatrix4x4 modelMatrix, ofBoxPrimitive myBox, ofCamera * cam);
	bool insideOBJ(int px, int py, int pz, ofMatrix4x4 modelMatrix, ofCamera * cam);
	bool Primitive3d::insideCurve(int px, int py, int pz, ofMatrix4x4 modelMatrix, ofVboMesh myMesh, ofCamera * cam);
	bool isInsideShape(int xPressed, int yPressed, int z, ofMatrix4x4 modelMatrix, ofCamera * cam, bool changedSelection = true);

	//Transformations
	void setOffsetTranslate(ofVec3f offset);
	void setOffsetRotateXY(ofVec3f offset, float delay);
	void setOffsetRotateZ(ofVec3f offset, float delay);
	void setOffsetRotateY(ofVec3f offset, float delay);
	void setOffsetRotateX(ofVec3f offset, float delay);
	void setOffsetScale(ofVec3f offset);

	//undo redo
	void keepTransformHistory();
	void undoTransform();
	void redoTransform();
	void resetTransform();

	//Fonction utile pour la cam�ra
	ofVec3f getPosition(ofMatrix4x4 modelMatrix);	

	//Curve
	bool setselectedCtrlPoint(int xPressed, int yPressed, int z, ofMatrix4x4 modelMatrix, ofCamera * cam, bool changedSelection = true);
	void unSelectCtrlPoint();
	void setTranslateCtrlPoint(ofVec3f offset);
	void drawCtrlPointZone();
	void addCtrlPoint(ofVec3f pts, ofMatrix4x4 modelMatrix, ofCamera * cam);
	void deleteCtrlPoint();
	void bezierCurve(std::vector<ofVec3f> ctrlP, float t, ofVec3f& positionCurve1, int i);
	void setPosition(ofVec3f position);
	std::vector<ofVec3f> getLineMeshCurve();
	VectorPrimitive3d get3dType();
	void setOrientation(ofVec3f Orientation, ofVec3f position);
	void showCatmullRom(bool isShow);
};
