// Programme fait par Sandra Bessette pour le cours IFT3100, 12 mars 2017

#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){
	/*ofSetupOpenGL(1024,768,OF_WINDOW);			// <-------- setup the GL context

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:*/
	// param�tres du contexte de rendu OpenGL
	ofGLWindowSettings windowSettings;

	windowSettings.width = 1024;
	windowSettings.height = 768;
	int glVersionMajor;
	int glVersionMinor;

	// s�lection de la version de OpenGL : option #1
	// glVersionMajor = 2;
	//glVersionMinor = 1;

	// s�lection de la version de OpenGL : option #2
	 glVersionMajor = 3;
	 glVersionMinor = 3;

	// s�lection de la version de OpenGL : option #3
	 //glVersionMajor = 3;
	 //glVersionMinor = 2;

	windowSettings.setGLVersion(glVersionMajor, glVersionMinor);

	// cr�ation de la fen�tre
	ofCreateWindow(windowSettings);

	

	ofRunApp(new ofApp(glVersionMajor, glVersionMinor));

}
