#version 330

uniform sampler2DRect fboTexture;
uniform float blur;

in vec2 texCoordVarying;
out vec4 outputColor;

void main()
{

	vec4 color = vec4(0.0);
	
    color += texture(fboTexture, texCoordVarying + vec2(blur * -4.0, 0.0)) * 0.0162162162;
    color += texture(fboTexture, texCoordVarying + vec2(blur * -3.0, 0.0)) * 0.0540540541;
    color += texture(fboTexture, texCoordVarying + vec2(blur * -2.0, 0.0)) * 0.1216216216;
    color += texture(fboTexture, texCoordVarying + vec2(blur * -1.0, 0.0)) * 0.1945945946;
    
    color += texture(fboTexture, texCoordVarying + vec2(blur, 0)) * 0.2270270270;
	
    color += texture(fboTexture, texCoordVarying + vec2(blur * 1.0, 0.0))* 0.1945945946;
    color += texture(fboTexture, texCoordVarying + vec2(blur * 2.0, 0.0)) * 0.1216216216;
    color += texture(fboTexture, texCoordVarying + vec2(blur * 3.0, 0.0)) * 0.0540540541;
    color += texture(fboTexture, texCoordVarying + vec2(blur * 4.0, 0.0)) * 0.0162162162; 
    
    
    outputColor = color;
}