
#version 330
#extension GL_EXT_geometry_shader4 : enable


in vec3 interpolationPosition1[];
in vec3 interpolationNormal1[];

//pour le fragment shader
out vec3 interpolationPosition;
out vec3 interpolationNormal;

// attributs uniformes
uniform bool isSphere;
uniform float time;

const float MAGNITUDE = 10.0f;

vec4 explode(vec4 position, vec3 normal){
    
    vec3 direction = normal * ((sin(time) + 1.0f) / 2.0f) * MAGNITUDE; 
	return position + vec4(direction, 0.0f);
}

vec3 GetNormal()
{

	vec3 a;
	vec3 b;	

	a = vec3(gl_in[0].gl_Position.xyz) - vec3(gl_in[1].gl_Position.xyz);
    b = vec3(gl_in[2].gl_Position.xyz) - vec3(gl_in[1].gl_Position.xyz);
	
	if(isSphere){
		a = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
		b = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	}
	vec3 result = normalize(cross(a, b));	
    return result;
}

void main() {    
    vec3 normal = GetNormal();
	int i;
	for (i = 0; i < gl_in.length(); i++)
	{
		gl_Position = explode(gl_in[i].gl_Position, normal);  	
		interpolationPosition = interpolationPosition1[i];
		interpolationNormal = interpolationNormal1[i];			
		EmitVertex();
	}
  
    EndPrimitive();
}