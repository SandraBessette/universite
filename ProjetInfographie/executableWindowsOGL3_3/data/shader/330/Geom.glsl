#version 330 core
#extension GL_EXT_geometry_shader4 : enable


const float MAGNITUDE = 10.0f;
uniform bool isSphere;
uniform float time;

void main()
{
   
	vec3 a;
	vec3 b;
	a = gl_in[0].gl_Position.xyz - gl_in[1].gl_Position.xyz;
	b = gl_in[2].gl_Position.xyz - gl_in[1].gl_Position.xyz;
	if(isSphere){
		a = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
		b = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	}
	vec3 face_normal = normalize(cross(a, b));

	vec4 center = (gl_in[0].gl_Position +
                         gl_in[1].gl_Position +
                         gl_in[2].gl_Position) / 3.0;


	gl_Position = center;
	EmitVertex();

	gl_Position =  (center + vec4(face_normal * ((sin(time) + 1.5f) / 2.0f) * MAGNITUDE, 0.0));
	EmitVertex();
	EndPrimitive();
}