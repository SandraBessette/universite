#version 330
// attributs de sommet
in vec4 position;
in vec4 normal;
in vec2 texcoord;

// attributs en sortie
out vec3 interpolationPosition;
out vec2 texCoordVarying;

// position  de la lumiere PointLight
out vec3 pLightPosition1t;
out vec3 pLightPosition2t;
out vec3 pLightPosition3t;
out vec3 pLightPosition4t;

out vec3 sLightPosition1t;

// direction de la lumiere directionel
out vec4 lightDirt;

// direction de la lumiere spotlight
out vec4 spotLightDirt;

// attributs uniformes
uniform mat4x4 modelViewMatrix;
uniform mat4x4 projectionMatrix;

// position  de la lumiere PointLight
uniform vec3 pLightPosition1;
uniform vec3 pLightPosition2;
uniform vec3 pLightPosition3;
uniform vec3 pLightPosition4;

// position  de la lumiere SpotLight
uniform vec3 sLightPosition1;

// direction de la lumiere directionel
uniform vec4 lightDir;

// direction de la lumiere spotlight
uniform vec4 spotLightDir;

void main()
{
    	
	vec3 ref = vec3(1,0,0);
	vec3 tangent;
	vec3 bitangent;
	if(normal.xyz == ref || normal.xyz == -ref ){
		
		if(normal.xyz == -ref)
			tangent = cross(normal.xyz, vec3(0.0, 0.0, -1.0));// argument different to ref
		else
			tangent = cross(normal.xyz, vec3(0.0, 0.0, 1.0));
	}else{
		tangent = cross(normal.xyz, ref);
	}	
	bitangent = cross(normal.xyz, tangent);
	
		
// transformation de la position du sommet par les matrices de modèle, vue et projection
	gl_Position = projectionMatrix * modelViewMatrix * position;
	
	// transformation de la position du sommet dans l'espace de vue
	interpolationPosition = vec3(modelViewMatrix * position);
	
	mat4x4 normalMatrix1 = transpose(inverse(modelViewMatrix));
	mat3 normalMatrix = mat3(normalMatrix1);
	
    vec3 T = normalize(normalMatrix * tangent);
    vec3 B = normalize(normalMatrix * bitangent);
    vec3 N = normalize(normalMatrix * normal.xyz);    
    
    mat3 TBN = transpose(mat3(T, B, N));
	interpolationPosition = TBN * interpolationPosition;
    pLightPosition1t = TBN * pLightPosition1;
	pLightPosition2t = TBN * pLightPosition2;
	pLightPosition3t = TBN * pLightPosition3;
	pLightPosition4t = TBN * pLightPosition4;
	sLightPosition1t = TBN * sLightPosition1;
   	lightDirt  = vec4(vec3(TBN * lightDir.xyz), 0.0);
    spotLightDirt = vec4(vec3(TBN * spotLightDir.xyz), 0.0);	
	
	
	texCoordVarying = texcoord;

	
	
}