#version 150
// attributs interpolés à partir des valeurs en sortie du shader de sommet
in vec3 interpolationPosition;
in vec3 interpolationNormal;

// attribut en sortie
out vec4 fragmentColor;

// couleurs de réflexion du matériau
uniform vec3 materialColorAmbient;
uniform vec3 materialColorDiffuse;
uniform vec3 materialColorSpecular;

// facteur de brillance spéculaire du matériau
uniform float materialBrightness;

//composante alpha de la couleur du matériaux
uniform float alpha;

//aténuation de la lumière de type PointLight
uniform float constant;
uniform float linear;
uniform float quadratic;

// couleurs de la lumiere Ambiante
uniform vec3 lightColorAmbient;

// position  de la lumiere PointLight
uniform vec3 pLightPosition1;
uniform vec3 pLightPosition2;
uniform vec3 pLightPosition3;
uniform vec3 pLightPosition4;

// couleurs de la lumiere PointLight
uniform vec3 pLightColorDiffuse1;
uniform vec3 pLightColorSpecular1;
uniform vec3 pLightColorDiffuse2;
uniform vec3 pLightColorSpecular2;
uniform vec3 pLightColorDiffuse3;
uniform vec3 pLightColorSpecular3;
uniform vec3 pLightColorDiffuse4;
uniform vec3 pLightColorSpecular4;

// position  de la lumiere SpotLight
uniform vec3 sLightPosition1;

// couleurs de la lumiere Directionel
uniform vec3 dLightColorDiffuse1;
uniform vec3 dLightColorSpecular1;

// couleurs de la lumiere Spotlight
uniform vec3 sLightColorDiffuse1;
uniform vec3 sLightColorSpecular1;

// direction de la lumiere directionel
uniform vec4 lightDir;

// direction de la lumiere spotlight
uniform vec4 spotLightDir;

// cutOff lumière spotlight
uniform float cutOff;
uniform float outerCutOff;

//lumiere PointLight active ou pas
uniform bool pLightIsOn1;
uniform bool pLightIsOn2;
uniform bool pLightIsOn3;
uniform bool pLightIsOn4;

//lumiere Directionel active ou pas
uniform bool dLightIsOn1;

//lumiere Ambiante active ou pas
uniform bool aLightIsOn1;

//lumiere Spotlight active ou pas
uniform bool sLightIsOn1;

vec3 CalcPointLight(vec3 lightPosition, vec3 interpolationNormal, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular);
vec3 CalcDirLight(vec4 lightDirection, vec3 interpolationNormal, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular);
vec3 CalcSpotLight(vec4 lightDirection, vec3 lightPosition, vec3 interpolationNormal, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular);

void main()
{    
	vec3 result = vec3(0,0,0);
	if(aLightIsOn1)
		result += lightColorAmbient * materialColorAmbient;
	if(pLightIsOn1)
		result += CalcPointLight(pLightPosition1, interpolationNormal, interpolationPosition, pLightColorDiffuse1, pLightColorSpecular1);
	if(pLightIsOn2)
		result += CalcPointLight(pLightPosition2, interpolationNormal, interpolationPosition, pLightColorDiffuse2, pLightColorSpecular2);
	if(pLightIsOn3)
		result += CalcPointLight(pLightPosition3, interpolationNormal, interpolationPosition, pLightColorDiffuse3, pLightColorSpecular3);
	if(pLightIsOn4)
		result += CalcPointLight(pLightPosition4, interpolationNormal, interpolationPosition, pLightColorDiffuse4, pLightColorSpecular4);
	if(dLightIsOn1)		
		result += CalcDirLight(lightDir,interpolationNormal, interpolationPosition, dLightColorDiffuse1, dLightColorSpecular1);	
    if(sLightIsOn1)
		result += CalcSpotLight(spotLightDir, sLightPosition1, interpolationNormal, interpolationPosition, sLightColorDiffuse1, sLightColorSpecular1);
	
	fragmentColor = vec4(result , alpha);
}

vec3 CalcPointLight(vec3 lightPosition, vec3 interpolationNormal, vec3 interpolationPosition,vec3 lightColorDiffuse, vec3 lightColorSpecular)
{
// re-normaliser la normale après interpolation
	vec3 N = normalize(interpolationNormal);

	// calculer la direction de la surface vers la lumière (L)
	vec3 L = normalize(lightPosition - interpolationPosition);

	// calculer le niveau de réflexion diffuse (N • L)
	float reflectionDiffuse = max(dot(N, L), 0.0);

	// réflexion spéculaire par défaut
	float reflectionSpecular = 0.0;

	// calculer la réflexion spéculaire seulement s'il y a réflexion diffuse
	if(reflectionDiffuse > 0.0)
	{
		// calculer la direction de la surface vers la caméra (V)
		vec3 V = normalize(-interpolationPosition);		
		
		vec3 H = normalize(V+L);

		// calculer la réflexion spéculaire entre (H dot N)
		reflectionSpecular = pow(max(dot(N, H), 0.0), materialBrightness);
	}
	
	// Attenuation
    float distance = length(lightPosition - interpolationPosition);
    float attenuation = 1.0f / (constant + linear * distance + quadratic * (distance * distance)); 	
	
		
    vec3 diffuse = lightColorDiffuse * materialColorDiffuse  * reflectionDiffuse ;  
    vec3 specular = lightColorSpecular * materialColorSpecular * reflectionSpecular;
   
    diffuse *= attenuation;
    specular *= attenuation;		
	
    return (vec3(diffuse + specular));

}

vec3 CalcDirLight(vec4 lightDirection, vec3 interpolationNormal, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular)
{
// re-normaliser la normale après interpolation
	vec3 N = normalize(interpolationNormal);

	// calculer la direction de la surface vers la lumière (L)
	vec3 lithD = lightDirection.xyz;
	vec3 L = normalize(-lithD); /// faut pas le mettre négatif je crois

	// calculer le niveau de réflexion diffuse (N • L)
	float reflectionDiffuse = max(dot(N, L), 0.0);

	// réflexion spéculaire par défaut
	float reflectionSpecular = 0.0;

	// calculer la réflexion spéculaire seulement s'il y a réflexion diffuse
	if(reflectionDiffuse > 0.0)
	{
		// calculer la direction de la surface vers la caméra (V)
		vec3 V = normalize(-interpolationPosition);
		
		vec3 H = normalize(V+L);

		// calculer la réflexion spéculaire entre (H dot N)
		reflectionSpecular = pow(max(dot(N, H), 0.0), materialBrightness);
	}
			
 
   vec3 diffuse = lightColorDiffuse * materialColorDiffuse  * reflectionDiffuse ;  
   vec3 specular = lightColorSpecular * materialColorSpecular * reflectionSpecular;
	
	return (vec3(diffuse + specular));
}

vec3 CalcSpotLight(vec4 lightDirection, vec3 lightPosition, vec3 interpolationNormal, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular)
{

	vec3 diffuse = vec3(0,0,0);
	vec3 specular = vec3(0,0,0);
	// re-normaliser la normale après interpolation
	vec3 N = normalize(interpolationNormal);

	// calculer la direction de la surface vers la lumière (L)
	vec3 L = normalize(lightPosition - interpolationPosition);

	//Direction du rayon incident de la lumière
	vec3 I = -L;
	
	// direction vers laquelle pointe la lumière
	vec3 lithD = lightDirection.xyz;
	vec3 D = normalize(lithD); 	
	
	float reflectionDiffuse = max(dot(N, L), 0.0);	

	// réflexion spéculaire par défaut
	float reflectionSpecular = 0.0;

	// calculer la réflexion spéculaire seulement s'il y a réflexion diffuse
	if(reflectionDiffuse > 0.0)
	{
		// calculer la direction de la surface vers la caméra (V)
		vec3 V = normalize(-interpolationPosition);
		
		
		vec3 H = normalize(V+L);

		// calculer la réflexion spéculaire entre (H dot N)
		reflectionSpecular = pow(max(dot(N, H), 0.0), materialBrightness);
	}	 
	
	// Attenuation
	float distance = length(lightPosition - interpolationPosition);
	float attenuation = 1.0f / (constant + linear * distance + quadratic * (distance * distance)); 
	
	diffuse = lightColorDiffuse * materialColorDiffuse  * reflectionDiffuse ;  
	specular = lightColorSpecular * materialColorSpecular * reflectionSpecular;   
	diffuse *= attenuation;
	specular *= attenuation;		
	
	  // Spotlight (soft edges)
    float theta = dot(I, D); 
    float epsilon = (cutOff - outerCutOff);
    float intensity = clamp((theta - outerCutOff) / epsilon, 0.0, 1.0);
  
    
    diffuse  *= intensity;
    specular *= intensity;		
	
    return (vec3(diffuse + specular));

}