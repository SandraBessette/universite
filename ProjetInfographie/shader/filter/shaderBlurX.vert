#version 330
// attributs de sommet
in vec4 position;
in vec2 texcoord;

// attributs en sortie

out vec2 texCoordVarying;

// attributs uniformes
uniform mat4x4 modelViewMatrix;
uniform mat4x4 projectionMatrix;

void main()
{
    
	texCoordVarying = texcoord;
	
	// transformation de la position du sommet par les matrices de modèle, vue et projection
	gl_Position = projectionMatrix * modelViewMatrix * position;
}