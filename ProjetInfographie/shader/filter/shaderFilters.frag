#version 330

uniform sampler2DRect fboTexture;
uniform float offset;
uniform mat3x3 filterMatrix;

in vec2 texCoordVarying;
out vec4 outputColor;

void main()
{
	
   vec4 color = vec4(0.0);
		
    color += texture(fboTexture, texCoordVarying + vec2(offset * -1.0, offset * 1.0)) * filterMatrix[0].x;
    color += texture(fboTexture, texCoordVarying + vec2(0.0, offset* 1.0)) * filterMatrix[1].x;
    color += texture(fboTexture, texCoordVarying + vec2(offset * 1.0, offset * 1.0)) * filterMatrix[2].x;
    color += texture(fboTexture, texCoordVarying + vec2(offset *-1.0, offset * 0.0)) * filterMatrix[0].y;
    
    color += texture(fboTexture, texCoordVarying + vec2(0.0, offset * 0)) * filterMatrix[1].y;
	
     color += texture(fboTexture, texCoordVarying + vec2(offset * 1.0, offset * 0.0)) * filterMatrix[2].y;
    color += texture(fboTexture, texCoordVarying + vec2(offset * -1.0, offset * -1.0)) * filterMatrix[0].z;
    color += texture(fboTexture, texCoordVarying + vec2(offset * 0.0, offset * -1.0)) * filterMatrix[1].z;
    color += texture(fboTexture, texCoordVarying + vec2(offset *1.0, offset * -1.0)) * filterMatrix[2].z;
	
   
    
    outputColor = color;
}