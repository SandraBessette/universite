#version 330 core
out vec4 fragcolor;
uniform vec3 materialColorDiffuse;
void main()
{
   
   fragcolor = vec4(materialColorDiffuse, 1.0f);
	  
}