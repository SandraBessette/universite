#version 330
// attributs interpolés à partir des valeurs en sortie du shader de sommet
in vec3 interpolationPosition;
in vec2 texCoordVarying;

// attribut en sortie
out vec4 fragmentColor;

// couleurs de réflexion du matériau
uniform vec3 materialColorAmbient;
uniform vec3 materialColorSpecular;


uniform sampler2DRect diffuseMap;
uniform sampler2DRect normalMap;


// facteur de brillance spéculaire du matériau
uniform float materialBrightness;

//composante alpha de la couleur du matériaux
uniform float alpha;

//aténuation de la lumière de type PointLight
uniform float constant;
uniform float linear;
uniform float quadratic;

// couleurs de la lumiere Ambiante
uniform vec3 lightColorAmbient;

// position  de la lumiere PointLight
in vec3 pLightPosition1t;
in vec3 pLightPosition2t;
in vec3 pLightPosition3t;
in vec3 pLightPosition4t;

// couleurs de la lumiere PointLight
uniform vec3 pLightColorDiffuse1;
uniform vec3 pLightColorSpecular1;
uniform vec3 pLightColorDiffuse2;
uniform vec3 pLightColorSpecular2;
uniform vec3 pLightColorDiffuse3;
uniform vec3 pLightColorSpecular3;
uniform vec3 pLightColorDiffuse4;
uniform vec3 pLightColorSpecular4;

// position  de la lumiere SpotLight
in vec3 sLightPosition1t;

// couleurs de la lumiere Directionel
uniform vec3 dLightColorDiffuse1;
uniform vec3 dLightColorSpecular1;

// couleurs de la lumiere Spotlight
uniform vec3 sLightColorDiffuse1;
uniform vec3 sLightColorSpecular1;

// direction de la lumiere directionel
in vec4 lightDirt;

// direction de la lumiere spotlight
in vec4 spotLightDirt;

// cutOff lumière spotlight
uniform float cutOff;
uniform float outerCutOff;

//lumiere PointLight active ou pas
uniform bool pLightIsOn1;
uniform bool pLightIsOn2;
uniform bool pLightIsOn3;
uniform bool pLightIsOn4;

//lumiere Directionel active ou pas
uniform bool dLightIsOn1;

//lumiere Ambiante active ou pas
uniform bool aLightIsOn1;

//lumiere Spotlight active ou pas
uniform bool sLightIsOn1;

vec3 CalcPointLight(vec3 lightPosition, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular);
vec3 CalcDirLight(vec4 lightDirection, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular);
vec3 CalcSpotLight(vec4 lightDirection, vec3 lightPosition, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular);

void main()
{    
	vec3 result = vec3(0,0,0);
	if(aLightIsOn1)
		result += lightColorAmbient *  texture(diffuseMap, texCoordVarying).rgb;	
	if(pLightIsOn1)
		result += CalcPointLight(pLightPosition1t, interpolationPosition, pLightColorDiffuse1, pLightColorSpecular1);
	if(pLightIsOn2)
		result += CalcPointLight(pLightPosition2t, interpolationPosition, pLightColorDiffuse2, pLightColorSpecular2);
	if(pLightIsOn3)
		result += CalcPointLight(pLightPosition3t, interpolationPosition, pLightColorDiffuse3, pLightColorSpecular3);
	if(pLightIsOn4)
		result += CalcPointLight(pLightPosition4t, interpolationPosition, pLightColorDiffuse4, pLightColorSpecular4);
	if(dLightIsOn1)		
		result += CalcDirLight(lightDirt, interpolationPosition, dLightColorDiffuse1, dLightColorSpecular1);	
    if(sLightIsOn1)
		result += CalcSpotLight(spotLightDirt, sLightPosition1t, interpolationPosition, sLightColorDiffuse1, sLightColorSpecular1);

	fragmentColor = vec4(result , alpha);
}

vec3 CalcPointLight(vec3 lightPosition, vec3 interpolationPosition,vec3 lightColorDiffuse, vec3 lightColorSpecular)
{

	vec3 normal = texture(normalMap, texCoordVarying).rgb;
	// Transform normal vector to range [-1,1]
	vec3 N = normalize(normal * 2.0 - 1.0);  // this normal is in tangent space

	// calculer la direction de la surface vers la lumière (L)
	vec3 L = normalize(lightPosition - interpolationPosition);

	// calculer le niveau de réflexion diffuse (N • L)
	float reflectionDiffuse = max(dot(N, L), 0.0);

	// réflexion spéculaire par défaut
	float reflectionSpecular = 0.0;

	// calculer la réflexion spéculaire seulement s'il y a réflexion diffuse
	if(reflectionDiffuse > 0.0)
	{
		// calculer la direction de la surface vers la caméra (V)
		vec3 V = normalize(-interpolationPosition);

		// calculer la direction de la réflection (R) du rayon incident (-L) en fonction de la normale (N)
		vec3 R = reflect(-L, N);

		// calculer la réflexion spéculaire entre (R dot V)
		reflectionSpecular = pow(max(dot(V, R), 0.0), materialBrightness);
	}
	
	// Attenuation
    float distance = length(lightPosition - interpolationPosition);
    float attenuation = 1.0f / (constant + linear * distance + quadratic * (distance * distance)); 		
	//texture(diffuseMap, texCoordVarying).rgb
	
    vec3 diffuse = lightColorDiffuse * texture(diffuseMap, texCoordVarying).rgb  * reflectionDiffuse ;  
    vec3 specular = lightColorSpecular * materialColorSpecular * reflectionSpecular;  
    diffuse *= attenuation;
    specular *= attenuation;		
	
    return (vec3(diffuse + specular));

}

vec3 CalcDirLight(vec4 lightDirection, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular)
{
	vec3 normal = texture(normalMap, texCoordVarying).rgb;
	// Transform normal vector to range [-1,1]
	vec3 N = normalize(normal * 2.0 - 1.0);  // this normal is in tangent space

	// calculer la direction de la surface vers la lumière (L)
	vec3 lithD = lightDirection.xyz;
	vec3 L = normalize(-lithD); 

	// calculer le niveau de réflexion diffuse (N • L)
	float reflectionDiffuse = max(dot(N, L), 0.0);

	// réflexion spéculaire par défaut
	float reflectionSpecular = 0.0;

	// calculer la réflexion spéculaire seulement s'il y a réflexion diffuse
	if(reflectionDiffuse > 0.0)
	{
		// calculer la direction de la surface vers la caméra (V)
		vec3 V = normalize(-interpolationPosition);

		// calculer la direction de la réflection (R) du rayon incident (-L) en fonction de la normale (N)
		vec3 R = reflect(-L, N);

		// calculer la réflexion spéculaire entre (R dot V)
		reflectionSpecular = pow(max(dot(V, R), 0.0), materialBrightness);
	}	
	
 
   vec3 diffuse = lightColorDiffuse * texture(diffuseMap, texCoordVarying).rgb  * reflectionDiffuse ;  
   vec3 specular = lightColorSpecular * materialColorSpecular * reflectionSpecular;
	
	return (vec3(diffuse + specular));
}

vec3 CalcSpotLight(vec4 lightDirection, vec3 lightPosition, vec3 interpolationPosition, vec3 lightColorDiffuse, vec3 lightColorSpecular)
{

	vec3 diffuse = vec3(0,0,0);
	vec3 specular = vec3(0,0,0);
	
	vec3 normal = texture(normalMap, texCoordVarying).rgb;
	// Transform normal vector to range [-1,1]
	vec3 N = normalize(normal * 2.0 - 1.0);  // this normal is in tangent space

	// calculer la direction de la surface vers la lumière (L)
	vec3 L = normalize(lightPosition - interpolationPosition);

	//Direction du rayon incident de la lumière
	vec3 I = -L;
	
	// direction vers laquelle pointe la lumière
	vec3 lithD = lightDirection.xyz;
	vec3 D = normalize(lithD); 	
	
	float reflectionDiffuse = max(dot(N, L), 0.0);	

	// réflexion spéculaire par défaut
	float reflectionSpecular = 0.0;

	// calculer la réflexion spéculaire seulement s'il y a réflexion diffuse
	if(reflectionDiffuse > 0.0)
	{
		// calculer la direction de la surface vers la caméra (V)
		vec3 V = normalize(-interpolationPosition);

		// calculer la direction de la réflection (R) du rayon incident (-L) en fonction de la normale (N)
		vec3 R = reflect(-L, N);

		// calculer la réflexion spéculaire entre (R dot V)
		reflectionSpecular = pow(max(dot(V, R), 0.0), materialBrightness);
	}	 
	
	// Attenuation
	float distance = length(lightPosition - interpolationPosition);
	float attenuation = 1.0f / (constant + linear * distance + quadratic * (distance * distance)); 
	
	diffuse = lightColorDiffuse * texture(diffuseMap, texCoordVarying).rgb  * reflectionDiffuse ;  
	specular = lightColorSpecular * materialColorSpecular * reflectionSpecular;   
	diffuse *= attenuation;
	specular *= attenuation;	

	//spot light
	//float intensity=pow(max(dot(I,D),0), 0.9);
	
	  // Spotlight (soft edges)
    float theta = dot(I, D); 
    float epsilon = (cutOff - outerCutOff);
    float intensity = clamp((theta - outerCutOff) / epsilon, 0.0, 1.0);
  
    
    diffuse  *= intensity;
    specular *= intensity;		
	
    return (vec3(diffuse + specular));

}