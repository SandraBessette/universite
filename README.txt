Voici le code de deux projets universitaire fait en Hiver 2017.

ProjetIA:
Code en python du projet final d'un cours d'intelligence artificielle suivi � l'Universit� Laval.
Il s'agit d'un r�seau de neurones et d'un arbre de d�cision, cod� sans librairie.
Ce projet est un projet individuel.
On y trouve aussi le rapport final et l'analyse des donn�es dans le document:
SandraBessetteProjetFinal.pdf

ProjetInfographie:
Code d'un mini logiciel de visualisation de mod�les 3d et d'images 2d fait dans le cadre d'un cours d'infographie. Fait en C++ avec OpenFramework.
Ce projet est un projet individuel.
Ce logiciel offre la possibilit� de cr�er des courbes et surfaces param�triques, objets 3d et textures, jeux de lumi�re et de cam�ra,
animation et transformation spatiale, etc. Il y a aussi le code des shaders servant pour diff�rents mod�les d�illuminations et effets sp�ciaux.

dossier executableWindowsOGL3_3:
Pour visualiser et utiliser le programme, double cliquez sur l'ex�cutable IFT3100_TP3_44.exe. Fonctionnel dans Windows avec au minimum OpenGL 3.3.
Quelques instructions de bases pour utiliser le logiciel : pour modifier ou supprimer les objets mis sur la sc�ne, vous devez d�abord les s�lectionner, 
et ceci ce fait en cliquant sur l�objet avec le bouton droit de la souris. 

dossier Image3DSample:
2 mod�les 3d (.obj)et une image qui peuvent servir pour l'essai du logiciel. Sinon vous pouvez utiliser vos propres images et objets 3d. Il suffit de faire un drag and drop 
de l'objet ou l'image dans la fen�tre du logiciel. 

dossier src:
Le code source du logiciel.

dossier shader:
Le code des shaders

DocumentExplicatif: Explication sur les fonctionalit�s, l'utilisation et le code du logiciel.