# Programme fait par Sandra Bessette cours IFT 4102 , Projet Finale 30 avril 2017
"""
	* train 	: pour entrainer le modèle sur l'ensemble d'entrainement
	* predict 	: pour prédire la classe d'un exemple donné
	* test 		: pour tester sur l'ensemble de test
"""

import numpy as np

# NeuralNet pour le modèle Réseaux de Neurones
# DecisionTree le modèle des arbres de decision

class DecisionTree:

    def __init__(self, **kwargs):
        """
        tree : variable utilisé pour mémorisé l'arbre de décision
        continuous_split : donne les valeurs ou faire la séparation de catégorie par attribut pour les données continues
        (n'a finalement pas été utilisé, voir document point 2.1.3 pour explication)
        """
        self.tree = {}
        self.continuous_split = []

    def train(self, train, train_labels, continuous=False): #vous pouvez rajouter d'autres attribus au besoin
        """
        c'est la méthode qui va entrainer le modèle,
        train est une matrice de taille nxm, avec
        n : le nombre d'exemple d'entrainement dans le dataset
        m : le mobre d'attribus (le nombre de caractéristiques)

        train_labels : est une matrice de taille nx1
        continuous, doit être mis à vrai pour les données continues (n'a finalement pas été utilisé,
        voir document point 2.1.3 pour explication)



        ----------
        --

        """

        # Créer l'arbre de décision
        train1 = np.copy(train)
        train_labels1 = np.copy(train_labels)
        self.continuous_split = []
        self.tree = {}
        if continuous:
            self.split_continious_data(train1, train_labels1)
            train1 = self.continious_data(train1)

        attributs = np.arange(len(train1[0]))
        self.tree = self.decision_tree(train1, train_labels1, attributs)

    def predict(self, exemple, label, tree):
        """
        Fonction récursive qui sert à prédire la classe d'un exemple donné en entrée
        exemple est de taille 1xm

        si la valeur retournée est la meme que la veleur dans label
        alors l'exemple est bien classifié, si non c'est une missclassification

        tree: contient l'arbre de décision, nécessaire pour la recherche récursive dans l'arbre

        """
        if isinstance(tree, int):
            return tree, tree == label

        # Traverse the tree further until a leaf node is found.

        else:
            key_lst = []

            for key in tree:
                key_lst.append(key)

            attr = key_lst[0]
            new_key = exemple[attr]
            if new_key not in tree[attr]:
                sub_tree = tree[key_lst[1]]
            else:
                sub_tree = tree[attr][new_key]
            return self.predict(exemple, label, sub_tree)

    def test(self, test, test_labels, nb_class, imprimer=True, continuous=False):
        """
        c'est la méthode qui va tester le modèle sur les données de test
        l'argument test est une matrice de taille nxm, avec
        n : le nombre d'exemple de test dans le dataset
        m : le mobre d'attribus (le nombre de caractéristiques)

        test_labels : est une matrice taille nx1
        imprimer: imprimer ou non les résultats (matrice de confusion et accuracy)
        continuous, doit être mis à vrai pour les données continues (n'a finalement pas été utilisé,
        voir document point 2.1.3 pour explication)

        Retourne l'accuracy

        Fait le test sur les données de test, et affiche :
        - la matrice de confusion (confusion matrix)
        - l'accuracy (ou le taux d'erreur)

        """

        test1 = np.copy(test)
        test_labels1 = np.copy(test_labels)
        if continuous:
            test1 = self.continious_data(test1)
        index = 0
        result_list = []
        for data in test1:
            label_predicted, is_well_classified = self.predict(data, test_labels1[index], self.tree)
            result_list.append((label_predicted, is_well_classified, test_labels1[index]))
            index += 1

        nb_classe0_in_classe0 = 0
        nb_classe0_in_classe1 = 0
        nb_classe0_in_classe2 = 0

        nb_classe1_in_classe0 = 0
        nb_classe1_in_classe1 = 0
        nb_classe1_in_classe2 = 0

        nb_classe2_in_classe0 = 0
        nb_classe2_in_classe1 = 0
        nb_classe2_in_classe2 = 0

        nb_true_result = 0

        for result in result_list:
            if result[1]:
                nb_true_result += 1
            if result[0] == 0 and result[2] == 0:
                nb_classe0_in_classe0 += 1
            if result[0] == 0 and result[2] == 1:
                nb_classe0_in_classe1 += 1

            if result[0] == 1 and result[2] == 0:
                nb_classe1_in_classe0 += 1
            if result[0] == 1 and result[2] == 1:
                nb_classe1_in_classe1 += 1

            if nb_class == 3:
                if result[0] == 0 and result[2] == 2:
                    nb_classe0_in_classe2 += 1
                if result[0] == 1 and result[2] == 2:
                    nb_classe1_in_classe2 += 1
                if result[0] == 2 and result[2] == 0:
                    nb_classe2_in_classe0 += 1
                if result[0] == 2 and result[2] == 1:
                    nb_classe2_in_classe1 += 1
                if result[0] == 2 and result[2] == 2:
                    nb_classe2_in_classe2 += 1

        if nb_class == 3:

            confusion = np.array([[nb_classe0_in_classe0, nb_classe0_in_classe1, nb_classe0_in_classe2],
                                  [nb_classe1_in_classe0, nb_classe1_in_classe1, nb_classe1_in_classe2],
                                  [nb_classe2_in_classe0, nb_classe2_in_classe1, nb_classe2_in_classe2]])

        else:
            confusion = np.array([[nb_classe0_in_classe0, nb_classe0_in_classe1],
                                  [nb_classe1_in_classe0, nb_classe1_in_classe1]])

        accuracy = (nb_true_result / len(test)) * 100

        if imprimer:
            print("Matrice de confusion :")
            print(confusion)
            print("Accuracy en %:")
            print(accuracy)
        return accuracy


    def decision_tree(self, data, data_labels, attributs):
        """
        Fonction récursive qui sert à créer l'arbre de décision
        retourne l'arbre de décision

        data: contient les exemple d'entrainement qui reste pour créer l'arbre et sous branche
        data_label: contient les datalabel associé aux exemple d'entrainement restant
        attributs: contient les attribut des exemple (attribut de classement) qui reste pour choisir les prochains
        noeud de l'arbre

        """

        data1 = data
        data_label1 = data_labels
        # Trouver une valeur de classe par défault: la valeur de la classes la plus nombreuse dans la liste
        default = int(np.bincount(data_label1).argmax())

        # Vérifier si la liste d'attributs est vide
        if len(attributs) == 0:
            return int(default)

        # Vérifier si les datas sont vides
        if len(data1) == 0:
            return 0

        # Vérifier si toutes les données dans data ont la même classe
        count = self.counter(data_label1)

        if len(list(count.keys())) == 1:
            return int(data_label1[0])

        # Choisir l'attribut avec le gain le plus élevé pour faire un nouveau noeurd de l'Arbre
        root = self.gain_maximum(data1, data_label1, attributs)

        # Créer un nouveau noeud dans l'arbre
        my_tree = {root: {}, 'Default': default}

        # Créer les autres branche de l'arbre
        values = self.get_value(data1, root)

        for val in values.tolist():
            new_data, new_data_label = self.get_new_data(data1, data_label1, root, val)
            new_attribute = []
            for attri in attributs:
                if attri != root:
                    new_attribute.append(attri)

            new_tree = self.decision_tree(new_data, new_data_label, np.array(new_attribute))
            my_tree[root][val] = new_tree
        return my_tree


    def get_new_data(self, data, data_labels, root, val):
        """
        Fonction qui sert à trié seulemetn les exemple d'entrainement qui ont une valeur (val) pour l'attribut root
        retourne la liste des exemple qui ont la valeur val pour l'attribut val et la liste des label correspondant à
        ces exemple

        data: contient les exemple d'entrainement à trié
        data_label: contient les datalabel associé aux exemple d'entrainement data
        root: l'attribut sur lequel ont trie (noeud de l'arbre)
        val : la valeur de l'attribut root avec laquel ont trie les exemples (ont veut ceux qui ont cette valeur)

        """
        new_data_list = []
        new_label_lst = []
        index = 0
        for exemple in data:
            if exemple[root] == val:
                new_data_list.append(exemple)
                new_label_lst.append(data_labels[index])
            index += 1
        return np.array(new_data_list), np.array(new_label_lst)

    def gain_maximum(self, data, data_labels, attributs):
        """
        Fonction détermine le gain maximum entre les différent attribut restant
        retourne l'index de l'attribut qui a le gain maximum

        data: contient les exemple d'entrainement
        data_label: contient les datalabel associé aux exemple d'entrainement data
        attributs: une liste d'attributs. On cherche l'attribut dans cette liste qui donne le gain maximum

        """
        s = self.entropy(data_labels)

        attr_gain_max = attributs[0]
        gain_max = 0

        for attr in attributs:
            gain = s - self.gain(data, data_labels, attr)
            if gain > gain_max:
                gain_max = gain
                attr_gain_max = attr

        return attr_gain_max

    def gain(self, data, data_labels, attr):
        """
        Fonction calcule le gain pour un attribut (du moins une partie du gain, entropie du noeud racine va lui être
        soustrait dans la fonction gain_maximum)
        retourne le gain

        data: contient les exemple d'entrainement
        data_label: contient les datalabel associé aux exemple d'entrainement data
        attr: l'attribut pour lequel on veut le gain

        """
        value_lst = []
        gain = 0
        for exemple in data:
            value_lst.append(exemple[attr])
        dict_frequency = self.counter(value_lst)

        nb_data = len(data_labels)

        for key in dict_frequency:
            sub_data_label = []
            index = 0
            for exemple in data:
                if exemple[attr] == key:
                    sub_data_label.append(data_labels[index])
                index += 1
            gain += dict_frequency[key]/nb_data * self.entropy(sub_data_label)

        return gain

    def entropy(self, label_value):
        """
        Fonction calcule l'entropie d'une valeur (catégories) pour un attribut
        retourne l'entropie

        label_value: contient une liste de label (classes) des exemple d'entrainement
        pour une valeur donnée d'un attribut donné

        """
        nb_item = len(label_value)
        count = self.counter(label_value)
        results = 0
        for key in count:
            results -= count[key]/nb_item * np.log2(count[key]/nb_item)
        return results

    def get_value(self, data, root):
        """
        Fonction qui donne toutes les valeurs unique (catégories) trouvées dans les exemples d'entrainement pour un
        attribut donnée
        retourne une liste de catégorie

        data: liste des exemples d'entrainement
        root: L'attribut pour lequel ont veut connaitre les catégorie (valeur)

        """
        value_lst = []
        for exemple in data:
            value_lst.append(exemple[root])
        return np.unique(value_lst)

    def counter(self, mylist):
        counter = {}
        for item in mylist:
            if item not in counter:
                counter[item] = 1
            else:
                counter[item] += 1
        return counter

    def split_continious_data(self, data, data_label):
        """
        Fonction qui sert à trouver les valeurs qui peuvent séparer les données de chaque attribut en 2 catégories
        (n'a finalement pas été utilisé, voir document point 2.1.3 pour explication)
        mets à jour self.continuous_split

        data: liste des exemples d'entrainement
        data_label: Liste des classes associé aux données

        """
        nb_attribute = len(data[0])
        column_lst = []
        label_lst = []
        data_label1 = data_label
        data1 = data
        for i in range(0, nb_attribute):
            one_colon = data1[:, i]
            inds = one_colon.argsort()
            one_colon_sorted = one_colon[inds]
            data_label_sorted = data_label1[inds]
            column_lst.append(one_colon_sorted)
            label_lst.append(data_label_sorted)

        indice_difference_lst = []
        for classes in label_lst:
            nb = len(classes)
            indice_lst = []
            for j in range(1, nb):
                if(classes[j-1]) != (classes[j]):
                    indice_lst.append(j-1)
            indice_difference_lst.append(indice_lst)

        gain_max_lst = []
        nb = len(label_lst)
        for c in range(0, nb):
            s = self.entropy(label_lst[c])
            freq = len(label_lst[c])
            gain_max = 0
            best_indice = indice_difference_lst[c][0]
            for i in indice_difference_lst[c]:
                gain = (i+1/freq) * self.entropy(label_lst[c][0:i+1]) + (freq - (i+1)/freq) * self.entropy(label_lst[c][i+1:freq])
                gain = s - gain
                if gain > gain_max:
                    gain_max = gain
                    best_indice = i

            gain_max_lst.append(best_indice)
        nb = len(column_lst)
        split_number = []
        for i in range(0, nb):
            indice = gain_max_lst[i]
            split = (column_lst[i][indice] + column_lst[i][indice + 1])/2
            split_number.append(split)
        self.continuous_split = split_number
        return

    def continious_data(self, data):
        """
        Fonction qui sert remplacer les données continue en données ayant deux catégorie par attribut. Se sert de
        self.continuous_split déja calculer dans la fonction split_continious_data pour savoir comment
         séparer les données
        (n'a finalement pas été utilisé, voir document point 2.1.3 pour explication)

        data: liste des exemples d'entrainement
        retourne les données (data) ou chque attribut a deux catégories au lieu de données continues

        """

        nb_attribute = len(data[0])

        for exemple in data:
            for i in range(0, nb_attribute):
                if exemple[i] >= self.continuous_split[i]:
                    exemple[i] = int(1)
                else:
                    exemple[i] = int(0)
        return data.astype(int)

    def learning_curve(self, train2, train_labels2, nb_class):
        """
        Fonction qui calcule les données pour créer une courbe d'apprentissage. Elle utilise 100 exemples d'entrainement
        et "split" ces exemples 20 fois à chaque fois de facon aléatoire, en prenant l'accuracy moyenne.
        En effet, cette fonction sépare les exemples en exemple d'entrainement et de test.
        Elle commence par 1 exemple d'entrainement jusqu'à 100.
        Retourne une liste de de dimension des exemple d'entrainement, une liste des accuracy pour chaque dimension.

        train2: liste des exemples
        train2_label: liste des classement de ces exemples
        nb_class: nombre de classes

        """

        proportion = [0.5]
        trainning_size = [0]
        train = train2[:100]
        train_labels = train_labels2[:100]
        nb_item = len(train_labels)
        train1 = 0
        np.random.seed(1)
        for i in range(1, nb_item):
            proportion_int = 0
            for j in range(0, 20):
                # melanger les données pour faire 20 split différents

                rng_state = np.random.get_state()
                np.random.shuffle(train)
                np.random.set_state(rng_state)
                np.random.shuffle(train_labels)

                train1 = train[:i]
                train_labels1 = train_labels[:i]
                test1 = train[i:]
                test_labels1 = train_labels[i:]

                self.train(train1, train_labels1)
                accuracy = self.test(test1, test_labels1, nb_class, False, False)
                proportion_int += accuracy/100
            proportion.append(proportion_int/20)
            trainning_size.append(len(train1))
        return trainning_size, proportion














