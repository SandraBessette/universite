Explication du main.py

En premier on voit les r�sultats de l'Arbre de d�cision (r�sultats des test, temps apprentissage mod�le, temps pr�diction 1 exemple)
Apr�s ceux du r�seau de neurone(r�sultats des test, temps apprentissage mod�le, temps pr�diction 1 exemple)


Pour l'arbre de d�cision:
Les fonctions qui permettent de cr�er les courbes d'apprentissage sont � la fin du fichier main.py et sont mises en commentaire.

Pour le neural networks:
J'ai mis toutes les fonctions qui s'occupent de faire la validation crois�e pour le nombre de noeud par couche cach�, pour le nombre de couche cach� et pour les courbes d'apprentissage des diff�rente couches cach�es en commentaire dans le code. Les fonctions qui s'occupent de cr�er les graphiques aussi. J'ai fait cela car ces fonctions sont quand m�me assez lente � ex�cuter (par exemple la validation crois�, pour chaque k passage j'ai mis 10 �poques, car j'ai trouv� que �a me donnait de meilleurs r�sultats en g�n�ral, mais c'est plus lent). 

Le code qui permette de comparer l'ex�cution du r�seau de neurone avec les initialisation � 0 et � non 0 et les graphiques associ�s est � la fin du main.py et est aussi en commentaire. 

Donc, pour le r�seau de neurone et pour l'arbre de d�cision j'ai laiss� en fonction les fonctions qui permettent de faire le trainning de chaque dataset (j'ai initialis� le nombre de noeud et de couche cach� selon les r�sultats donn�s par la validation crois�). J'ai aussi laiss� les fonctions qui permettent de faire les tests de chaque dataset , et � la fin on a les r�sultats pour le temps d'ex�cution des training et aussi des fonctions qui permettent de calculer le temps d'ex�cution de pr�diction pour un exemple pour chaque dataset. Donc si on "run" le main.py sans enlever les commentaires c'est ce qui va fonctionner. 


Si vous voulez tester la valisation crois� ou autre chose, il s'agit simplement d'enlever les lignes appropri�s en comentaire dans le code. 

Dans tous les cas, pour voir apparaitre les r�sultats il faut quand m�me attendre plusieurs minutes car le r�seau de neurone est assez lent � rouler pour les tests � cause du nombre d'�poques de certaines datasets qui est �lev�. 
