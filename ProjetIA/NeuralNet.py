# Programme fait par Sandra Bessette cours IFT 4102 , Projet Finale 30 avril 2017
"""
	* train 	: pour entrainer le modèle sur l'ensemble d'entrainement
	* predict 	: pour prédire la classe d'un exemple donné
	* test 		: pour tester sur l'ensemble de test
"""

import numpy as np

# NeuralNet pour le modèle Réseaux de Neurones
# DecisionTree le modèle des arbres de decision


class NeuralNet: # nom de la class à changer

    def __init__(self, number_of_output):
        """
        number_of_neurons : le nombre de neurone par couches cachés
        number_of_layers : le nombre de couches cachés
        number_of_output : le nombre de neurone en sortie
        weights : les poids du réseaux
        """
        self.number_of_neurons = 4
        self.number_of_layers = 3
        self.number_of_output = number_of_output
        self.weights = []

    def train(self, train, train_labels, nb_of_epoque, number_of_neurons, number_of_layers, non_zero=True):
        """
        c'est la méthode qui va entrainer votre modèle,
        train est une matrice de taille nxm, avec
        n : le nombre d'exemple d'entrainement dans le dataset
        m : le mobre d'attribus (le nombre de caractéristiques)

        train_labels : est une matrice de taille nx1

        nb_of_epoque : le nombre d'époque nécessaire pour faire les entrainement sans réinitilisation
        number_of_neurons : le nombre de neurone par couches cachés
        number_of_layers : le nombre de couches cachés
        non_zero : donne le type d'initilisation des poids. Par défault cela est à vrai et l'initilisation ce fait
        au hasrd. Si on le met à faut les poids seront initiliser à 0. Ceci sert à répondre aux questions du travail
        ------------

        """
        np.random.seed(1)  # Pour avoir les meme nombres aléatoires à chaque initialisation.
        # Initialiser les poids
        self.number_of_layers = number_of_layers
        self.number_of_neurons = number_of_neurons

        nb_of_initial_node = len(train[0])
        self.weights = []
        if non_zero:
            self.initialize_weight(nb_of_initial_node, self.number_of_output)
        else:
            self.initialize_weight_zero(nb_of_initial_node, self.number_of_output)
        for epochs in range(0, nb_of_epoque):
            nb_example = len(train)
            error = []
            for exemple in range(0, nb_example):
                train_label1 = train_labels[exemple]
                train1 = train[exemple]
                np.array([train1])
                np.array([train_label1])
                output = self.predict(train1, train_label1)
                nb_output = len(output)

                # calculer l'erreur des neurones de sortie
                error = []
                if self.number_of_output == 2:
                    if train_label1 == 0:
                        train_label1 = [1, 0]
                    else:
                        train_label1 = [0, 1]
                elif self.number_of_output == 3:
                    if train_label1 == 0:
                        train_label1 = [1, 0, 0]
                    elif train_label1 == 1:
                        train_label1 = [0, 1, 0]
                    else:
                        train_label1 = [0, 0, 1]

                t = train_label1 - output[nb_output-1]
                error.append(output[nb_output-1] * (1 - output[nb_output-1]) * t)

                # calculer l'erreur des autres neurones
                i = 0
                for layer in range(nb_output-1, 0, -1):
                    error.append(output[layer-1] * (1 - output[layer-1]) * error[i].dot(self.weights[layer].T))
                    i += 1

                # calculer le delta_weight
                delta = []

                error1 = np.array([error[nb_output-1]])
                delta.append(0.3 * np.transpose(np.array([train1])).dot(error1))
                # delta.append(np.transpose(np.array([train1])).dot(error[nb_output-1]))
                i = nb_output-2
                for layer in range(0, nb_output-1):
                    error1 = np.array([error[i]])
                    output1 = np.array([output[layer]])
                    delta.append(0.3 * output1.T.dot(error1))
                    i -= 1

                # mise à jour des poids
                for layer in range(0, nb_output):
                    self.weights[layer] += delta[layer]

            # mélanger les données entre chaque epoque
            np.random.seed(1)
            rng_state = np.random.get_state()
            np.random.shuffle(train)
            np.random.set_state(rng_state)
            np.random.shuffle(train_labels)

    def predict(self, exemple, label):
        """
        Prédire la classe d'un exemple donné en entrée
        exemple est de taille 1xm

        si la valeur retournée est la meme que la veleur dans label
        alors l'exemple est bien classifié, si non c'est une missclassification

        """
        output = []
        output.append(self.sigmoid(np.dot(exemple, self.weights[0])))

        for layer in range(1, self.number_of_layers-1):
            output.append(self.sigmoid(np.dot(output[layer-1], self.weights[layer])))

        return output

    def test(self, test, test_labels, imprimer=True):
        """
        c'est la méthode qui va tester votre modèle sur les données de test
        l'argument test est une matrice de taille nxm, avec
        n : le nombre d'exemple de test dans le dataset
        m : le mobre d'attribus (le nombre de caractéristiques)

        test_labels : est une matrice taille nx1
        imprimer: imprimer ou non les résultats (matrice de confusion et accuracy)

        Retourne l'accuracy

        Fait le test sur les données de test, et affiche:
        - la matrice de confision (confusion matrix)
        - l'accuracy (ou le taux d'erreur)

        """

        output = self.predict(test, test_labels)
        nb_output = len(output)

        result = []

        for out in output[nb_output - 1]:
            if self.number_of_output == 1:
                if out > 0.50:
                    result.append(1)
                else:
                    result.append(0)
            else:
                result.append(np.where(out == max(out)))

        t = abs(np.transpose(np.array([test_labels])) - np.array([result]).T)
        sum2 = np.count_nonzero(t)
        long = len(result)
        accuracy = ((long-sum2)/long)*100

        nb_classe0_in_classe0 = 0
        nb_classe0_in_classe1 = 0
        nb_classe0_in_classe2 = 0

        nb_classe1_in_classe0 = 0
        nb_classe1_in_classe1 = 0
        nb_classe1_in_classe2 = 0

        nb_classe2_in_classe0 = 0
        nb_classe2_in_classe1 = 0
        nb_classe2_in_classe2 = 0

        for i in range(0, long):
            if self.number_of_output != 1:
                if result[i][0][0] == 0 and test_labels[i] == 0:
                    nb_classe0_in_classe0 += 1
                if result[i][0][0] == 0 and test_labels[i] == 1:
                    nb_classe0_in_classe1 += 1

                if result[i][0][0] == 1 and test_labels[i] == 0:
                    nb_classe1_in_classe0 += 1
                if result[i][0][0] == 1 and test_labels[i] == 1:
                    nb_classe1_in_classe1 += 1

                if self.number_of_output == 3:
                    if result[i][0][0] == 0 and test_labels[i] == 2:
                        nb_classe0_in_classe2 += 1
                    if result[i][0][0] == 1 and test_labels[i] == 2:
                        nb_classe1_in_classe2 += 1
                    if result[i][0][0] == 2 and test_labels[i] == 0:
                        nb_classe2_in_classe0 += 1
                    if result[i][0][0] == 2 and test_labels[i] == 1:
                        nb_classe2_in_classe1 += 1
                    if result[i][0][0] == 2 and test_labels[i] == 2:
                        nb_classe2_in_classe2 += 1
            else:
                if result[i] == 0 and test_labels[i] == 0:
                    nb_classe0_in_classe0 += 1
                if result[i] == 0 and test_labels[i] == 1:
                    nb_classe0_in_classe1 += 1

                if result[i] == 1 and test_labels[i] == 0:
                    nb_classe1_in_classe0 += 1
                if result[i] == 1 and test_labels[i] == 1:
                    nb_classe1_in_classe1 += 1

        if self.number_of_output == 3:
            confusion = np.array([[nb_classe0_in_classe0, nb_classe0_in_classe1, nb_classe0_in_classe2],
                                  [nb_classe1_in_classe0, nb_classe1_in_classe1, nb_classe1_in_classe2],
                                  [nb_classe2_in_classe0, nb_classe2_in_classe1, nb_classe2_in_classe2]])
        else:
            confusion = np.array([[nb_classe0_in_classe0, nb_classe0_in_classe1],
                                  [nb_classe1_in_classe0, nb_classe1_in_classe1]])
        if imprimer:
            print("Matrice de confusion :")
            print(confusion)
            print("Accuracy en %:")
            print(accuracy)
        return accuracy

    def initialize_weight(self, nb_of_initial_node, nb_final_neurone):
        """
        Initialise les poids du réseau de neurone au hasard avec valeur entre -1 et 1 et moyenne de 0
        nb_of_initial_node : nombre d'attribut dans le dataset. Nous donne le nombre de noeud
        initial du réseau de neurone.
        nb_final_neurone : nombre de neurone en output

        """
        # Initialiser la première couche

        self.weights.append(2 * np.random.random((nb_of_initial_node, self.number_of_neurons)) - 1)
        # Initialiser les autres couches
        for i in range(2, self.number_of_layers-1):
            self.weights.append(2 * np.random.random((self.number_of_neurons, self.number_of_neurons)) - 1)

        # Initialiser la couche finale
        self.weights.append(2 * np.random.random((self.number_of_neurons, nb_final_neurone)) - 1)

    def initialize_weight_zero(self, nb_of_initial_node, nb_final_neurone):
        """
        Initialise les poids du réseau de neurone à 0
        nb_of_initial_node : nombre d'attribut dans le dataset. Nous donne le nombre de noeud
                    initial du réseau de neurone.
        nb_final_neurone : nombre de neurone en output

        On suppose que le réseau à a trois couches minimums

        """
        # Initialiser la première couche
        self.weights.append(0 * np.random.random((nb_of_initial_node, self.number_of_neurons)))
        # Initialiser les autres couches
        for i in range(2, self.number_of_layers - 1):
            self.weights.append(0 * np.random.random((self.number_of_neurons, self.number_of_neurons)))

        # Initialiser la couche finale
        self.weights.append(0 * np.random.random((self.number_of_neurons, nb_final_neurone)))

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def cross_v_number_neuron(self, k, train, train_labels, nb_epocs):
        """
         Fait la validation croisé pour trouver le nombre de neurone par couche caché
         k : données sont diviser en k partie
         train : les donnée d'entrainement
         train_labels : les classes des données d'entrainement
         nb_epocs : le nombre d'époque pour chaque k passage, je l'ai mis à 10 en général, ça me donnait de meilleur
         résultats

         """
        train_labels_split = np.array_split(train_labels, k)
        train_split = np.array_split(train, k)
        nb_sub_array = len(train_labels_split)
        mean_error = []
        for nb_neuron in range(4, 51):
            error = []
            for i in range(0, nb_sub_array):

                nb = len(train_split[i])
                array_train = np.delete(train, np.s_[i:nb], 0)
                array_validate = np.delete(train_labels, np.s_[i:nb], 0)

                self.train(array_train, array_validate, nb_epocs, nb_neuron, 3)

                output = self.predict(train_split[i], train_labels_split[i])

                nb_output = len(output)
                result = []
                validate_labels_copy = train_labels_split[i]
                for out in output[nb_output - 1]:
                    if self.number_of_output == 1:
                        if out > 0.50:
                            result.append(1)
                        else:
                            result.append(0)
                    else:
                        result.append(np.where(out == max(out)))

                t = abs(np.transpose(np.array([validate_labels_copy])) - np.array([result]).T)
                sum1 = np.count_nonzero(t)
                long = len(result)
                error.append(sum1/long)

            sum1 = np.sum(error)
            long = len(error)
            mean_error.append(sum1/long)

        minimum = mean_error.index(min(mean_error))
        nbr_neurone = np.arange(4, 51)
        print("meilleurs nombres de neurone par couche caché:")
        print(minimum + 4)
        return (minimum + 4), nbr_neurone, mean_error

    def cross_v_number_layer(self, k, train, train_labels, nbr_neuron):
        """
        Fait la validation croisé pour trouver le nombre de couche caché
         k : données sont diviser en k partie
         train : les donnée d'entrainement
         train_labels : les classes des données d'entrainement
         nbr_neuron : le nombre de neurones par couche caché.

        """

        train_labels_split = np.array_split(train_labels, k)
        train_split = np.array_split(train, k)
        nb_sub_array = len(train_labels_split)
        # fix parameter
        mean_error = []

        for nb_layers in range(3, 8):
            error = []
            for i in range(0, nb_sub_array):
                nb = len(train_split[i])
                array_train = np.delete(train, np.s_[i:nb], 0)
                array_validate = np.delete(train_labels, np.s_[i:nb], 0)

                self.train(array_train, array_validate, 10, nbr_neuron, nb_layers)

                output = self.predict(train_split[i], train_labels_split[i])

                nb_output = len(output)
                result = []
                validate_labels_copy = train_labels_split[i]
                for out in output[nb_output - 1]:
                    if self.number_of_output == 1:
                        if out > 0.50:
                            result.append(1)
                        else:
                            result.append(0)
                    else:
                        result.append(np.where(out == max(out)))

                t = abs(np.transpose(np.array([validate_labels_copy])) - np.array([result]).T)
                sum3 = np.count_nonzero(t)
                long = len(result)
                error.append(sum3/long)

            sum3 = np.sum(error)
            long = len(error)
            mean_error.append(sum3/long)

        minimum = mean_error.index(min(mean_error))
        nbr_layers = np.arange(3, 8)
        print("meilleurs nombres de couches:")
        print((minimum + 3))
        return (minimum + 3), nbr_layers, mean_error

    def cross_v_number_epoch(self, k, train, train_labels):
        """
        Fait la validation croisé pour trouver le nombre d'époque optimal
         k : données sont diviser en k partie
         train : les donnée d'entrainement
         train_labels : les classes des données d'entrainement

       """
        train_labels_split = np.array_split(train_labels, k)
        train_split = np.array_split(train, k)
        nb_sub_array = len(train_labels_split)
        # fix parameter
        mean_error = []

        for nb_epoch in range(1, 500):
            error = []
            for i in range(0, nb_sub_array):
                nb = len(train_split[i])
                array_train = np.delete(train, np.s_[i:nb], 0)
                array_validate = np.delete(train_labels, np.s_[i:nb], 0)

                self.train(array_train, array_validate, nb_epoch, 4, 3)

                output = self.predict(train_split[i], train_labels_split[i])

                nb_output = len(output)
                result = []
                validate_labels_copy = train_labels_split[i]
                for out in output[nb_output - 1]:
                    if self.number_of_output == 1:
                        if out > 0.50:
                            result.append(1)
                        else:
                            result.append(0)
                    else:
                        result.append(np.where(out == max(out)))

                t = abs(np.transpose(np.array([validate_labels_copy])) - np.array([result]).T)
                sum4 = np.count_nonzero(t)
                long = len(result)
                error.append(sum4/long)

            sum4 = np.sum(error)
            long = len(error)
            mean_error.append(sum4/long)

        minimum = mean_error.index(min(mean_error))
        print(minimum)
        return minimum

    def learning_curve(self, train2, train_labels2, nb_epoc, best_nb_neuron, best_nb_layer):
        """
        Fonction qui calcule les données pour créer une courbe d'apprentissage, une pour une initialisation aléatoire
        des poids (valeur entre -1 et 1 et moyenne de 0) et l'autre pour une initilisation des poids à 0.
        Elle utilise 120 exemples d'entrainement et "split" ces exemples 20 fois à chaque fois de facon aléatoire,
        en prenant l'accuracy moyenne.
        En effet, cette fonction sépare les exemples en exemple d'entrainement et de test.
        Elle commence par 1 exemple d'entrainement jusqu'à 120.

        Retourne une liste de dimension des exemple d'entrainement, une liste des accuracy pour chaque dimension avec
        l'initilisation des poid de facon aléatoire et une liste des accuracy pour chaque dimension avec
        l'initilisation des poid à0.

        train2: liste des exemples
        train2_label: liste des classement de ces exemples
        nb_epoc: nombre d'époque
        best_nb_neuron : meilleur nombre de neurone par couches cachées
        best_nb_layer : meilleur nombre de couche cachée
        """
        proportion_zero = [0.5]
        proportion = [0.5]
        trainning_size = [0]
        train = train2[:120]
        train_labels = train_labels2[:120]
        nb_item = len(train_labels)
        train1 = 0
        for i in range(1, nb_item):
            proportion_int = 0
            proportion_zero_int = 0
            for j in range(0, 20):
                # melanger les données pour faire 20 split différents
                np.random.seed(1)
                rng_state = np.random.get_state()
                np.random.shuffle(train)
                np.random.set_state(rng_state)
                np.random.shuffle(train_labels)

                train1 = train[:i]
                train_labels1 = train_labels[:i]
                test1 = train[i:]
                test_labels1 = train_labels[i:]

                self.train(train1, train_labels1, nb_epoc, best_nb_neuron, best_nb_layer)
                accuracy = self.test(test1, test_labels1, False)
                proportion_int += accuracy/100
                self.train(train1, train_labels1, nb_epoc, best_nb_neuron, best_nb_layer, False)
                accuracy2 = self.test(test1, test_labels1, False)
                proportion_zero_int += accuracy2/100

            proportion.append(proportion_int/20)
            proportion_zero.append(proportion_zero_int/20)
            trainning_size.append(len(train1))
        return trainning_size, proportion, proportion_zero

    def learning_curve_nbLayer(self, train2, train_labels2, nb_epoc, best_nb_neuron, best_nb_layer):
        """
        Fonction qui calcule les données pour créer une courbe d'apprentissage. Sert à voir la différence
        d'apprentissage entre un réseau ayant différent nombre de couche cachée.
        Elle utilise 120 exemples d'entrainement et "split" ces exemples 20 fois à chaque fois de facon aléatoire,
        en prenant l'accuracy moyenne.
        En effet, cette fonction sépare les exemples en exemple d'entrainement et de test.
        Elle commence par 1 exemple d'entrainement jusqu'à 120.

        Retourne une liste de dimension des exemple d'entrainement et une liste des accuracy pour chaque dimension.

        train2: liste des exemples
        train2_label: liste des classement de ces exemples
        nb_epoc: nombre d'époque
        best_nb_neuron : meilleur nombre de neurone par couches cachées
        best_nb_layer : nombre de couche cachée à tester
        """

        proportion = [0.5]
        trainning_size = [0]
        train = train2[:120]
        train_labels = train_labels2[:120]
        nb_item = len(train_labels)
        train1 = 0
        np.random.seed(1)
        for i in range(1, nb_item):
            proportion_int = 0
            for j in range(0, 20):
                # melanger les données pour faire 20 split différents

                rng_state = np.random.get_state()
                np.random.shuffle(train)
                np.random.set_state(rng_state)
                np.random.shuffle(train_labels)

                train1 = train[:i]
                train_labels1 = train_labels[:i]
                test1 = train[i:]
                test_labels1 = train_labels[i:]

                self.train(train1, train_labels1, nb_epoc, best_nb_neuron, best_nb_layer)
                accuracy = self.test(test1, test_labels1, False)
                proportion_int += accuracy/100

            proportion.append(proportion_int/20)
            trainning_size.append(len(train1))
        return trainning_size, proportion

