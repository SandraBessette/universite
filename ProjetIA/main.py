# Programme fait par Sandra Bessette cours IFT 4102 , Projet Finale 30 avril 2017

import numpy as np
import matplotlib.pyplot as plt
import sys
import load_datasets
import NeuralNet  # importer la classe du Réseau de Neurones
import DecisionTree  # importer la classe de l'Arbre de Décision
import time

# Initializer/instanciez les classifieurs avec leurs paramètres
neuralNet_iris = NeuralNet.NeuralNet(3)
neuralNet_cong = NeuralNet.NeuralNet(2)
neuralNet_monk1 = NeuralNet.NeuralNet(1)
neuralNet_monk2 = NeuralNet.NeuralNet(2)
neuralNet_monk3 = NeuralNet.NeuralNet(2)

decisionTree_iris = DecisionTree.DecisionTree()
decisionTree_cong = DecisionTree.DecisionTree()
decisionTree_monk1 = DecisionTree.DecisionTree()
decisionTree_monk2 = DecisionTree.DecisionTree()
decisionTree_monk3 = DecisionTree.DecisionTree()

# Charger/lire les datasets
(train_iris, train_labels_iris, test_iris, test_labels_iris) = load_datasets.load_iris_dataset(0.7)
(train_cong, train_labels_cong, test_cong, test_labels_cong) = load_datasets.load_congressional_dataset(0.7)
(train_monk1, train_labels_monk1, test_monk1, test_labels_monk1) = load_datasets.load_monks_dataset(1)
(train_monk2, train_labels_monk2, test_monk2, test_labels_monk2) = load_datasets.load_monks_dataset(2)
(train_monk3, train_labels_monk3, test_monk3, test_labels_monk3) = load_datasets.load_monks_dataset(3)

# Entrainez le classifieur
# Entrainer l'arbre de décision

vitesse1 = time.clock()
decisionTree_iris.train(train_iris, train_labels_iris)
vitesse2 = time.clock()
time_ADIris = vitesse2 - vitesse1

vitesse1 = time.clock()
decisionTree_cong.train(train_cong, train_labels_cong)
vitesse2 = time.clock()
time_ADCong = vitesse2 - vitesse1

vitesse1 = time.clock()
decisionTree_monk1.train(train_monk1, train_labels_monk1)
vitesse2 = time.clock()
time_ADMonk1 = vitesse2 - vitesse1

vitesse1 = time.clock()
decisionTree_monk2.train(train_monk2, train_labels_monk2)
vitesse2 = time.clock()
time_ADMonk2 = vitesse2 - vitesse1

vitesse1 = time.clock()
decisionTree_monk3.train(train_monk3, train_labels_monk3)
vitesse2 = time.clock()
time_ADMonk3 = vitesse2 - vitesse1


# Entrainer le réseau de neurone

# dataset iris
best_nb_neuron = 37
best_nb_layer = 4

# validation croisé nombre de neurones par couche caché
# best_nb_neuron, neuroneX, errorY = neuralNet_iris.cross_v_number_neuron(10, train_iris, train_labels_iris, 10)
# lines = plt.plot(neuroneX , errorY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de neurone')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# validation croisé nombre couche caché
# best_nb_layer, layerX, layerY = neuralNet_iris.cross_v_number_layer(10, train_iris, train_labels_iris, best_nb_neuron)
# lines = plt.plot(layerX , layerY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de couches cachées')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# courbe d'apprentissage pour le nombre de couche cachée
# trainning_size, proportion3 = neuralNet_iris.learning_curve_nbLayer(train_iris, train_labels_iris, 2, best_nb_neuron, 3)
# trainning_size2, proportion4 = neuralNet_iris.learning_curve_nbLayer(train_iris, train_labels_iris, 2, best_nb_neuron, 4)
# trainning_size3, proportion5 = neuralNet_iris.learning_curve_nbLayer(train_iris, train_labels_iris, 2, best_nb_neuron, 5)
# trainning_size4, proportion6 = neuralNet_iris.learning_curve_nbLayer(train_iris, train_labels_iris, 2, best_nb_neuron, 6)
# trainning_size6, proportion7 = neuralNet_iris.learning_curve_nbLayer(train_iris, train_labels_iris, 2, best_nb_neuron, 7)

# lines3 = plt.plot(trainning_size, proportion3)
# lines4 = plt.plot(trainning_size, proportion4)
# lines5 = plt.plot(trainning_size, proportion5)
# lines6 = plt.plot(trainning_size, proportion6)
# lines7 = plt.plot(trainning_size, proportion7)
# plt.ylabel('Proportion correct on test set')
# plt.xlabel('Trainning set size')
# plt.setp(lines3, 'color', 'r', 'linewidth', 1.0)
# plt.setp(lines4, 'color', 'g', 'linewidth', 1.0)
# plt.setp(lines5, 'color', 'b', 'linewidth', 1.0)
# plt.setp(lines6, 'color', 'c', 'linewidth', 1.0)
# plt.setp(lines7, 'color', 'm', 'linewidth', 1.0)
# plt.show()

vitesse1 = time.clock()
neuralNet_iris.train(train_iris, train_labels_iris, 100, best_nb_neuron, best_nb_layer)
vitesse2 = time.clock()
time_NNIris = vitesse2 - vitesse1


# Dataset vote aux congres
best_nb_neuron1 = 16
best_nb_layer1 = 3

# validation croisé nombre de neurones par couche caché
# best_nb_neuron1, neuroneX, errorY = neuralNet_cong.cross_v_number_neuron(10, train_cong, train_labels_cong, 10)
# lines = plt.plot(neuroneX , errorY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de neurone')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# validation croisé nombre couche caché
# best_nb_layer1, layerX, layerY = neuralNet_cong.cross_v_number_layer(10, train_cong, train_labels_cong, best_nb_neuron1)
# lines = plt.plot(layerX , layerY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de couches cachées')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# courbe d'apprentissage pour le nombre de couche cachée
# trainning_size, proportion3 = neuralNet_cong.learning_curve_nbLayer(train_cong, train_labels_cong, 2, best_nb_neuron1, 3)
# trainning_size2, proportion4 = neuralNet_cong.learning_curve_nbLayer(train_cong, train_labels_cong, 2, best_nb_neuron1, 4)
# trainning_size3, proportion5 = neuralNet_cong.learning_curve_nbLayer(train_cong, train_labels_cong, 2, best_nb_neuron1, 5)
# trainning_size4, proportion6 = neuralNet_cong.learning_curve_nbLayer(train_cong, train_labels_cong, 2, best_nb_neuron1, 6)
# trainning_size6, proportion7 = neuralNet_cong.learning_curve_nbLayer(train_cong, train_labels_cong, 2, best_nb_neuron1, 7)
#
# lines3 = plt.plot(trainning_size, proportion3)
# lines4 = plt.plot(trainning_size, proportion4)
# lines5 = plt.plot(trainning_size, proportion5)
# lines6 = plt.plot(trainning_size, proportion6)
# lines7 = plt.plot(trainning_size, proportion7)
# plt.ylabel('Proportion correct on test set')
# plt.xlabel('Trainning set size')
# plt.setp(lines3, 'color', 'r', 'linewidth', 1.0)
# plt.setp(lines4, 'color', 'g', 'linewidth', 1.0)
# plt.setp(lines5, 'color', 'b', 'linewidth', 1.0)
# plt.setp(lines6, 'color', 'c', 'linewidth', 1.0)
# plt.setp(lines7, 'color', 'm', 'linewidth', 1.0)
# plt.show()

vitesse1 = time.clock()
neuralNet_cong.train(train_cong, train_labels_cong, 10, best_nb_neuron1, best_nb_layer1)
vitesse2 = time.clock()
time_NNcong = vitesse2 - vitesse1

# Dataset Monk1
best_nb_neuron2 = 15
best_nb_layer2 = 3

# validation croisé nombre de neurones par couche caché
# best_nb_neuron2, neuroneX, errorY = neuralNet_monk1.cross_v_number_neuron(10, train_monk1, train_labels_monk1, 10)
# lines = plt.plot(neuroneX , errorY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de neurone')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# validation croisé nombre couche caché
# best_nb_layer2, layerX, layerY  = neuralNet_monk1.cross_v_number_layer(5, train_monk1, train_labels_monk1, best_nb_neuron2)
# lines = plt.plot(layerX , layerY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de couches')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# courbe d'apprentissage pour le nombre de couche cachée
# trainning_size, proportion3 = neuralNet_monk1.learning_curve_nbLayer(train_monk1, train_labels_monk1, 2, best_nb_neuron2, 3)
# trainning_size2, proportion4 = neuralNet_monk1.learning_curve_nbLayer(train_monk1, train_labels_monk1, 2, best_nb_neuron2, 4)
# trainning_size3, proportion5 = neuralNet_monk1.learning_curve_nbLayer(train_monk1, train_labels_monk1, 2, best_nb_neuron2, 5)
# trainning_size4, proportion6 = neuralNet_monk1.learning_curve_nbLayer(train_monk1, train_labels_monk1, 2, best_nb_neuron2, 6)
# trainning_size6, proportion7 = neuralNet_monk1.learning_curve_nbLayer(train_monk1, train_labels_monk1, 2, best_nb_neuron2, 7)
#
# lines3 = plt.plot(trainning_size, proportion3)
# lines4 = plt.plot(trainning_size, proportion4)
# lines5 = plt.plot(trainning_size, proportion5)
# lines6 = plt.plot(trainning_size, proportion6)
# lines7 = plt.plot(trainning_size, proportion7)
# plt.ylabel('Proportion correct on test set')
# plt.xlabel('Trainning set size')
# plt.setp(lines3, 'color', 'r', 'linewidth', 1.0)
# plt.setp(lines4, 'color', 'g', 'linewidth', 1.0)
# plt.setp(lines5, 'color', 'b', 'linewidth', 1.0)
# plt.setp(lines6, 'color', 'c', 'linewidth', 1.0)
# plt.setp(lines7, 'color', 'm', 'linewidth', 1.0)
# plt.show()

vitesse1 = time.clock()
neuralNet_monk1.train(train_monk1, train_labels_monk1, 1000, best_nb_neuron2, best_nb_layer2)
vitesse2 = time.clock()
time_NNmonk1 = vitesse2 - vitesse1

# Dataset Monk2
best_nb_neuron3 = 10
best_nb_layer3 = 4

# validation croisé nombre de neurones par couche caché
# best_nb_neuron3, neuroneX, errorY = neuralNet_monk2.cross_v_number_neuron(10, train_monk2, train_labels_monk2, 10)
# lines = plt.plot(neuroneX , errorY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de neurone')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# validation croisé nombre couche caché
# best_nb_layer3, layerX, layerY  = neuralNet_monk2.cross_v_number_layer(5, train_monk2, train_labels_monk2, best_nb_neuron3)
# lines = plt.plot(layerX , layerY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de couches')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# courbe d'apprentissage pour le nombre de couche cachée
# trainning_size, proportion3 = neuralNet_monk2.learning_curve_nbLayer(train_monk2, train_labels_monk2, 2, best_nb_neuron3, 3)
# trainning_size2, proportion4 = neuralNet_monk2.learning_curve_nbLayer(train_monk2, train_labels_monk2, 2, best_nb_neuron3, 4)
# trainning_size3, proportion5 = neuralNet_monk2.learning_curve_nbLayer(train_monk2, train_labels_monk2, 2, best_nb_neuron3, 5)
# trainning_size4, proportion6 = neuralNet_monk2.learning_curve_nbLayer(train_monk2, train_labels_monk2, 2, best_nb_neuron3, 6)
# trainning_size6, proportion7 = neuralNet_monk2.learning_curve_nbLayer(train_monk2, train_labels_monk2, 2, best_nb_neuron3, 7)
#
# lines3 = plt.plot(trainning_size, proportion3)
# lines4 = plt.plot(trainning_size, proportion4)
# lines5 = plt.plot(trainning_size, proportion5)
# lines6 = plt.plot(trainning_size, proportion6)
# lines7 = plt.plot(trainning_size, proportion7)
# plt.ylabel('Proportion correct on test set')
# plt.xlabel('Trainning set size')
# plt.setp(lines3, 'color', 'r', 'linewidth', 1.0)
# plt.setp(lines4, 'color', 'g', 'linewidth', 1.0)
# plt.setp(lines5, 'color', 'b', 'linewidth', 1.0)
# plt.setp(lines6, 'color', 'c', 'linewidth', 1.0)
# plt.setp(lines7, 'color', 'm', 'linewidth', 1.0)
# plt.show()

vitesse1 = time.clock()
neuralNet_monk2.train(train_monk2, train_labels_monk2, 10, best_nb_neuron3, best_nb_layer3)
vitesse2 = time.clock()
time_NNmonk2 = vitesse2 - vitesse1

# Dataset Monk3
best_nb_neuron4 = 33
best_nb_layer4 = 3

# validation croisé nombre de neurones par couche caché
# best_nb_neuron4, neuroneX, errorY = neuralNet_monk3.cross_v_number_neuron(5, train_monk3, train_labels_monk3, 10)
# lines = plt.plot(neuroneX , errorY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de neurone')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# validation croisé nombre couche caché
# best_nb_layer4, layerX, layerY  = neuralNet_monk3.cross_v_number_layer(5, train_monk3, train_labels_monk3, best_nb_neuron4)
# lines = plt.plot(layerX , layerY)
# plt.ylabel('Erreur Moyenne')
# plt.xlabel('Nombre de couches')
# plt.setp(lines, 'color', 'r', 'linewidth', 2.0)
# plt.show()

# courbe d'apprentissage pour le nombre de couche cachée
# trainning_size, proportion3 = neuralNet_monk3.learning_curve_nbLayer(train_monk3, train_labels_monk3, 2, best_nb_neuron4, 3)
# trainning_size2, proportion4 = neuralNet_monk3.learning_curve_nbLayer(train_monk3, train_labels_monk3, 2, best_nb_neuron4, 4)
# trainning_size3, proportion5 = neuralNet_monk3.learning_curve_nbLayer(train_monk3, train_labels_monk3, 2, best_nb_neuron4, 5)
# trainning_size4, proportion6 = neuralNet_monk3.learning_curve_nbLayer(train_monk3, train_labels_monk3, 2, best_nb_neuron4, 6)
# trainning_size6, proportion7 = neuralNet_monk3.learning_curve_nbLayer(train_monk3, train_labels_monk3, 2, best_nb_neuron4, 7)
#
# lines3 = plt.plot(trainning_size, proportion3)
# lines4 = plt.plot(trainning_size, proportion4)
# lines5 = plt.plot(trainning_size, proportion5)
# lines6 = plt.plot(trainning_size, proportion6)
# lines7 = plt.plot(trainning_size, proportion7)
# plt.ylabel('Proportion correct on test set')
# plt.xlabel('Trainning set size')
# plt.setp(lines3, 'color', 'r', 'linewidth', 1.0)
# plt.setp(lines4, 'color', 'g', 'linewidth', 1.0)
# plt.setp(lines5, 'color', 'b', 'linewidth', 1.0)
# plt.setp(lines6, 'color', 'c', 'linewidth', 1.0)
# plt.setp(lines7, 'color', 'm', 'linewidth', 1.0)
# plt.show()


vitesse1 = time.clock()
neuralNet_monk3.train(train_monk3, train_labels_monk3, 5000, best_nb_neuron4, best_nb_layer4)
vitesse2 = time.clock()
time_NNmonk3 = vitesse2 - vitesse1


print(" ")
print("************************************TESTS ************************************************")
print("_______________ _______________ARBRE DE DECISION___________________________________________:")
print(" ")
print("Résultats des test pour le dataset IRIS avec Arbre de decision:")
decisionTree_iris.test(test_iris, test_labels_iris, 3)
print(" ")
print("Résultats des test pour le dataset de VOTES CONGRET avec Arbre de decision:")
decisionTree_cong.test(test_cong, test_labels_cong, 2)
print(" ")
print("Résultats des test pour le dataset de MONK1 avec Arbre de decision:")
decisionTree_monk1.test(test_monk1, test_labels_monk1, 2)
print(" ")
print("Résultats des test pour le dataset de MONK2 avec Arbre de decision:")
decisionTree_monk2.test(test_monk2, test_labels_monk2, 2)
print(" ")
print("Résultats des test pour le dataset de MONK3 avec Arbre de decision:")
decisionTree_monk3.test(test_monk3, test_labels_monk3, 2)

# temps d'apprentissage
print(" ")
print("***********************************temps d'apprentissage modele (ARBRE DECISIOM)****************************** ")

print("dataset Iris")
print(time_ADIris)
print("dataset Vote au congres")
print(time_ADCong)
print("dataset Monk1")
print(time_ADMonk1)
print("dataset Monk2")
print(time_ADMonk2)
print("dataset Monk3")
print(time_ADMonk3)

print(" ")
print("***********************************temps de prédiction 1 exemple(ARBRE DE  DECISION)******************** ")

vitesse1 = time.clock()
decisionTree_iris.test(test_iris[:1], test_labels_iris[:1], 2, False, False)
vitesse2 = time.clock()
time_ADIris = vitesse2 - vitesse1
print("temps de prédiction dataset Iris")
print(time_ADIris)

vitesse1 = time.clock()
decisionTree_cong.test(test_cong[:1], test_labels_cong[:1], 2, False, False)
vitesse2 = time.clock()
time_ADCong = vitesse2 - vitesse1
print("temps de prédiction dataset Vote au congres")
print(time_ADCong)

vitesse1 = time.clock()
decisionTree_monk1.test(test_monk1[:1], test_labels_monk1[:1], 2, False, False)
vitesse2 = time.clock()
time_ADMonk1 = vitesse2 - vitesse1
print("temp prédiction dataset Monk1")
print(time_ADMonk1)

vitesse1 = time.clock()
decisionTree_monk2.test(test_monk2[:1], test_labels_monk2[:1], 2, False, False)
vitesse2 = time.clock()
time_ADMonk2 = vitesse2 - vitesse1
print("temp prédiction dataset Monk2")
print(time_ADMonk2)

vitesse1 = time.clock()
decisionTree_monk3.test(test_monk3[:1], test_labels_monk3[:1], 2, False, False)
vitesse2 = time.clock()
time_ADMonk3 = vitesse2 - vitesse1
print("temp prédiction dataset Monk3")
print(time_ADMonk3)


print(" ")
print("************************************TESTS ************************************************")
print("_______________ _______________NEURAL NETWORKS___________________________________________:")
print(" ")
print("Résultats des test pour le dataset IRIS avec Neural Networks:")
neuralNet_iris.test(test_iris, test_labels_iris)
print(" ")
print("Résultats des test pour le dataset de VOTES CONGRET avec Neural Networks:")
neuralNet_cong.test(test_cong, test_labels_cong)
print(" ")
print("Résultats des test pour le dataset MONK1 avec Neural Networks:")
neuralNet_monk1.test(test_monk1, test_labels_monk1)
print(" ")
print("Résultats des test pour le dataset MONK2 avec Neural Networks:")
neuralNet_monk2.test(test_monk2, test_labels_monk2)
print(" ")
print("Résultats des test pour le dataset MONK3 avec Neural Networks:")
neuralNet_monk3.test(test_monk3, test_labels_monk3)

# temps d'apprentissage
print(" ")
print("***********************************temps d'apprentissage modele (NEURAL NETWORKS)**************************** ")
print("dataset Iris")
print(time_NNIris)
print("dataset Vote au congres")
print(time_NNcong)
print("dataset Monk1")
print(time_NNmonk1)
print("dataset Monk2")
print(time_NNmonk2)
print("dataset Monk3")
print(time_NNmonk3)

print(" ")
print("***********************************temps de prédiction 1 exemple(NEURAL NETWORKS)*************************** ")

vitesse1 = time.clock()
neuralNet_iris.test(test_iris[:1], test_labels_iris[:1], False)
vitesse2 = time.clock()
time_NNiris = vitesse2 - vitesse1
print("Temps de pr/diction dataset Iris")
print(time_NNiris)


vitesse1 = time.clock()
neuralNet_cong.test(test_cong[:1], test_labels_cong[:1], False)
vitesse2 = time.clock()
time_NNcong = vitesse2 - vitesse1
print("temps de prédiction dataset Vote au congres")
print(time_NNcong)

vitesse1 = time.clock()
neuralNet_monk1.test(test_monk1[:1], test_labels_monk1[:1], False)
vitesse2 = time.clock()
time_NNmonk1 = vitesse2 - vitesse1
print("temp prédiction dataset Monk1")
print(time_NNmonk1)

vitesse1 = time.clock()
neuralNet_monk2.test(test_monk2[:1], test_labels_monk2[:1], False)
vitesse2 = time.clock()
time_NNmonk2 = vitesse2 - vitesse1
print("temp prédiction dataset Monk2")
print(time_NNmonk2)

vitesse1 = time.clock()
neuralNet_monk3.test(test_monk3[:1], test_labels_monk3[:1], False)
vitesse2 = time.clock()
time_NNmonk3 = vitesse2 - vitesse1
print("temp prédiction dataset Monk3")
print(time_NNmonk3)

# Calculer la courbe d'apprentissage l'arbre de décsion
"""
trainning_size, proportion = decisionTree_iris.learning_curve(train_iris, train_labels_iris, 3)
lines = plt.plot(trainning_size, proportion)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.show()


trainning_size, proportion = decisionTree_cong.learning_curve(train_cong, train_labels_cong, 2)
lines = plt.plot(trainning_size, proportion)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.show()

trainning_size, proportion = decisionTree_monk1.learning_curve(train_monk1, train_labels_monk1, 2)
lines = plt.plot(trainning_size, proportion)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.show()


trainning_size, proportion = decisionTree_monk2.learning_curve(train_monk2, train_labels_monk2, 2)
lines = plt.plot(trainning_size, proportion)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.show()

trainning_size, proportion = decisionTree_monk3.learning_curve(train_monk3, train_labels_monk3, 2)
lines = plt.plot(trainning_size, proportion)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.show()
"""
# Calculer la courbe d'apprentissage pour RN_ZERO et RN_NON_ZERO pour le réseau de neurone

"""
# dataset iris

trainning_size, proportion, proportion_zero = neuralNet_iris.learning_curve(train_iris, train_labels_iris, 2, best_nb_neuron, best_nb_layer)

lines = plt.plot(trainning_size , proportion)
lines2 = plt.plot(trainning_size , proportion_zero)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.setp(lines2, 'color', 'g', 'linewidth', 1.0)
plt.show()

# dataset vote au congres

trainning_size, proportion, proportion_zero = neuralNet_cong.learning_curve(train_cong, train_labels_cong, 2, best_nb_neuron1, best_nb_layer1)

lines = plt.plot(trainning_size , proportion)
lines2 = plt.plot(trainning_size , proportion_zero)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.setp(lines2, 'color', 'g', 'linewidth', 1.0)
plt.show()

#  dataset monk1
trainning_size, proportion, proportion_zero = neuralNet_monk1.learning_curve(train_monk1, train_labels_monk1, 20, best_nb_neuron2, best_nb_layer2)

lines = plt.plot(trainning_size , proportion)
lines2 = plt.plot(trainning_size , proportion_zero)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.setp(lines2, 'color', 'g', 'linewidth', 1.0)
plt.show()

#  dataset monk2
trainning_size, proportion, proportion_zero = neuralNet_monk2.learning_curve(train_monk2, train_labels_monk2, 2, best_nb_neuron3, best_nb_layer3)

lines = plt.plot(trainning_size , proportion)
lines2 = plt.plot(trainning_size , proportion_zero)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.setp(lines2, 'color', 'g', 'linewidth', 1.0)
plt.show()

#  dataset monk3

trainning_size, proportion, proportion_zero = neuralNet_monk3.learning_curve(train_monk3, train_labels_monk3, 20, best_nb_neuron4, best_nb_layer4)

lines = plt.plot(trainning_size , proportion)
lines2 = plt.plot(trainning_size , proportion_zero)
plt.ylabel('Proportion correct on test set')
plt.xlabel('Trainning set size')
plt.setp(lines, 'color', 'r', 'linewidth', 1.0)
plt.setp(lines2, 'color', 'g', 'linewidth', 1.0)
plt.show()"""

