# Programme fait par Sandra Bessette cours IFT 4102 , Projet Finale 30 avril 2017

import numpy as np
import random
np.set_printoptions(threshold=np.inf)


def load_iris_dataset(train_ratio):
    """Cette fonction a pour but de lire le dataset Iris

    Args:
        train_ratio: le ratio des exemples (ou instances) qui vont etre attribués à l'entrainement,
        le rest des exemples va etre utilisé pour les tests.
        Par exemple : si le ratio est 50%, il y aura 50% des exemple (75 exemples) qui vont etre utilisé
        pour l'entrainement, et 50% (75 exemples) pour le test.

    Retours:
        Cette fonction doit retourner 4 matrices de type Numpy, train, train_labels, test, et test_labels

        - train : une matrice numpy qui contient les exemples qui vont etre utilisés pour l'entrainement, chaque
        ligne dans cette matrice représente un exemple (ou instance) d'entrainement.

        - train_labels : contient les labels (ou les étiquettes) pour chaque exemple dans train, de telle sorte
          que : train_labels[i] est le label (ou l'etiquette) pour l'exemple train[i]

        - test : une matrice numpy qui contient les exemples qui vont etre utilisés pour le test, chaque
        ligne dans cette matrice représente un exemple (ou instance) de test.

        - test_labels : contient les labels (ou les étiquettes) pour chaque exemple dans test, de telle sorte
          que : test_labels[i] est le label (ou l'etiquette) pour l'exemple test[i]
    """

    random.seed(1)  # Pour avoir les meme nombres aléatoires à chaque initialisation.

    # Vous pouvez utiliser des valeurs numériques pour les différents types de classes, tel que :
    conversion_labels = {'Iris-setosa': 0, 'Iris-versicolor': 1, 'Iris-virginica': 2}

    # Le fichier du dataset est dans le dossier datasets en attaché
    f = open('datasets/bezdekIris.data', 'r')

    content = f.read().splitlines()
    f.close()

    intermediate_array = []
    train_array = []
    train_label_array = []
    test_array = []
    test_label_array = []

    # séparer en liste
    for sample in content:
        sample = sample.split(',')
        intermediate_array.append(sample)

    # le dernier élément de la liste n'est pas une donnée, il faut l'enlever
    intermediate_array = intermediate_array[:-1]

    random.shuffle(intermediate_array)

    # convertir les string en float et séparer les différentes liste
    long = len(intermediate_array)

    nb_element = 0
    for line in intermediate_array:
        nb_element += 1
        new_list_train = []
        new_list_test = []
        for item in line:
            if item in conversion_labels:
                if nb_element <= long * train_ratio:
                    train_label_array.append(conversion_labels[item])
                else:
                    test_label_array.append(conversion_labels[item])
            else:
                new_item = float(item)
                if nb_element <= long * train_ratio:
                    new_list_train.append(new_item)
                else:
                    new_list_test.append(new_item)
        if nb_element <= long * train_ratio:
            train_array.append(new_list_train)
        else:
            test_array.append(new_list_test)

    # mettre dans des arrays numpy
    train = np.asarray(train_array)
    train_labels = np.asarray(train_label_array)
    test = np.asarray(test_array)
    test_labels = np.asarray(test_label_array)
    return (train, train_labels, test, test_labels)


def load_congressional_dataset(train_ratio):
    """Cette fonction a pour but de lire le dataset Congressional Voting Records

    Args:
        train_ratio: le ratio des exemples (ou instances) qui vont servir pour l'entrainement,
        le rest des exemples va etre utilisé pour les test.

    Retours:
        Cette fonction doit retourner 4 matrices de type Numpy, train, train_labels, test, et test_labels

        - train : une matrice numpy qui contient les exemples qui vont etre utilisés pour l'entrainement, chaque
        ligne dans cette matrice représente un exemple (ou instance) d'entrainement.

        - train_labels : contient les labels (ou les étiquettes) pour chaque exemple dans train, de telle sorte
          que : train_labels[i] est le label (ou l'etiquette) pour l'exemple train[i]

        - test : une matrice numpy qui contient les exemples qui vont etre utilisés pour le test, chaque
        ligne dans cette matrice représente un exemple (ou instance) de test.

        - test_labels : contient les labels (ou les étiquettes) pour chaque exemple dans test, de telle sorte
          que : test_labels[i] est le label (ou l'etiquette) pour l'exemple test[i]
    """

    random.seed(1)  # Pour avoir les meme nombres aléatoires à chaque initialisation.

    conversion_labels = {'republican': 0, 'democrat': 1,
                         'n': 0, 'y': 1, '?': 2}

    # Le fichier du dataset est dans le dossier datasets en attaché
    f = open('datasets/house-votes-84.data', 'r')

    # TODO : le code ici pour lire le dataset
    content = f.read().splitlines()
    f.close()

    intermediate_array = []
    train_array = []
    train_label_array = []
    test_array = []
    test_label_array = []

    # séparer en liste
    for sample in content:
        sample = sample.split(',')
        intermediate_array.append(sample)

    # convertir les string en float et séparer les différentes liste
    long = len(intermediate_array)
    nb_element = 0
    for line in intermediate_array:
        nb_element += 1
        new_list_train = []
        new_list_test = []
        for item in line:
            if item in conversion_labels:
                if (item == 'republican') or (item == 'democrat'):
                    if nb_element <= long * train_ratio:
                        train_label_array.append(conversion_labels[item])
                    else:
                        test_label_array.append(conversion_labels[item])
                else:
                    if nb_element <= long * train_ratio:
                        new_list_train.append(int(conversion_labels[item]))
                    else:
                        new_list_test.append(int(conversion_labels[item]))
        if nb_element <= long * train_ratio:
            train_array.append(new_list_train)
        else:
            test_array.append(new_list_test)

    # mettre dans des arrays numpy
    train = np.asarray(train_array)
    train_labels = np.asarray(train_label_array)
    test = np.asarray(test_array)
    test_labels = np.asarray(test_label_array)

    # La fonction retourne 4 structures de données de type Numpy.
    return (train, train_labels, test, test_labels)


def load_monks_dataset(numero_dataset):
    """Cette fonction a pour but de lire le dataset Monks

    Args:
        numero_dataset: lequel des sous problèmes nous voulons charger (1, 2 ou 3 ?)
        les fichiers sont tous dans le dossier datasets
    Retours:
        Cette fonction doit retourner 4 matrices de type Numpy, train, train_labels, test, et test_labels

        - train : une matrice numpy qui contient les exemples qui vont etre utilisés pour l'entrainement, chaque
        ligne dans cette matrice représente un exemple (ou instance) d'entrainement.
        - train_labels : contient les labels (ou les étiquettes) pour chaque exemple dans train, de telle sorte
          que : train_labels[i] est le label (ou l'etiquette) pour l'exemple train[i]

        - test : une matrice numpy qui contient les exemples qui vont etre utilisés pour le test, chaque
        ligne dans cette matrice représente un exemple (ou instance) de test.
        - test_labels : contient les labels (ou les étiquettes) pour chaque exemple dans test, de telle sorte
          que : test_labels[i] est le label (ou l'etiquette) pour l'exemple test[i]
    """

    # les donné de trainning
    if numero_dataset == 1:
        f = open('datasets/monks-1.train', 'r')
    elif numero_dataset == 2:
        f = open('datasets/monks-2.train', 'r')
    elif numero_dataset == 3:
        f = open('datasets/monks-3.train', 'r')
    else:
        return -1

    content = f.read().splitlines()
    f.close()

    intermediate_array = []
    train_array = []
    train_label_array = []

    # séparer en liste
    for sample in content:
        sample = sample.strip(' ')
        sample = sample.split(',')
        for item in sample:
            item = item.split(' ')
            intermediate_array.append(item)

    for line in intermediate_array:
        new_list_train = []
        for item in line[0:1]:
            new_item = int(item)
            train_label_array.append(new_item)
        for item in line[1:-1]:
            new_item = int(item)
            new_list_train.append(new_item)
            # for item in line[7:8]:
            #   new_list_train.append(item)
        train_array.append(new_list_train)

    # les donné de test
    if numero_dataset == 1:
        f = open('datasets/monks-1.test', 'r')
    elif numero_dataset == 2:
        f = open('datasets/monks-2.test', 'r')
    elif numero_dataset == 3:
        f = open('datasets/monks-3.test', 'r')

    content = f.read().splitlines()
    f.close()

    intermediate_array = []
    test_array = []
    test_label_array = []

    # séparer en liste
    for sample in content:
        sample = sample.strip(' ')
        sample = sample.split(',')
        for item in sample:
            item = item.split(' ')
            intermediate_array.append(item)

    for line in intermediate_array:
        new_list_test = []
        for item in line[0:1]:
            new_item = int(item)
            test_label_array.append(new_item)
        for item in line[1:-1]:
            new_item = int(item)
            new_list_test.append(new_item)
            # for item in line[7:8]:
            #   new_list_test.append(item)
        test_array.append(new_list_test)

    # mettre dans des arrays numpy
    train = np.asarray(train_array)
    train_labels = np.asarray(train_label_array)
    test = np.asarray(test_array)
    test_labels = np.asarray(test_label_array)

    # La fonction retourne 4 matrices (ou vecteurs) de type Numpy.
    return (train, train_labels, test, test_labels)

load_iris_dataset(0.5)
load_congressional_dataset(0.5)
#load_monks_dataset(1)
